<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Framenumber extends Model
{
    protected $fillable = ['geographical_id', 'vehicel_id','year_id','frame_number','modeld_id','frame_number_arabic'];

    public function vehicel()
    {
        return $this->belongsTo('App\Model\Vehicel');
    }
    public function year()
    {
        return $this->belongsTo('App\Model\Year');
    }
    public function geographical()
    {
        return $this->belongsTo('App\Model\Geographical');
    }
    public function modeld()
    {
        return $this->belongsTo('App\Model\Modeld');
    }
}
