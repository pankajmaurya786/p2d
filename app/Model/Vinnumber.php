<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Vinnumber extends Model
{
    protected $fillable = ['geographical_id', 'vehicel_id','year_id','framenumber_id','modeld_id','gradenumber_id','vin_number','vin_number_arabic'];


    public function vehicel()
    {
        return $this->belongsTo('App\Model\Vehicel');
    }
    public function year()
    {
        return $this->belongsTo('App\Model\Year');
    }
    public function geographical()
    {
        return $this->belongsTo('App\Model\Geographical');
    }
    public function modeld()
    {
        return $this->belongsTo('App\Model\Modeld');
    }
    public function framenumber()
    {
        return $this->belongsTo('App\Model\Framenumber');
    }
    public function gradenumber()
    {
        return $this->belongsTo('App\Model\Gradeengine');
    }
}
