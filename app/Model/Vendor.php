<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Vendor extends Model
{
    protected $fillable = ['user_id', 'company_name','profile_icon','address','flat_no','locality','landmark'];


    public function users(){

    	return $this->belongsTo('App\User');
    } 
}
