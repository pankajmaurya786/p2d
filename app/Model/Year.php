<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Year extends Model
{
    protected $fillable = ['geographical_id', 'vehicel_id','year','slug'];

    public function vehicel()
    {
        return $this->belongsTo('App\Model\Vehicel');
    }
    public function geographical()
    {
        return $this->belongsTo('App\Model\Geographical');
    }
}
