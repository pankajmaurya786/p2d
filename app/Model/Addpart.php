<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Addpart extends Model
{
    protected $fillable = ['geographical_id', 'vehicel_id','year_id','framenumber_id','modeld_id','gradenumber_id','vinnumber_id','display_number','part_name','part_number','part_name_arabic'];


    public function vehicel()
    {
        return $this->belongsTo('App\Model\Vehicel');
    }
    public function year()
    {
        return $this->belongsTo('App\Model\Year');
    }
    public function geographical()
    {
        return $this->belongsTo('App\Model\Geographical');
    }
    public function modeld()
    {
        return $this->belongsTo('App\Model\Modeld');
    }
    public function framenumber()
    {
        return $this->belongsTo('App\Model\Framenumber');
    }
    public function gradenumber()
    {
        return $this->belongsTo('App\Model\Gradeengine');
    }
    public function vinnumber()
    {
        return $this->belongsTo('App\Model\Vinnumber');
    }
    public function product()
    {
        return $this->belongsTo('App\Model\Product');
    }
}
