<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = ['product_image','addpart_id','vendor_id','price','quantity','vehicel_id','description','arabic_description'];
    
    public function addpart(){

    	return $this->belongsTo('App\Model\Addpart');
    }
    public function vendor(){

    	return $this->belongsTo('App\Model\Vendor');
    }
}
