<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Gradeengine extends Model
{
    protected $fillable = ['geographical_id', 'vehicel_id','year_id','framenumber_id','modeld_id','grade_number','grade_number_arabic'];


    public function vehicel()
    {
        return $this->belongsTo('App\Model\Vehicel');
    }
    public function year()
    {
        return $this->belongsTo('App\Model\Year');
    }
    public function geographical()
    {
        return $this->belongsTo('App\Model\Geographical');
    }
    public function modeld()
    {
        return $this->belongsTo('App\Model\Modeld');
    }
    public function framenumber()
    {
        return $this->belongsTo('App\Model\Framenumber');
    }
}
