<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class UserAddres extends Model
{
    protected $fillable = ['user_id', 'address','city','states','pin_code','locality','address_location'];
}
