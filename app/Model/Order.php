<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = ['product_id','user_id','vendor_id','addpart_id'];

    public function product(){

    	return $this->belongsTo('App\Model\Product');
    }

    public function vendor(){

    	return $this->belongsTo('App\Model\Vendor');
    }

    public function addpart(){

    	return $this->belongsTo('App\Model\Addpart');
    }

    public function user(){

    	return $this->belongsTo('App\User');
    }
}
