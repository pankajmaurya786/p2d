<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Vehicel extends Model
{
     protected $fillable = ['name', 'slug','product_icon','name_arabic'];

}
