<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Modeld extends Model
{
    protected $fillable = ['geographical_id', 'vehicel_id','year_id','name','name_arabic'];

    public function vehicel()
    {
        return $this->belongsTo('App\Model\Vehicel');
    }
    public function year()
    {
        return $this->belongsTo('App\Model\Year');
    }
    public function geographical()
    {
        return $this->belongsTo('App\Model\Geographical');
    }
}
