<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Geographical extends Model
{
    protected $fillable = ['name','slug','vehicel_id','name_arabic'];

    public function vehicel()
    {
        return $this->belongsTo('App\Model\Vehicel');
    }
}
