<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Vehicel;
use App\Model\Geographical;
use App\Model\Year;
use App\Model\Modeld;
use App\Model\Framenumber;
use App\Model\Gradeengine;
use App\Model\Vinnumber;
use App\Model\Addpart;
use App\User;
use App\Model\Vendor;
use App\Model\Product;

class AdminController extends Controller
{
    public function product(){
        $vehicel =Vehicel::paginate(10);
        $geographical =Geographical::all();
        $year =Year::all();
        $model =Modeld::all();
        $frame =Framenumber::all();
        $grade =Gradeengine::all();
        $vinnumber =Vinnumber::all();
        $addpart =Addpart::all();
    	return view('admin.product',compact('vehicel','geographical','year','model','frame','grade','vinnumber','addpart'));
    }

    public function addproduct(){

    	return view('vehicel.index');
    }

    public function productstore(Request $request)
    {   
        $product = new Product($request->all()); 
        $slug  = str_slug($request->input('name'),'-');
        $product->slug = $slug;
        
        if($file = $request->hasFile('product_icon')) 
            {
                $file = $request->file('product_icon') ;
                $fileName = "Product".str_random(6)."_".$file->getClientOriginalName() ;
                $destinationPath=public_path()."/uploads/product/";
                if (!is_dir($destinationPath)) 
                {
                  mkdir($destinationPath,0777,true);
                }

                $file->move($destinationPath,$fileName);
                $product->product_icon = $fileName ;
                $product->product_icon_url = env('APP_URL').'/uploads/product/'.$fileName;
                
            } 
            $product->save();
            $request->session()->flash('message.level', 'success');
            $request->session()->flash('message.content', 'Post was successfully Create!'); 
        return redirect()->route('admin.product');

        }
        
        
    public function vendorlist(){
        
        $vendor = User::where('user_type','Vendor')->get();
        return view('admin.adminvendor',compact('vendor'));
        
    }    
    
    public function userlist(){
        
        $user = User::where('user_type','User')->get();
        return view('admin.adminuser',compact('user'));
        
    }
    
    public function addproductadmin(){
        $part = Addpart::all();
        $vendor = Vendor::all();
        $vehicel = Vehicel::all();
        return view('admin.addproduct',compact('vendor','part','vehicel'));
    }
    
    
    public function addproductadminpost(Request $request){
        $product = new Product($request->all()); 
        
        if($file = $request->hasFile('product_image')) 
            {
                $file = $request->file('product_image') ;
                $fileName = "Product".str_random(6)."_".$file->getClientOriginalName() ;
                $destinationPath=public_path()."/uploads/product/";
                if (!is_dir($destinationPath)) 
                {
                  mkdir($destinationPath,0777,true);
                }

                $file->move($destinationPath,$fileName);
                $product->product_image = $fileName ;
                $product->product_image_url = env('APP_URL').'/uploads/product/'.$fileName;
                
            } 
            $product->save();
            $request->session()->flash('message.level', 'success');
            $request->session()->flash('message.content', 'Post was successfully Create!'); 
        return redirect()->route('admin.dashboard');
        
    }

    public function addproductlist(){

        $product = Product::all();
        return view('admin.addproductlist',compact('product'));
    }

    public function productadmindelet(Request $request, $id)
    {
       $product=Product::destroy($id);
       $request->session()->flash('message.level', 'danger');
       $request->session()->flash('message.content', 'Post was successfully Deleted!');
       return redirect()->route('admin.productlist'); 
    }


    public function adminusershow($id){

        $user = User::findOrFail($id);
        return view('admin.adminuser_show',compact('user'));
    }

    public function adminvendorshow($id){

        $vendor = User::findOrFail($id);
        return view('admin.adminvendor_show',compact('vendor'));
    }

    
}
