<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Year;
use App\Model\Vehicel;
use App\Model\Geographical;
use DB;

class YearController extends Controller
{
    

    public function index()
    {
        //
    }

    

    public function create()
    {
        $vehical =Vehicel::all();
        $geographic =Geographical::all();
        return view('year.create',compact('vehical','geographic'));
    }

    public function getStateList(Request $request)
    {
        $states = DB::table("geographicals")
                    ->where("vehicel_id",$request->vehicel_id)
                    ->pluck("name","id");
        return response()->json($states);
    }

    public function store(Request $request)
    {
        $years = new Year($request->all()); 
        $slug  = str_slug($request->input('year'),'-');
        $years->slug = $slug;
        $years->save();
            $request->session()->flash('message.level', 'success');
            $request->session()->flash('message.content', 'Post was successfully Create!'); 
        return redirect()->route('admin.product');
    }

    

    public function show($id)
    {
        //
    }

    

    public function edit($id)
    {
        $year = Year::findOrFail($id);
        return view('year.edit',compact('year'));
    }

    

    public function update(Request $request, $id)
    {
       $year = Year::findOrFail($id);
       $year->update($request->all());
            $year->save();
            $request->session()->flash('message.level', 'success');
            $request->session()->flash('message.content', 'Year was successfully Updated!');
            return redirect()->route('admin.dashboard');  
    }

    
    
    public function destroy(Request $request,$id)
    {
       $year=Year::destroy($id);
       $request->session()->flash('message.level', 'danger');
       $request->session()->flash('message.content', 'Year was successfully Deleted!');
      return redirect()->route('admin.dashboard');
    }
}
