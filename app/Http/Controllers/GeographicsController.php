<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Geographical;
use DB;

class GeographicsController extends Controller
{
    

    public function index()
    {
        
    }

    

    public function create()
    {
        $vehical = DB::table("vehicels")->pluck("name","id");
        return view('geographic.create',compact('vehical')); 
    }

    

    public function store(Request $request)
    {
        $geographics = new Geographical($request->all()); 
        $slug  = str_slug($request->input('name'),'-');
        $geographics->slug = $slug;
            $geographics->save();
            $request->session()->flash('message.level', 'success');
            $request->session()->flash('message.content', 'Post was successfully Create!'); 
        return redirect()->route('admin.product');
    }
    

    

    public function show($id)
    {
        
    }

    

    public function edit($id) { 

        $vehical = DB::table("vehicels")->pluck("name","id");
        // dd($vehical);
        $geographic = Geographical::findOrFail($id);
        return view('geographic.edit',compact('geographic','vehical'));
    }

    

    public function update(Request $request, $id)
    {
       $geographic = Geographical::findOrFail($id);
       $geographic->update($request->all());
            $geographic->save();
            $request->session()->flash('message.level', 'success');
            $request->session()->flash('message.content', 'Geographical was successfully Updated!');
            return redirect()->route('admin.dashboard');  
    }

    

    public function destroy(Request $request,$id)
    {
       $geographic=Geographical::destroy($id);
       $request->session()->flash('message.level', 'danger');
       $request->session()->flash('message.content', 'Geographical was successfully Deleted!');
      return redirect()->route('admin.dashboard');
    }
}
