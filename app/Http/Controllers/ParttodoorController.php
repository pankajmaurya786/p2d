<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Validator;
use Auth;
use App\Model\UserAddres;
use App\Model\Vendor;
use App\Model\Vehicel;
use App\Model\Geographical;
use App\Model\Year;
use App\Model\Modeld;
use App\Model\Framenumber;
use App\Model\Gradeengine;
use App\Model\Vinnumber;
use App\Model\Addpart;
use App\Model\Product;
use App\Model\About;
use App\Model\Contact;
use App\Model\Order;
use Lang;



class ParttodoorController extends Controller
{
    public function home(){
        
       $vehicles= Vehicel::all();
       

    	return view('layouts.home',compact('vehicles'));
    }

    public function login(){

    	return view('include.login');
    }

    public function register(){

    	return view('include.register');
    }

    public function registerpost(Request $request){


    	$this->validate($request, [
                               'name'=>'required',
                               'gender'=>'required',
                               'email'=>'required|email|unique:users',
                               'mobile'=>'required|numeric|digits_between:10,15',
                               'password'=>'required|min:6',
                               'confirmPassword' => 'required|same:password',
                                ]);

        
        if ($data   = $request->all()){
        $data['user_type'] = 'User';
        $user = new User($data);
        $user->password =  bcrypt($data['password']);
        $user->save();
        $request->session()->flash('message.level', 'success');
        $request->session()->flash('message.content', 'Post was successfully added!');
        } else {
        $request->session()->flash('message.level', 'danger');
        $request->session()->flash('message.content', 'Error!');
    }

        return redirect()->back();
    }

    public function loginpost(Request $request){

    	$data = $request->only('email', 'password');
        $data['user_type'] = 'User';
        
        if (Auth::attempt($data))
        {
            $request->session()->flash('message.level', 'success');
            $request->session()->flash('message.content', 'You have successfully logged in!');
           return redirect()->route('user.dashboard');
        }
      
        $request->session()->flash('message.level', 'danger');
        $request->session()->flash('message.content', 'Your username/password combination was incorrect!');
       
        return redirect()->route('user.login')->withInput();
    }
    
     public function logout(){
        Auth::logout();
        return redirect()->route('home');
    }

    /*user info update*/
    public function userUpdate(Request $request){
        // dd($request->all());
        $this->validate($request, [
                               'name'=>'required',
                               'gender'=>'required',
                               'email'=>'required|email|unique:users',
                               'mobile'=>'required|numeric|digits_between:10,15',
                               // 'password'=>'required|min:6',
                               // 'confirmPassword' => 'required|same:password',
                                ]);

        $id = Auth::id();
        $userInfoUpdate = User::where('id',$id)->first();
        $userInfoUpdate->name = $request->name;
        $userInfoUpdate->gender = $request->gender;
        $userInfoUpdate->email = $request->email;
        $userInfoUpdate->mobile = $request->mobile;
        $userInfoUpdate->save();
        $request->session()->flash('message.level', 'success');
        $request->session()->flash('message.content', 'Profile Updated Successfully!.');
        return back();
    }
    public function dashboard(){
    	$id=Auth::user()->id;
    	$order = Order::where('user_id',$id)->get();
    	$address = UserAddres::where('user_id',$id)->get();
    	return view('include.dashboard',compact('address','id','order'));
    }

    public function vendorlogin(){

    	return view('include.vendor_login');
    }


    public function useraddress(Request $request){
      
    	$user = new UserAddres($request->all()); 
	    $user->save();
	    $request->session()->flash('message.level', 'success');
        $request->session()->flash('message.content', 'Post was successfully Create!'); 
        return back();
        
        
    }

    public function vendorregister(){

    	return view('include.vendor_register');
    }

    public function vendorregisterpost(Request $request){
    	$this->validate($request, [
                               'name'=>'required',
                               'email'=>'required|email|unique:users',
                               'mobile'=>'required|numeric|digits_between:10,15',
                               'company_name'=>'required',
                               'password'=>'required|min:6',
                               'confirmPassword' => 'required|same:password',
                                ]);

        
        if ($data   = $request->all()){
        $data['user_type'] = 'Vendor';
        $user = new User($data);
        $user->password =  bcrypt($data['password']);
        $user->save();
        $vendor = new Vendor($data);
        $vendor->user_id = $user->id;
        $vendor->save();
        $request->session()->flash('message.level', 'success');
        $request->session()->flash('message.content', 'Post was successfully added!');
        } else {
        $request->session()->flash('message.level', 'danger');
        $request->session()->flash('message.content', 'Error!');
    }

        return redirect()->route('vendor.login');
    }


    public function vendorloginpost(Request $request){

    	$data = $request->only('email', 'password');
        $data['user_type'] = 'Vendor';
        
        if (Auth::attempt($data))
        {
            $request->session()->flash('message.level', 'success');
            $request->session()->flash('message.content', 'You have successfully logged in!');
           return redirect()->route('vendor.dashboard');
        }
      
        $request->session()->flash('message.level', 'danger');
        $request->session()->flash('message.content', 'Your username/password combination was incorrect!');
       
        return redirect()->route('vendor.login')->withInput();
    }

    public function vendordashboard(){

    	return view('vendors.vendor_maincontent');
    }

    public function vendorprofile(){
        $id = Auth::user()->id;
        $vendor = Vendor::where('user_id',$id)->get();
    	return view('vendors.vendor_profile',compact('vendor','id'));
    }


    public function adminlogin(){

    	return view('include.admin_login');
    }

    public function adminloginpost(Request $request){

    	$data = $request->only('email', 'password');
        $data['user_type'] = 'Admin';
        
        if (Auth::attempt($data))
        {
            $request->session()->flash('message.level', 'success');
            $request->session()->flash('message.content', 'You have successfully logged in!');
           return redirect()->route('admin.dashboard');
        }
      
        $request->session()->flash('message.level', 'danger');
        $request->session()->flash('message.content', 'Your username/password combination was incorrect!');
       
        return redirect()->route('admin.login')->withInput();
    }

    public function admindashboard(){

        $totaluser = User::where('user_type','User')->count();
        $totalvendor = User::where('user_type','Vendor')->count();
        $order = Order::all()->count();
        $sell = Order::all()->count();

    	return view('admin.admin_dashboard',compact('totaluser','totalvendor','order',''));
    }


    public function adminregister(){

    	return view('include.admin_register');
    }
    
    public function getLocation($id)
    {
        $locations = Geographical::where('vehicel_id',$id)->get();
        
        return view('layouts.location',compact('locations'));
    }
    
    public function geographicallist(Request $request){
        $search_type = $request->input('search_type');
        $geographical = Year::query();
        $geographical = $geographical->where('geographical_id','LIKE','%'.$search_type.'%')->get();
        // dd($geographical);
        return view('include.geographical',compact('geographical'));
        
    }
    
    public function yearsearch(Request $request){
        $search_type = $request->input('search_type');
        $model = Modeld::query();
        $model = $model->where('year_id','LIKE','%'.$search_type.'%')->get();
        return view('include.model',compact('model'));
        
    }
    
    public function framesearch(Request $request){
        $search_type = $request->input('search_type');
        $frame = Framenumber::query();
        $frame = $frame->where('modeld_id','LIKE','%'.$search_type.'%')->get();
        return view('include.frame',compact('frame'));
        
    }
    
    public function gradesearch(Request $request){
        $search_type = $request->input('search_type');
        $grade = Gradeengine::query();
        $grade = $grade->where('framenumber_id','LIKE','%'.$search_type.'%')->get();
        return view('include.grade',compact('grade'));
        
    }
    public function vinsearch(Request $request){
        $search_type = $request->input('search_type');
        $vin = Vinnumber::query();
        $vin = $vin->where('gradenumber_id','LIKE','%'.$search_type.'%')->get();
        return view('include.vinnumber',compact('vin'));
        
    }
    
    public function partssearch(Request $request){
        $search_type = $request->input('search_type');

        $showpart = Addpart::query();
        $showpart = $showpart->where('vinnumber_id','LIKE','%'.$search_type.'%')->get();
        return view('include.partshow',compact('showpart'));
        
    }
    
    public function productssearch(Request $request){
         // dd($vechile_id);
        $product = Product::query();
        if ($request->input('search_type')<>'') {
            $product->where('addpart_id',$request->input('search_type'));
        }
        if ($request->input('part_number')<>'') {
           $product->whereHas('addpart', function($q) use ($request) {
        return $q->where('part_name',$request->input('part_number'))->orWhere('part_name_arabic',$request->input('part_number'));
        });
        }
        $product = $product->orderBy('products.id','desc')->paginate(config('view.rpp'));
        return view('include.productshow',compact('product'));
        
    }
    
    public function productssearch2(Request $request){
        
        $product = Product::query();
        
        if ($request->input('search_type')<>'') {
           $product->where('vehicel_id', $request->input('search_type'));
        }
        if ($request->input('part_number')<>'') {
           $product->whereHas('addpart', function($q) use ($request) {
        return $q->where('part_name',$request->input('part_number'))->orWhere('part_name_arabic',$request->input('part_number'));

        });
        }
        $product = $product->orderBy('products.id','desc')->paginate(config('view.rpp'));
        // dd($product);
        return view('include.productshow',compact('product'));
        
    }
    
    
    public function autoComplete(Request $request) {
        $query = $request->input('search_text');
        
        $products=Product::whereHas('addpart', function($q) use ($query) {
        return $q->where('part_name','LIKE','%'.$query.'%');
        })->get();
        
        $data=array();
        foreach ($products as $product) {
                $data[]=array('value'=>$product->addpart->part_name,'id'=>$product->id);
        }
        if(count($data))
             return $data;
        else
            return ['value'=>'No Result Found','id'=>''];
    }
    

    public function frameproductssearch(Request $request){
        // dd($request->all());
        $search_type = $request->input('search_type');
        $valueSearch =  \Session::put('search_type',$search_type);
        // dd($valueSearch);
        if ($request->input('search_type')<>''||$valueSearch <> '') {
            $framesearch = Addpart::where('part_number', 'LIKE', '%' .$search_type. '%')
                ->orWhereHas('vinnumber', function($q) use ($search_type) {
                 return $q->where('vin_number', 'LIKE', '%' . $search_type . '%');
            })
                ->orWhereHas('framenumber', function($q) use ($search_type) {
                return $q->where('frame_number', 'LIKE', '%'. $search_type . '%');
        })->get();
    }
        return view('include.frameproductshow',compact('framesearch'));
        
    }

    public function updateprofile(Request $request)
    { 

       $vendor=Vendor::find(Auth::user()->vendor->id);
        if($file = $request->hasFile('profile_icon')) 
            {
                $file = $request->file('profile_icon') ;
                $fileName = "Product".str_random(6)."_".$file->getClientOriginalName() ;
                $destinationPath=public_path()."/uploads/profile/";
                if (!is_dir($destinationPath)) 
                {
                  mkdir($destinationPath,0777,true);
                }

                $file->move($destinationPath,$fileName);
                $vendor->profile_icon = $fileName ;
                $vendor->profile_icon_url = env('APP_URL').'/uploads/profile/'.$fileName;
                
            } 
              $vendor->update();
              $request->session()->flash('message.level', 'success');
              $request->session()->flash('message.content', 'Profile Update Successfully!');
              return redirect()->route('vendor.dashboard');
    }

    public function updateprofiles(Request $request)
    { 

        $user = Auth::user();
        $data = $request->all();
        $user->update($data);
        $user->vendor->update($data);
              $request->session()->flash('message.level', 'success');
              $request->session()->flash('message.content', 'Profile Update Successfully!');
              return redirect()->route('vendor.dashboard');
    }
    public function aboutus(){

        $aboutus = About::all();
        return view('include.aboutus',compact('aboutus'));
    }

    public function contactus(){

        return view('include.contactus');
    }

    public function contactuspost(Request $request){
        
        $contact= new Contact($request->all()); 
              $contact->save();
              $request->session()->flash('message.level', 'success');
              $request->session()->flash('message.content', 'Message Successfully Send Touch soon!');
        return back();
    }
    public function checkout(){
        if(!empty(Auth::id())){
            $allUserAddress = UserAddres::where('user_id',Auth::user()->id)->get();
            return view('include.checkout',compact('allUserAddress'));
        }else{
            return view('include.checkout');
        }
    }
     public function orderolaced(Request $request){
        
        if(!empty($request->product_id)) {
        $order = new Order($request->all()); 
            if($request->product_id) {
                $cart = session()->get('cart');
                if(isset($cart[$request->product_id])) {
                    unset($cart[$request->product_id]);
                    session()->put('cart', $cart);
                }
            }
            $order->save();
            $request->session()->flash('message.level', 'success');
            if(app()->getLocale() =='en'){
                $request->session()->flash('message.content', 'Product Placed successfully!');

            }else{
                $request->session()->flash('message.content', 'المنتج وضعت بنجاح!');
            }

            return view('include.placed');
        }else{
            $request->session()->flash('message.level', 'danger');
            if(app()->getLocale() =='en'){
                $request->session()->flash('message.content', 'Cart is Empty!');

            }else{
                $request->session()->flash('message.content', 'البطاقه خاليه!');
            }
                return back();
        }
        
    }
    
    public function vendororder(){
        $order = Order::where('vendor_id',Auth::user()->vendor->id)->get();
        return view('include.vendor_orders',compact('order'));
    }
    
    public function adminorder(){
        $order = Order::all();
        return view('admin.admin_orders',compact('order'));
    }

    public function autoCompleteSearch(Request $request) {
        $input=$request->all();
        $query=$input['query'];
        $vechile_id=$input['vechile_id'];
        if(!empty($query)) {
            if(\Lang::getLocale() == 'en'){
                $queryResult = Addpart::where('part_name','LIKE','%'.$query.'%')
                                        ->orWhere('vehicel_id',$vechile_id)
                                        ->select('part_name','id')
                                        ->get();
            if(!empty($queryResult)){
                $output = '<ul class="dropdown-menu form-control" style="display:block;
                        max-height: 150px;overflow: auto; position:relative">';
                        foreach($queryResult as $row)
                        {
                            
                           $output .= '
                           <li class="select select_result"><a>'.$row->part_name.'</a></li>
                           ';
                        }
                           $output .= '</ul>';
                          echo $output;
                          return;
            }

            $queryResult = Framenumber::where('frame_number','LIKE','%'.$query.'%')->orWhere('vehicel_id',$vechile_id)
                                    ->select('frame_number','id')->get();
            
            if(!empty($queryResult)){
                $output = '<ul class="dropdown-menu form-control" style="display:block;
                        max-height: 150px;overflow: auto; position:relative">';
                        foreach($queryResult as $row)
                        {
                           $output .= '
                           <li class="select select_result"><a>'.$row['frame_number'].'</a></li>
                           ';
                        }
                           $output .= '</ul>';
                          echo $output;
                          return;
            }

            $queryResult = Vinnumber::where('vin_number','LIKE','%'.$query.'%')->orWhere('vehicel_id',$vechile_id)
                                    ->select('vin_number','id')
                                    ->get();
            
            if(!empty($queryResult)){
                $output = '<ul class="dropdown-menu form-control" style="display:block;
                        max-height: 150px;overflow: auto; position:relative">';
                        foreach($queryResult as $row)
                        {
                           $output .= '
                           <li class="select select_result"><a>'.$row->vin_number.'</a></li>
                           ';
                        }
                           $output .= '</ul>';
                          echo $output;
                          return;
            }
        
         }elseif(\Lang::getLocale() == 'ar'){
            $queryResult = Addpart::where('part_name_arabic','LIKE','%'.$query.'%')->orWhere('vehicel_id',$vechile_id)
                                    ->select('part_name_arabic','id')
                                     ->get();
            // dd($queryResult);
            if(!empty($queryResult)){
                $output = '<ul class="dropdown-menu form-control" style="display:block;
                        max-height: 150px;overflow: auto; position:relative">';
                        foreach($queryResult as $row)
                        {
                            
                           $output .= '
                           <li class="select select_result"><a>'.$row->part_name_arabic.'</a></li>
                           ';
                        }
                           $output .= '</ul>';
                          echo $output;
                          return;
            }

            $queryResult = Framenumber::where('frame_number_arabic','LIKE','%'.$query.'%')->orWhere('vehicel_id',$vechile_id)->get();
            
            if(!empty($queryResult)){
                $output = '<ul class="dropdown-menu form-control" style="display:block;
                        max-height: 150px;overflow: auto; position:relative">';
                        foreach($queryResult as $row)
                        {
                           $output .= '
                           <li class="select select_result"><a>'.$row['frame_number_arabic'].'</a></li>
                           ';
                        }
                           $output .= '</ul>';
                          echo $output;
                          return;
            }

            $queryResult = Vinnumber::where('vin_number_arabic','LIKE','%'.$query.'%')
                                    ->orWhere('vehicel_id',$vechile_id)
                                    ->select('vin_number','id','vin_number_arabic')
                                    ->get();
            
            if(!empty($queryResult)){
                $output = '<ul class="dropdown-menu form-control" style="display:block;
                        max-height: 150px;overflow: auto; position:relative">';
                        foreach($queryResult as $row)
                        {
                           $output .= '
                           <li class="select select_result"><a>'.$row->vin_number_arabic.'</a></li>
                           ';
                        }
                           $output .= '</ul>';
                          echo $output;
                          return;
            }
        
         }
         
        }
            
    }

    /*check that input language is arabic or not*/
   //public function ValidateArabic($str) { 
        
    //}
}
