<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Gradeengine;
use App\Model\Vehicel;
use App\Model\Geographical;
use App\Model\Year;
use App\Model\Modeld;
use App\Model\Framenumber;
use DB;

class GradenumberController extends Controller
{
    

    public function index()
    {
        
    }

    

    public function create()
    {   
        $vehical = DB::table("vehicels")->pluck("name","id");
        $geographic =Geographical::all();
        $year =Year::all();
        $model =Modeld::all();
        $frame =Framenumber::all();
        return view('gradeengine.create',compact('vehical','geographic','year','model','frame'));
    }

    public function getmodellist(Request $request)
    {
        $states = DB::table("modelds")
                    ->where("year_id",$request->year_id)
                    ->pluck("name","id");
        return response()->json($states);
    }

    public function store(Request $request)
    {
        $grade = new Gradeengine($request->all()); 
            $grade->save();
            $request->session()->flash('message.level', 'success');
            $request->session()->flash('message.content', 'Post was successfully Create!'); 
        return redirect()->route('admin.product');
    }

    
    
    public function show($id)
    {
        
    }

    

    public function edit($id)
    {
        $grade = Gradeengine::findOrFail($id);
        return view('gradeengine.edit',compact('grade'));
    }

    

    public function update(Request $request, $id)
    {
       $grade = Gradeengine::findOrFail($id);
       $grade->update($request->all());
            $grade->save();
            $request->session()->flash('message.level', 'success');
            $request->session()->flash('message.content', 'Gradeengine was successfully Updated!');
            return redirect()->route('admin.dashboard');  
    }

    

    public function destroy(Request $request,$id)
    {
       $grade=Gradeengine::destroy($id);
       $request->session()->flash('message.level', 'danger');
       $request->session()->flash('message.content', 'Gradeengine was successfully Deleted!');
      return redirect()->route('admin.dashboard');
    }
}
