<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Product;
use App\Model\Addpart;
use App\Model\Vendor;
use Auth;

class ProductController extends Controller
{
    

    public function index()
    {
        $vendor=Vendor::where('user_id',Auth::user()->id)->first();
        $product = Product::where('vendor_id',$vendor->id)->get();
        return view('product.index',compact('product'));
    }

    

    public function create()
    {
        $vendor=Vendor::where('user_id',Auth::user()->id)->first();
        $addpart = Addpart::all();
        return view('product.create',compact('addpart','vendor'));
    }

    

    public function store(Request $request)
    {
        $product = new Product($request->all()); 
        
        if($file = $request->hasFile('product_image')) 
            {
                $file = $request->file('product_image') ;
                $fileName = "Product".str_random(6)."_".$file->getClientOriginalName() ;
                $destinationPath=public_path()."/uploads/product/";
                if (!is_dir($destinationPath)) 
                {
                  mkdir($destinationPath,0777,true);
                }

                $file->move($destinationPath,$fileName);
                $product->product_image = $fileName ;
                $product->product_image_url = env('APP_URL').'/uploads/product/'.$fileName;
                
            } 
            $product->save();
            $request->session()->flash('message.level', 'success');
            if(app()->getLocale() == 'en'){
                $request->session()->flash('message.content', 'Post was successfully Create!'); 
            }else{
                $request->session()->flash('message.content', 'تم النشر بنجاح!');   
            }
        return redirect()->route('vendor.dashboard');
    }

    

    public function show($id)
    {
        
    }

    

    public function edit($id)
    {
        $vendor=Vendor::where('user_id',Auth::user()->id)->first();
        $product = Product::findOrFail($id);
        $addpart = Addpart::all();
        return view('product.edit',compact('product','vendor','addpart'));
    }

    

    public function update(Request $request, $id)
    {
        $product = Product::findOrFail($id);
        $product->update($request->all());
        if($file = $request->hasFile('product_image')) 
            {
                $file = $request->file('product_image') ;
                $fileName = "Product".str_random(6)."_".$file->getClientOriginalName() ;
                $destinationPath=public_path()."/uploads/product/";
                if (!is_dir($destinationPath)) 
                {
                  mkdir($destinationPath,0777,true);
                }

                $file->move($destinationPath,$fileName);
                $product->product_image = $fileName ;
                $product->product_image_url = env('APP_URL').'/uploads/product/'.$fileName;
                
            } 
            $product->save();
            $request->session()->flash('message.level', 'success');
            if(app()->getLocale() == 'en'){
                $request->session()->flash('message.content', 'Post was successfully Create!'); 
            }else{
                $request->session()->flash('message.content', 'تم النشر بنجاح!');   
            } 
        return redirect()->route('product.index');
     
    }

    

    public function destroy(Request $request, $id)
    {
       $product=Product::destroy($id);
       $request->session()->flash('message.level', 'danger');
       $request->session()->flash('message.content', 'Post was successfully Deleted!');
       return redirect()->route('product.index'); 
    }


    public function cart()
    {
        return view('cart');
    }

   public function addToCart(Request $request, $id)
    {
        $product = Product::find($id);
 
        if(!$product) {
 
            abort(404);
 
        }
 
        $cart = session()->get('cart');
 
        // if cart is empty then this the first product
        if(!$cart) {
 
            $cart = [
                    $id => [
                        "part_name" => $product->addpart->part_name,
                        "part_name_arabic" => $product->addpart->part_name_arabic,
                        "quantity" => 1,
                        "price" => $product->price,
                        "photo" => $product->product_image_url,
                        "name" => $product->addpart->vehicel->name,
                        "name_arabic" => $product->addpart->vehicel->name_arabic,
                        "vendor" => $product->vendor->id,
                        "addpart" => $product->addpart->id
                    ]
            ];
 
            session()->put('cart', $cart);
            $request->session()->flash('message.level', 'success');
            if(app()->getLocale() =='en'){
            $request->session()->flash('message.content', 'Product added to cart successfully');

        }else{
             $request->session()->flash('message.content', 'تم اضافة المنتج الى السلة بنجاح');
        }
           
            return redirect()->back();
        }
 
        // if cart not empty then check if this product exist then increment quantity
        if(isset($cart[$id])) {
 
            $cart[$id]['quantity']++;
 
            session()->put('cart', $cart);
            $request->session()->flash('message.level', 'success');
            if(app()->getLocale() =='en'){
            $request->session()->flash('message.content', 'Product added to cart successfully');

        }else{
             $request->session()->flash('message.content', 'تم اضافة المنتج الى السلة بنجاح');
        }
            return redirect()->back();
 
        }
 
        // if item not exist in cart then add to cart with quantity = 1
        $cart[$id] = [
            "part_name" => $product->addpart->part_name,
            "part_name_arabic" => $product->addpart->part_name_arabic,
            "quantity" => 1,
            "price" => $product->price,
            "photo" => $product->product_image_url,
            "name" => $product->addpart->vehicel->name,
            "name_arabic" => $product->addpart->vehicel->name_arabic,
            "vendor" => $product->vendor->id,
            "addpart" => $product->addpart->id
        ];
 
        session()->put('cart', $cart);
        
        $request->session()->flash('message.level', 'success');
        if(app()->getLocale() =='en'){
            $request->session()->flash('message.content', 'Product added to cart successfully');

        }else{
             $request->session()->flash('message.content', 'تم اضافة المنتج الى السلة بنجاح');
        }
        return redirect()->back();
    }


    public function updatecart(Request $request)
    {
        if($request->id and $request->quantity) {

            $cart = session()->get('cart');
 
            $cart[$request->id]["quantity"] = $request->quantity;
 
            session()->put('cart', $cart);
            
            if(app()->getLocale() =='en'){
                $request->session()->flash('message.content', 'Cart updated successfully');

            }else{
                $request->session()->flash('message.content', 'تم تحديث السلة بنجاح');
            }
        }
    }
 
    public function remove(Request $request)
    {
        if($request->id) {
 
            $cart = session()->get('cart');
 
            if(isset($cart[$request->id])) {
 
                unset($cart[$request->id]);
 
                session()->put('cart', $cart);
            }
            if(app()->getLocale() =='en'){
                $request->session()->flash('message.content', 'Your Product place removed successfully');

            }else{
                 $request->session()->flash('message.content', 'تمت إزالة مكان المنتج الخاص بك بنجاح');
            }
        }
    }
}
