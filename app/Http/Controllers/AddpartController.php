<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Addpart;
use App\Model\Vehicel;
use App\Model\Geographical;
use App\Model\Year;
use App\Model\Modeld;
use App\Model\Framenumber;
use App\Model\Gradeengine;
use App\Model\Vinnumber;
use DB;

class AddpartController extends Controller
{
    

    public function index()
    {
        
    }

    

    public function create()
    {
        $vehical = DB::table("vehicels")->pluck("name","id");
        $geographic =Geographical::all();
        $year =Year::all();
        $model =Modeld::all();
        $frame =Framenumber::all();
        $grade =Gradeengine::all();
        $vin =Vinnumber::all();
        return view('addpart.create',compact('vehical','geographic','year','model','frame','grade','vin'));
    }

    public function getvinlist(Request $request)
    {
        $states = DB::table("vinnumbers")
                    ->where("gradenumber_id",$request->gradenumber_id)
                    ->pluck("vin_number","id");
        return response()->json($states);
    }

    public function store(Request $request)
    {
        $addpart = new Addpart($request->all()); 
            $addpart->save();
            $request->session()->flash('message.level', 'success');
            $request->session()->flash('message.content', 'Post was successfully Create!'); 
        return redirect()->route('admin.product');
    }

    

    public function show($id)
    {
        
    }

    
    
    public function edit($id)
    {
        $addpart = Addpart::findOrFail($id);
        return view('addpart.edit',compact('addpart'));
    }

    

    public function update(Request $request, $id)
    {
       $addpart = Addpart::findOrFail($id);
       $addpart->update($request->all());
            $addpart->save();
            $request->session()->flash('message.level', 'success');
            $request->session()->flash('message.content', 'Addpart was successfully Updated!');
            return redirect()->route('admin.dashboard');  
    }

    

    public function destroy(Request $request,$id)
    {
       $addpart=Addpart::destroy($id);
       $request->session()->flash('message.level', 'danger');
       $request->session()->flash('message.content', 'Addpart was successfully Deleted!');
      return redirect()->route('admin.dashboard');
    }
}
