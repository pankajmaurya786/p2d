<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\About;

class AboutController extends Controller
{
    

    public function index()
    {
        $aboutus = About::all();
        return view('about.index',compact('aboutus'));
    }

    

    public function create()
    {
        return view('about.create');
    }

    

    public function store(Request $request)
    {
        $about = new About($request->all()); 
        
        if($file = $request->hasFile('about_image')) 
            {
                $file = $request->file('about_image') ;
                $fileName = "About".str_random(6)."_".$file->getClientOriginalName() ;
                $destinationPath=public_path()."/uploads/about/";
                if (!is_dir($destinationPath)) 
                {
                  mkdir($destinationPath,0777,true);
                }

                $file->move($destinationPath,$fileName);
                $about->about_image = $fileName ;
                $about->about_image_url = env('APP_URL').'/uploads/about/'.$fileName;
                
            } 
            $about->save();
            $request->session()->flash('message.level', 'success');
            $request->session()->flash('message.content', 'Post is successfully Created!'); 
        return redirect()->route('admin.dashboard');
    }

    

    public function show($id)
    {
        
    }

    

    public function edit($id)
    {
      $about = About::findOrFail($id);
      return view('about.edit',compact('about'));  
    }

    

    public function update(Request $request, $id)
    {
       $about = About::findOrFail($id);
       $about->update($request->all());
       if($file = $request->hasFile('about_image')) 
            {
                $file = $request->file('about_image') ;
                $fileName = "About".str_random(6)."_".$file->getClientOriginalName() ;
                $destinationPath=public_path()."/uploads/about/";
                if (!is_dir($destinationPath)) 
                {
                  mkdir($destinationPath,0777,true);
                }

                $file->move($destinationPath,$fileName);
                $about->about_image = $fileName ;
                $about->about_image_url = env('APP_URL').'/uploads/about/'.$fileName;
                
            } 
            $about->save();
            $request->session()->flash('message.level', 'success');
            $request->session()->flash('message.content', 'Post is successfully Updated!'); 
        return redirect()->route('admin.dashboard'); 
    }

    

    public function destroy(Request $request,$id)
    {
       $about=About::destroy($id);
       $request->session()->flash('message.level', 'danger');
       $request->session()->flash('message.content', 'Aboutus Content is successfully Deleted!');
      return redirect()->route('admin.dashboard');
    }
}
