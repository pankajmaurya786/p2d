<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Framenumber;
use App\Model\Vehicel;
use App\Model\Geographical;
use App\Model\Year;
use App\Model\Modeld;
use DB;

class FrameController extends Controller
{
    

    public function index()
    {
        //
    }

    

    public function create()
    {
        $vehical = DB::table("vehicels")->pluck("name","id");
        $geographic =Geographical::all();
        $year =Year::all();
        $model =Modeld::all();
        return view('frame.create',compact('vehical','geographic','year','model'));
    }

    public function getcitylist(Request $request)
    {
        $states = DB::table("years")
                    ->where("geographical_id",$request->geographical_id)
                    ->pluck("year","id");
        return response()->json($states);
    }

    public function store(Request $request)
    {
        $frame = new Framenumber($request->all()); 
            $frame->save();
            $request->session()->flash('message.level', 'success');
            $request->session()->flash('message.content', 'Post was successfully Create!'); 
        return redirect()->route('admin.product');
    }

    
    
    public function show($id)
    {
       
    }


    public function edit($id)
    {
        $frame = Framenumber::findOrFail($id);
        return view('frame.edit',compact('frame'));
    }

    

    public function update(Request $request, $id)
    {
       $frame = Framenumber::findOrFail($id);
       $frame->update($request->all());
            $frame->save();
            $request->session()->flash('message.level', 'success');
            $request->session()->flash('message.content', 'Framenumber was successfully Updated!');
            return redirect()->route('admin.dashboard');  
    }

    

    public function destroy(Request $request,$id)
    {
       $frame=Framenumber::destroy($id);
       $request->session()->flash('message.level', 'danger');
       $request->session()->flash('message.content', 'Framenumber was successfully Deleted!');
      return redirect()->route('admin.dashboard');
    }
}
