<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Vinnumber;
use App\Model\Vehicel;
use App\Model\Geographical;
use App\Model\Year;
use App\Model\Modeld;
use App\Model\Framenumber;
use App\Model\Gradeengine;
use DB;

class VinnumberController extends Controller
{
    

    public function index()
    {
        //
    }

    

    public function create()
    {
        $vehical = DB::table("vehicels")->pluck("name","id");
        $geographic =Geographical::all();
        $year =Year::all();
        $model =Modeld::all();
        $frame =Framenumber::all();
        $grade =Gradeengine::all();
        return view('vinnumber.create',compact('vehical','geographic','year','model','frame','grade'));
    }

    public function getgradelist(Request $request)
    {
        $states = DB::table("framenumbers")
                    ->where("modeld_id",$request->modeld_id)
                    ->pluck("frame_number","id");
        return response()->json($states);
    }
    public function getframelist(Request $request)
    {
        $states = DB::table("gradeengines")
                    ->where("framenumber_id",$request->framenumber_id)
                    ->pluck("grade_number","id");
        return response()->json($states);
    }

    public function store(Request $request)
    {
        $vinnumber = new Vinnumber($request->all()); 
            $vinnumber->save();
            $request->session()->flash('message.level', 'success');
            $request->session()->flash('message.content', 'Post was successfully Create!'); 
        return redirect()->route('admin.product');
    }

    

    public function show($id)
    {
        
    }

    

    public function edit($id)
    {
        $vin = Vinnumber::findOrFail($id);
        return view('vinnumber.edit',compact('vin'));
    }

    

    public function update(Request $request, $id)
    {
       $vin = Vinnumber::findOrFail($id);
       $vin->update($request->all());
            $vin->save();
            $request->session()->flash('message.level', 'success');
            $request->session()->flash('message.content', 'Vinnumber was successfully Updated!');
            return redirect()->route('admin.dashboard');  
    }

    
    
    public function destroy(Request $request,$id)
    {
       $vinnumber=Vinnumber::destroy($id);
       $request->session()->flash('message.level', 'danger');
       $request->session()->flash('message.content', 'Vinnumber was successfully Deleted!');
      return redirect()->route('admin.dashboard');
    }
}
