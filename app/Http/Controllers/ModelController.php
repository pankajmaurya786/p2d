<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Modeld;
use App\Model\Vehicel;
use App\Model\Geographical;
use App\Model\Year;
use DB;

class ModelController extends Controller
{
    

    public function index()
    {
        //
    }

    

    public function create()
    {
        $vehical = DB::table("vehicels")->pluck("name","id");
        $geographic =Geographical::all();
        $year =Year::all();
        return view('model.create',compact('vehical','geographic','year'));
    }

    

    public function store(Request $request)
    {
        $models = new Modeld($request->all()); 
            $models->save();
            $request->session()->flash('message.level', 'success');
            $request->session()->flash('message.content', 'Post was successfully Create!'); 
        return redirect()->route('admin.product');
    }

    

    public function show($id)
    {
        //
    }

    

    public function edit($id)
    {
        $model = Modeld::findOrFail($id);
        return view('model.edit',compact('model'));
    }

    

    public function update(Request $request, $id)
    {
       $model = Modeld::findOrFail($id);
       $model->update($request->all());
            $model->save();
            $request->session()->flash('message.level', 'success');
            $request->session()->flash('message.content', 'Model was successfully Updated!');
            return redirect()->route('admin.dashboard');  
    }


    public function destroy(Request $request,$id)
    {
       $model=Modeld::destroy($id);
       $request->session()->flash('message.level', 'danger');
       $request->session()->flash('message.content', 'Modeld was successfully Deleted!');
      return redirect()->route('admin.dashboard');
    }
}
