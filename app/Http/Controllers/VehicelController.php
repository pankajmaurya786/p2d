<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Vehicel;

class VehicelController extends Controller
{
    

    public function index()
    {
        $vehicel = Vehicel::all();
        return view('vehicel.index');
    }

    

    public function create()
    {
       return view('vehicel.create'); 
    }

    

    public function store(Request $request)
    {
        $this->validate($request, [
                               'name'=>'required|unique:vehicels',
                                ]);
        $vehicel = new Vehicel($request->all()); 
        $slug  = str_slug($request->input('name'),'-');
        $vehicel->slug = $slug;
        
        if($file = $request->hasFile('product_icon')) 
            {
                $file = $request->file('product_icon') ;
                $fileName = "Vehicel".str_random(6)."_".$file->getClientOriginalName() ;
                $destinationPath=public_path()."/uploads/vehicel/";
                if (!is_dir($destinationPath)) 
                {
                  mkdir($destinationPath,0777,true);
                }

                $file->move($destinationPath,$fileName);
                $vehicel->product_icon = $fileName ;
                $vehicel->product_icon_url = env('APP_URL').'/uploads/vehicel/'.$fileName;
                
            } 
            $vehicel->save();
            $request->session()->flash('message.level', 'success');
            $request->session()->flash('message.content', 'Brand was successfully Create!'); 
        return redirect()->route('admin.product');
    }

    
    
    public function show($id)
    {
        
    }

    

    public function edit($id)
    {
        $vehicel = Vehicel::findOrFail($id);
        return view('vehicel.edit',compact('vehicel'));
    }

    

    public function update(Request $request, $id)
    {
       $vehicel = Vehicel::findOrFail($id);
       $slug  = str_slug($request->input('name'),'-');
       $vehicel->slug = $slug;
       $vehicel->update($request->all());
        if($file = $request->hasFile('product_icon')) 
            {
                $file = $request->file('product_icon') ;
                $fileName = "Vehicel".str_random(6)."_".$file->getClientOriginalName() ;
                $destinationPath=public_path()."/uploads/vehicel/";
                if (!is_dir($destinationPath)) 
                {
                  mkdir($destinationPath,0777,true);
                }

                $file->move($destinationPath,$fileName);
                $vehicel->product_icon = $fileName ;
                $vehicel->product_icon_url = env('APP_URL').'/uploads/vehicel/'.$fileName;
                
            } 
            $vehicel->save();
            $request->session()->flash('message.level', 'success');
            $request->session()->flash('message.content', 'Brand was successfully Create!'); 
        return redirect()->route('admin.product');
    }
    

    public function destroy(Request $request,$id)
    {
       $category=Vehicel::destroy($id);
       $request->session()->flash('message.level', 'danger');
       $request->session()->flash('message.content', 'Vehicel was successfully Deleted!');
      return redirect()->route('admin.dashboard');
    }
}
