<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Contact;

class ContactusController extends Controller
{
    

    public function index()
    {
        $contactus = Contact::all();
        return view('contact.index',compact('contactus'));
    }

    

    public function create()
    {
        //
    }

    

    public function store(Request $request)
    {
        //
    }

    

    public function show($id)
    {
        $contact = Contact::findOrFail($id);
        return view('contact.show',compact('contact'));
    }

    

    public function edit($id)
    {
        //
    }

    

    public function update(Request $request, $id)
    {
        //
    }

    

    public function destroy($id)
    {
        //
    }
}
