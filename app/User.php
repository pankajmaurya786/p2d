<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    
    protected $fillable = [
        'name', 'email', 'password','mobile','user_type','gender',
    ];

    
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function multiadd()
    {
        return $this->belongsTo('App\Model\UserAddres');
    }

    public function vendor()
    {
        return $this->hasOne('App\Model\Vendor');
    }

    public function useraddress()
    {
        return $this->hasOne('App\Model\UserAddres');
    }
    public function order()
    {
        return $this->belongsTo('App\Model\Order');
    }
}
