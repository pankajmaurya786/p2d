<div id="sidebar-wrapper" data-simplebar="" data-simplebar-auto-hide="true">
            <div class="brand-logo">
                <a href="{{route('vendor.dashboard')}}">
                    <img src="{{asset('images/logo-icon.png')}}" class="logo-icon" alt="logo icon">
                    <h5 class="logo-text">Vendor Dashboard</h5>
                </a>
            </div>
            <div class="user-details">
                <div class="media align-items-center user-pointer collapsed" data-toggle="collapse" data-target="#user-dropdown">
                    <div class="avatar"></div>
                    <div class="media-body">
                        <h6 class="side-user-name">{{Auth::user()->name}}</h6>
                    </div>
                </div>
                <div id="user-dropdown" class="collapse">
                    <ul class="user-setting-menu">
                        <li><a href="{{route('vendor.profile')}}"><i class="icon-user"></i> My Profile</a></li>
                        <li><a href="{{route('user.logout')}}"><i class="icon-power"></i> Logout</a></li>
                    </ul>
                </div>
            </div>
            <div class="topheader">
                <ul class="sidebar-menu do-nicescrol">
                    <li class="sidebar-header">MAIN NAVIGATION</li>
                    <li class="active">
                        <a href="{{route('vendor.dashboard')}}" class="waves-effect">
                            <i class="zmdi zmdi-view-dashboard"></i> <span style="color: white">Dashboard</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{route('product.create')}}" class="waves-effect">
                            <i class="zmdi zmdi-card-travel"></i>
                            <span> Add Products</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{route('product.index')}}" class="waves-effect">
                            <i class="zmdi zmdi-card-travel"></i>
                            <span> View Products</span>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="waves-effect">
                            <i class="zmdi zmdi-card-travel"></i>
                            <span> Customer Details</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{route('vendor.order')}}" class="waves-effect">
                            <i class="zmdi zmdi-invert-colors"></i> <span>Order Details</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>