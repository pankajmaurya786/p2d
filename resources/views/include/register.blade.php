@extends('layouts.main')

@section('content')

   <!--start loginhere-->
    <div class="section" id="login">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="login_articles">
                        <h2>
                            Growing Your Business
                        </h2>
                        <p>
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                            Lorem Ipsum has been the industry's standard dummy text ever since the
                            1500s, when an unknown printer took a galley of type and scrambled it to
                            make a type specimen book.
                        </p>
                        <img src="images/car.png" />
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="login_aside">
                        <img src="images/hr.png" />
                        <h2>Create your Account</h2>
                        @if(session()->has('message.level'))
    <div class="alert alert-{{ session('message.level') }}"> 
    {!! session('message.content') !!}
    </div>
@endif
                         @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
          @endif
                        {{ Form::open(array('route' => 'user.register','class'=>'form-horizontal')) }}
                            <div class="form-group">
                                <label>Full Name</label>
                                <input type="text" value="{{ old('name') }}" name="name" placeholder="Name">
                            </div>
                            <div class="form-group">
                                <label>Enter your Email Id</label>
                                <input type="email" name="email" value="{{ old('email') }}" placeholder="">
                            </div>
                            <div class="form-group">
                                <label>Gender</label>
                                <div class="col-md-2">
                                    <input type="radio"  name="gender"  value="male" checked="">Male
                                </div>
                                <div class="col-md-3">
                                    <input type="radio" name="gender"  value="female">Female
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Mobile Number</label>
                                <br>
                                <input id="phone" name="mobile" value="{{ old('mobile') }}" type="tel">
                                <!-----  <input type="Number" name="" placeholder="">---->
                            </div>
                            <!--<div class="form-group">
                                <label>OTP</label>
                                <div class="row">
                                    <div class="col-md-12">
                                        <form method="post" action="confirm" role="form">
                                            <div class="form-group form-group-lg">
                                                <input type="text" class="form-control" name="verifyCode" style="width: 70%;border-radius: 0px;box-shadow: none;margin-right: 10%;" required>
                                                <input class="btn btn-primary btn-lg" style="width: 20%;background-image: linear-gradient(to right, #375cff , #02a2ff);" type="submit" value="Verify">
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>-->
                            <div class="form-group" style="margin-bottom: 56px;">
                                <label>Create a password</label>
                                <input type="password" name="password" placeholder="">
                            </div>
                            <div class="form-group">
                                <label>Confirm your password</label>
                                <input type="password" name="confirmPassword" placeholder="">
                            </div>
                            <div class="form-group">
                               <button type="submit" class="btn btn-primary pull-right">Submit</button>
                            </div>
                            <div class="form-group" style="text-align: center;">
                                Already have an Account <a href="{{route('users.login')}}"><span>Log In</span></a>
                            </div>


                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--End Click Froud here-->


@endsection