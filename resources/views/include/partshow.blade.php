@extends('layouts.main')

@section('content')

   <div class="my">
        <div class="inside_ban_Wrap listing">
            <div class="container">

            </div>
        </div>
    </div>
    <!--End banner sec here-->
<style> 
    @if(app()->getLocale() == 'ar')

.dropdown.bootstrap-select {
    float: right!important;
}
.filter_div .search_btn {
    position: absolute;
    z-index: 1;
    top: 3px;
    background: transparent;
    border: none;
    padding-left: 52px;
    right: auto;
}
.filter-option-inner-inner {
    color: #333;
    font-weight: 500;
    float: right;
}
/* direction move*/

@endif
    .dropdown .dropdown-menu {
    padding: 20px;
    top: 30px !important;
    width: 350px !important;
    left: 0px !important;
    box-shadow: 0px 5px 30px black;
}i.fa.fa-search.custom_cs {
    margin-top: 10px;
}.dropdown {
    float: right;
    padding-right:0px!important;
    position: relative;

}.bootstrap-select > .dropdown-toggle {
    position: relative;
    width: 100%;
    z-index: 1;
    text-align: right;
    white-space: nowrap;
}.bootstrap-select:not(.input-group-btn), .bootstrap-select[class*="col-"] {
    float: none;
    display: inline-block;
    margin-left:none;
}.show {
    display: block!important;
}.bootstrap-select:not(.input-group-btn) {
    width: 70%!important;
    margin: 0 auto;
}


</style>




    <!--start Click Froud here-->
    <div class="section listing_details selection2">
        <div class="container">
            <div class="section_container">
                @foreach($showpart->take(1) as $products)
                @if(app()->getLocale() == 'en')<p>@lang('message.Vehicle')<img src="{{$products->vehicel->product_icon_url}}" style="width: 20px;">@lang('message.model') : {{$products->vehicel->name}} - {{$products->modeld->name}}</p>@else
                <p>@lang('message.Vehicle') <img src="{{$products->vehicel->product_icon_url}}" style="width: 20px;">@lang('message.model') : {{$products->vehicel->name_arabic}} - {{$products->modeld->name_arabic}}</p>@endif
                @endforeach
               <p>{{ __('message.Select Part Number') }}</p>
                <div class="row">
                    <div class="col-md-12">
                        <div class="filter_div">
                            <form method="get" action="{{route('search.productpart')}}">
                            @foreach($showpart->take(1) as $showparts)
                              <input type="hidden" name="search_type" value="{{$showparts->id}}">
                              <input type="hidden" id="vechile_id" name="vechile_id" value="{{$showparts->vehicel_id}}">
                            @endforeach

                            <div class="filter_div tag_serch">
                     <input type="text" id="add_part_number" name="part_number" placeholder="@lang('message.Example: Brake Pads...')" required="" />
                            <p>@lang('message.Search By: Part number / Vin Number / Frame Number')</p>
                        </div>
                        <div id="search_suggesstions"></div>
                        @foreach($showpart as $showparts)
                        <li><h3><a href="{{route('search.productpart',['search_type'=>$showparts->id])}}">{{$showparts->part_number}}</a></h3></li>
                        @endforeach
                            <button class="search_btn" @if(app()->getLocale()=='ar') style="padding-right:187px !important;" @endif>
                                <i class="fa fa-search custom_cs" aria-hidden="true"></i></a>
                            </button>
                            </form>
                        </div>
                    </div>


                </div>


            </div>
        </div>
    </div>
        

<script>
  

    $(document).ready(function(){
      
    $(document).on("keyup",'#add_part_number', function() {
            var key = $(this).val();
            var vechile_id = $('#vechile_id').val();
            // alert(vechile_id);
            $("#search_suggesstions").html("");
            // $("#success_message").html("");
            if (key != "") {
                var _token = "{{ csrf_token() }}";
                $.ajax({
                    url: "{{ route('autoCompleteSearch') }}",
                    method: "POST",
                    data: { query: key,vechile_id:vechile_id,_token: _token },
                    success: function(data) {
                        $("#search_suggesstions").fadeIn();
                        $("#search_suggesstions").html(data);
                        
                    }
                });
            }
        });

    $(document).on('click','.select_result',function(){
       var result =  $(this).text();
        $('#add_part_number').val(result);
        $("#search_suggesstions").hide();
    });
});



//     
</script>

@endsection