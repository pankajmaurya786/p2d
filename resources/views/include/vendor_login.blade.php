@extends('layouts.vendor')

@section('content')

   <div id="pageloader-overlay" class="visible incoming"><div class="loader-wrapper-outer"><div class="loader-wrapper-inner" ><div class="loader"></div></div></div></div>

 <div id="wrapper">

 <div class="loader-wrapper"><div class="lds-ring"><div></div><div></div><div></div><div></div></div></div>
    <div class="card card-authentication1 mx-auto my-5">
        <div class="card-body">
         <div class="card-content p-2">
            <div class="text-center">
@if(session()->has('message.level'))
    <div class="alert alert-{{ session('message.level') }}"> 
    {!! session('message.content') !!}
    </div>
 @endif
 @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
  @endif
                <img src="{{asset('images/logo-icon.png')}}" alt="logo icon">
            </div>
          <div class="card-title text-uppercase text-center py-3">Sign In</div>
            {{ Form::open(array('route' =>'vendor.loginpost','class'=>'form-horizontal')) }}
              <div class="form-group">
              <label for="exampleInputUsername" class="sr-only">Username</label>
               <div class="position-relative has-icon-right">
                  <input type="text" name="email" id="exampleInputUsername" class="form-control input-shadow" placeholder="Enter Username">
                  <div class="form-control-position">
                      <i class="icon-user"></i>
                  </div>
               </div>
              </div>
              <div class="form-group">
              <label for="exampleInputPassword" class="sr-only">Password</label>
               <div class="position-relative has-icon-right">
                  <input type="password" name="password" id="exampleInputPassword" class="form-control input-shadow" placeholder="Enter Password">
                  <div class="form-control-position">
                      <i class="icon-lock"></i>
                  </div>
               </div>
              </div>
            <div class="form-row">
             <div class="form-group col-6">
               <div class="icheck-material-white">
                <input type="checkbox" id="user-checkbox" checked="" />
                <label for="user-checkbox">Remember me</label>
              </div>
             </div>
             <div class="form-group col-6 text-right">
              <a href="authentication-reset-password.php">Reset Password</a>
             </div>
            </div>
             <button type="submit" class="btn btn-light btn-block">Sign In</button>
              <div class="text-center mt-3">Sign In With</div>
              
             
             
             </form>
           </div>
          </div>
          <div class="card-footer text-center py-3">
            <p class="text-warning mb-0">Do not have an account? <a href="{{route('vendor.register')}}"> Sign Up here</a></p>
          </div>
         </div>
    
     <!--Start Back To Top Button-->
    <a href="javaScript:void();" class="back-to-top"><i class="fa fa-angle-double-up"></i> </a>
    <!--End Back To Top Button-->
    
    <!--start color switcher-->
   <div class="right-sidebar">
    <div class="switcher-icon">
      <i class="zmdi zmdi-settings zmdi-hc-spin"></i>
    </div>
    <div class="right-sidebar-content">

      <p class="mb-0">Gaussion Texture</p>
      <hr>
      
      <ul class="switcher">
        <li id="theme1"></li>
        <li id="theme2"></li>
        <li id="theme3"></li>
        <li id="theme4"></li>
        <li id="theme5"></li>
        <li id="theme6"></li>
      </ul>

      <p class="mb-0">Gradient Background</p>
      <hr>
      
      <ul class="switcher">
        <li id="theme7"></li>
        <li id="theme8"></li>
        <li id="theme9"></li>
        <li id="theme10"></li>
        <li id="theme11"></li>
        <li id="theme12"></li>
      </ul>
      
     </div>
   </div>
  <!--end color cwitcher-->
    
    </div><!--wrapper-->
    
  

@endsection