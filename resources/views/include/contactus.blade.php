@extends('layouts.main')

@section('content')

<div class="my">
        <div class="inside_ban_Wrap seo">
            <div class="container">

                <div class="seo_flex">
                    <h1>Contact Us</h1>
                    <i class="fa fa-arrow-down custom_icons_color" aria-hidden="true"></i>
                </div>
                @if(session()->has('message.level'))
                <div class="alert alert-{{ session('message.level') }}"> 
                {!! session('message.content') !!}
                </div>
                @endif
            </div>
        </div>
    </div>
    <!--End banner sec here-->

    <!-- contact us  -->
    <div class="wrapper margin-top_custom">
        <div class="container-fluid">
            <div class="container">
                <div class="row panel panel-default panel-body">
                    <div class="col-md-6">
                        <div class="wow fadeInUp contact-info" data-wow-delay="0.4s">
                            <div class="section-title">

                                <h3 class="website_heading_font inhariteance_Color_heading">Contact Info</h3>

                                <p class="custome_p_career">Lorem ipsum dolor sit consectetur adipiscing morbi venenatis et tortor consectetur adipisicing lacinia tortor morbi ultricies.</p>
                                <hr>
                            </div>

                            <p class="custome_p_career"><i class="fa fa-map-marker"></i>456 New Street 22000, New York City, USA</p>
                            <p class="custome_p_career"><i class="fa fa-comment"></i><a href="mailto:info@company.com">info@company.com</a></p>
                            <p class="custome_p_career"><i class="fa fa-phone"></i>010-020-0340</p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        {{ Form::open(array('route' => 'contact.uspost','class'=>'row','files'=>true)) }}
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="name">First Name:</label>
                                    <input type="text" name="first_name" class="form-control" id="name" required="">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="lastname">Last Name:</label>
                                    <input type="text" name="last_name" class="form-control" id="lastname" required="">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="email">EmailID:</label>
                                    <input type="email" name="email" class="form-control" id="email" required="">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="tel">MobileNo:</label>
                                    <input type="tel" name="mobile" class="form-control" id="tel" required="" pattern="[789][0-9]{9}">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="message">Message</label>
                                    <textarea type="text" name="message" class="form-control" id="message" required=""></textarea>
                                </div>
                                <button type="submit" class="btn btn-default submit_button">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ended contact us -->
    <div class="wrapper padding-top">
        <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d14033.703948934164!2d77.55043015!3d28.4365724!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sin!4v1566037563714!5m2!1sen!2sin" width="100%" height="400" frameborder="0" style="border:0" allowfullscreen></iframe>
    </div>


@endsection