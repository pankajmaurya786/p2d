<!--Start Header sec here-->
    <div class="headerWrap headerInside"  >
        <nav class="navbar navbar-inverse navbar-fixed-top">
            <div class="container">
                <div class="customNav">
                    <div class="navbar-header " @if(app()->getLocale() == 'en') lang="en" dir="ltr" @else dir="rtl" style="float:right;" @endif >
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="{{url('/')}}">
                            <img src="{{asset('images/logo.png')}}" border="0" alt="">
                        </a>
                    </div>
                    <div id="navbar" class="navbar-collapse collapse navbar-right">
                        <ul class="nav navbar-nav">
                            <button type="button" class="btn btn-info" data-toggle="dropdown" style="margin-left: 20px;">
                    <i class="fa fa-shopping-cart" aria-hidden="true"></i> @lang('message.Cart') <span class="badge badge-pill badge-danger">{{ count((array) session('cart')) }}</span>
                </button>
                <div class="dropdown-menu custom_drop">
                    <div class="row total-header-section">
                        <div class="col-lg-6 col-sm-6 col-6">
                            <i class="fa fa-shopping-cart custom_cart" aria-hidden="true"></i> <span class="badge badge-pill badge-danger custom_danger">{{ count((array) session('cart')) }}</span>
                        </div>
 
                        <?php $total = 0 ?>
                        @foreach((array) session('cart') as $id => $details)
                            <?php $total += $details['price'] * $details['quantity'] ?>
                        @endforeach
 
                        <div class="col-lg-6 col-sm-6 col-6 total-section text-right">
                            <p class="custom_pp">@lang('message.Total'): <span class="text-info">$ {{ $total }}</span></p>
                        </div>
                    </div>
 
                    @if(session('cart'))
                        @foreach(session('cart') as $id => $details)
                                                    <div class="row cart-detail">
                                <div class="col-lg-4 col-sm-4 col-4 cart-detail-img">
                                    <img src="{{ $details['photo'] }}" width="82px" height="108px" />
                                </div> 
                                <div class="col-lg-8 col-sm-8 col-8 cart-detail-product">

                                    <p>@if(app()->getLocale() == 'en'){{ $details['part_name'] }} @else {{ $details['part_name_arabic'] }} @endif</p>
                                    <span class="price text-info"> ${{ $details['price'] }}</span> <span class="count"> @lang('message.Quantity'):{{ $details['quantity'] }}</span>
                                </div>
                            </div>
                        @endforeach
                    @endif
                    <div class="row">
                        <div class="col-lg-12 col-sm-12 col-12 text-center checkout">
                            <a href="{{ url('cart') }}" class="btn btn-primary btn-block ">@lang('message.View all')</a>
                        </div>
                    </div>
                </div>
                            <li>
                                @if(Auth::guest())
                                <a href="{{route('users.login')}}" class="login">@lang('message.Login')</a>
                                @else
                                <a href="{{route('user.dashboard')}}" class="login">{{Auth::user()->name}}</a>
                                @endif
                            </li>

                            <li>
                                <a href="#">
                                    <div id="myNav" class="overlay">
                                        <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
                                        <div class="overlay-content">
                                            <a href="{{route('home')}}">Home</a>
                                            <a href="{{route('about.us')}}">About</a>
                                            <!--<a href="VehicleBrands.html">Vehicle Brands</a>
                                            <a href="Services.html">Services</a>
                                            <a href="Clients.html">Clients</a>-->
                                            <a href="{{route('contact.us')}}">Contact</a>
                                        </div>
                                    </div>
                                    <span class="navbar_custom" style="cursor:pointer" onclick="openNav()">
                                        <img src="{{asset('images/bar.png')}}" />
                                    </span>
                                </a>
                            </li>

                           
                        </ul>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </nav>
    </div>
    <!--End Header sec here-->
        <style>
    .dropdown-menu.custom_drop {
    padding: 39px;
    width: 21%;
    top: 70px;
}i.fa.fa-shopping-cart.custom_cart {
    padding-bottom: 15px;
    font-size: 22px;
}span.badge.badge-pill.badge-danger.custom_danger {
    font-size: 15px;
    margin-left: 5px;
    margin-bottom: 5px;
}p.custom_pp {
    font-size: 18px;
    color: #001526;
}.checkout .btn-primary {
    border-radius: 50px;
    height: 31px!important;
}
</style>