@extends('layouts.main')

@section('content')

    <!--start loginhere-->
    <div class="section" id="login">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="login_articles">
                        <h2>
                            @lang('message.Growing Your Business')
                        </h2>
                        <p>
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                            Lorem Ipsum has been the industry's standard dummy text ever since the
                            1500s, when an unknown printer took a galley of type and scrambled it to
                            make a type specimen book.
                        </p>
                        <img src="images/car.png" />
                    </div>
                </div>
                <div class="col-md-6">
                    @if(session()->has('message.level'))
                         <div class="alert alert-{{ session('message.level') }}"> 
                              {!! session('message.content') !!}
                          </div>
                    @endif
                    <div class="login_aside">
                        <img src="images/hr.png" />
                        <h2>@lang('message.Login')</h2>
                        {{ Form::open(array('route' => 'user.login','class'=>'form-horizontal')) }}
                            <div class="form-group" style="height: 95px;">
                                <!-- <input type="Number" name="" placeholder="Mobile Number"> -->

                                <section class="forms" style="margin-left:18px;">
                                    <div class="row">
                                        <input type="text" name="email" placeholder="Email">
                                    </div>
                                    <link rel='stylesheet' href='css/rewardcard.min.css'>
                                </section>
                                <!-- <img src="images/username.png"/> -->
                            </div>
                            <div class="form-group">
                                <input type="password" name="password" placeholder="Password">
                                <!-- <img src="images/password.png"/> -->
                            </div>
                            <div class="form-group" style="height: auto;">
                                <style type="text/css">
                                    /* Base for label styling */
                                    [type="checkbox"]:not(:checked),
                                    [type="checkbox"]:checked {
                                        position: absolute;
                                        left: -9999px;
                                    }

                                    [type="checkbox"]:not(:checked)+label,
                                    [type="checkbox"]:checked+label {
                                        position: relative;
                                        padding-left: 1.95em;
                                        cursor: pointer;
                                    }

                                    /* checkbox aspect */
                                    [type="checkbox"]:not(:checked)+label:before,
                                    [type="checkbox"]:checked+label:before {
                                        content: '';
                                        position: absolute;
                                        left: 0;
                                        top: 0;
                                        width: 1.45em;
                                        height: 1.35em;
                                        border: 2px solid #ccc;
                                        background: #fff;
                                        border-radius: 4px;
                                        box-shadow: inset 0 1px 3px rgba(0, 0, 0, .1);
                                    }

                                    /* checked mark aspect */
                                    [type="checkbox"]:not(:checked)+label:after,
                                    [type="checkbox"]:checked+label:after {
                                        content: '\2713\0020';
                                        position: absolute;
                                        top: .15em;
                                        left: .22em;
                                        font-size: 1.3em;
                                        line-height: 0.8;
                                        color: #09ad7e;
                                        transition: all .2s;
                                        font-family: 'Lucida Sans Unicode', 'Arial Unicode MS', Arial;
                                    }

                                    /* checked mark aspect changes */
                                    [type="checkbox"]:not(:checked)+label:after {
                                        opacity: 0;
                                        transform: scale(0);
                                    }

                                    [type="checkbox"]:checked+label:after {
                                        opacity: 1;
                                        transform: scale(1);
                                    }

                                    /* disabled checkbox */
                                    [type="checkbox"]:disabled:not(:checked)+label:before,
                                    [type="checkbox"]:disabled:checked+label:before {
                                        box-shadow: none;
                                        border-color: #bbb;
                                        background-color: #ddd;
                                    }

                                    [type="checkbox"]:disabled:checked+label:after {
                                        color: #999;
                                    }

                                    [type="checkbox"]:disabled+label {
                                        color: #aaa;
                                    }

                                    /* accessibility */
                                    [type="checkbox"]:checked:focus+label:before,
                                    [type="checkbox"]:not(:checked):focus+label:before {
                                        border: 2px dotted blue;
                                    }

                                    /* hover style just for information */
                                    label:hover:before {
                                        border: 2px solid #4778d9 !important;
                                    }

                                    .txtcenter {
                                        margin-top: 4em;
                                        font-size: .9em;
                                        text-align: center;
                                        color: #aaa;
                                    }

                                    .copy {
                                        margin-top: 2em;
                                    }

                                    .copy a {
                                        text-decoration: none;
                                        color: #4778d9;
                                    }

                                    a.forget {
                                        float: right;
                                        font-weight: 600;
                                    }
                                </style>
                                <p>
                                    <input type="checkbox" id="test1" />
                                    <label for="test1" style="padding-top: 0px;">@lang('message.Remember Me')</label>
                                    <a href="#" class="forget">@lang('message.Forget password') ?</a>
                                </p>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-default login_submit">@lang('message.Login')</button>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6 col-md-offset-3">
                                        <a href="{{route('users.register')}}" class="new_account">@lang('message.Sign Up Parts 2 Door')</a>
                                    </div>
                                </div>
                            </div>
                            
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--End Click Froud here-->


@endsection