@extends('layouts.main')

@section('content')

<div class="section checkoutt">
        <div class="container">
            <div class="row">
                
           @if(session()->has('message.level'))
    <div class="alert alert-{{ session('message.level') }}"> 
    {!! session('message.content') !!}
    </div>
 @endif
        </div>
    </div>
</div>
 <div class="section checkoutt">
        <div class="container">
            <div class="row">
                @if(Auth::guest())
                
 <div class="atbd_notice alert alert-info" role="alert">
                                <span class="la la-info" aria-hidden="true"></span>
                                You need to <a href="{{route('user.login')}}">Login</a> or <a href="{{route('users.register')}}">Register</a> to checkout your order
                               </div><!-- ends: .atbd_notice -->                      
                             @else
                <div class="col-md-6 delivery_123" style="margin-bottom: 10px; margin-top: 10px;">
                    <h1>@lang('message.Select Delivery Address')</h1>
                </div>
                <div class="col-md-6">
                    <a class="btn btn-info pull-right addaddress" href="{{route('user.dashboard')}}" style="border-radius: 7px; margin-top: 27px;">@lang('message.Add New Address')
                    </a>
                </div>
            </div>

@if(Auth::user()->useraddress==null)
                <div class="atbd_notice alert alert-info" role="alert">
                                <span class="la la-info" aria-hidden="true"></span>
                                You need to add your address <a href="{{route('user.dashboard')}}">Add New Address</a> to checkout your order
                               </div><!-- ends: .atbd_notice -->  
                @else
            <div class="row">
                {{ Form::open(array('route' => 'check.post','class'=>'form-horizontal','id'=>'atbdp_review_form')) }}
                @foreach((array) session('cart') as $id => $details)
                            <input type="hidden" name="vendor_id" value="{{ $details['vendor'] }}">
                            <input type="hidden" name="product_id" value="{{ $id }}">
                            <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
                            <input type="hidden" name="addpart_id" value="{{ $details['addpart'] }}">
                        @endforeach
                    <div class="col-md-8 addTab">
                        <table class="table">
                            <tbody>
                                <tr>
                                    <th>@lang('message.Name')</th>
                                    <th>@lang('message.Mobile')</th>
                                    <th>@lang('message.Address')</th>
                                    <th>@lang('message.Postal Code')</th>
                                    <th>@lang('message.Landmark')</th>
                                    <th>@lang('message.City')</th>
                                    <th>@lang('message.State')</th>
                                    <th>@lang('message.Edit')</th>
                                    <th>@lang('message.Select Address')</th>
                                </tr>
                            </tbody>
                            <tbody>
                                <tr>
                                    @if(!empty($allUserAddress))
                                        @foreach($allUserAddress as $user)
                                        <td>{{Auth::user()->name}}</td>
                                        <td>{{Auth::user()->mobile}}</td>
                                        <td>@if(isset($user->address)){{$user->address}}@endif</td>
                                        <td>@if(isset($user->pin_code)){{$user->pin_code}}@endif</td>
                                        <td>@if(isset($user->locality)){{$user->locality}}@endif</td>
                                        <td>@if(isset($user->city)){{$user->city}}@endif</td>
                                        <td>@if(isset($user->states)){{$user->states}}@endif</td>
                                        <td>
                                            <a class="edit" href=""><i class="fa fa-pencil" aria-hidden="true"></i> @lang('message.Edit')</a>
                                        </td>
                                        <td>
                                            <input type="radio" name="deliveryaddress" value="1" required="">
                                        </td>
                                    </tr>
                                  @endforeach
                                @endif
                               
                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-4 order-right">
                        <h4>@lang('message.ORDER DETAILS')</h4>
                        <div class="pricing-list" style="border: 1px solid #d8ddf5;">
                            <table class="table">
                                <thead>
                                    <tr style="background-color: #dadff7;">
                                        <th>@lang('message.Product')</th>
                                        <th style="padding-left: 60px;">@lang('message.Quantity')</th>
                                        <th>@lang('message.Price')</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <?php $total = 0 ?>
 
        @if(session('cart'))
            @foreach(session('cart') as $id => $details)
 
                <?php $total += $details['price'] * $details['quantity'] ?>
                                        <td>@if(app()->getLocale() == 'en'){{ $details['part_name'] }} @else {{ $details['part_name_arabic'] }}  @endif</td>
                                        <td style="padding-left: 70px;">{{ $details['quantity'] }}</td>
                                        <td> ${{ $details['price'] }}</td>
                                        @endforeach
                                        @endif
                                        
                                    </tr>
                                </tbody>
                            </table>
        <?php $total = 0 ?>
                        @foreach((array) session('cart') as $id => $details)
                            <?php $total += $details['price'] * $details['quantity'] ?>
                        @endforeach
 
                            <table class="table">
                                <tbody>
                                    <td>
                                        <h6 class="sub-total" style="margin: 0">@lang('message.Subtotal'):</h6>
                                    </td>
                                    <td width="43%">
                                        <h6 class="sub-total" style="margin: 0;text-align: center;
    padding-left: 15px;">$ {{ $total }}</h6>
                                    </td>
                                    
                                </tbody>
                                <tbody>
                                    <tr>

                                        <td>
                                            <h3 class="pricing-count" style="margin: 0">@lang('message.Total Cost'):</h3>
                                        </td>
                                        <td width="30%">
                                            <h3 class="pricing-count" style="margin: 0;text-align: center;
    padding-left: 20px;">$ {{ $total }}</h3>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <br>
                        <div class="form-group">
                            <label>@lang('message.Payment Method')<span>*</span></label>
                            <div class="form-group">
                                <select class="form-control" onchange="meThods(this)" id="formac" name="method" required="">
                                    
                                    <option value="Cash">@lang('message.Cash On Delivery')</option>
                                </select>
                            </div>
                        </div>

                        <button type="submit" class="btn btn-success btn-block">@lang('message.Order Now')</button>

                    </div>

                </form>
@endif 
            </div>

@endif

        </div>
    </div>
                  


@endsection