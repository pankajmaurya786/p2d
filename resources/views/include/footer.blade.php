 <!--start Click Froud here-->
    <div class="footer">
        <div class="container">
            <div class="row">
                <div class="col-md-4" @if(app()->getLocale() == 'en') lang="en" dir="ltr" @else lang="ar" dir="rtl" style="float:right;"  @endif>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur
                        adipisicing elit, sed do eiusmod tempor
                        incididunt ut labore et dolore magna aliqua.
                        Ut enim ad minim veniam, quis nostrud exercitation
                        ullamco laboris nisi ut aliquip ex ea commodo consequat.
                    </p>
                    <div class="social_icons">
                        <ul>
                            <li>
                                <a href="#">
                                    <i class="fa fa-facebook" aria-hidden="true"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="fa fa-twitter" aria-hidden="true"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="fa fa-google-plus" aria-hidden="true"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="fa fa-facebook" aria-hidden="true"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="fa fa-facebook" aria-hidden="true"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="fa fa-facebook" aria-hidden="true"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-2" @if(app()->getLocale() == 'en') lang="en" dir="ltr" @else lang="ar" dir="rtl" style="float:right;"  @endif>
                    <h2>{{ __('message.More Links') }}</h2>
                    <ul>
                        <li><a href="{{route('about.us')}}" style="color: white">@lang('message.about_us')</a></li>
                        <li><a href="{{route('contact.us')}}" style="color: white">@lang('message.Contact Us')</a></li>
                        <li>@lang('message.Portfolio')</li>
                        <li>@lang('message.Car Parts')</li>
                        <li><a href="{{route('vendor.login')}}" style="color: white">@lang('message.Vendor Login')</a></li>
                    </ul>
                </div>
                <div class="col-md-4" @if(app()->getLocale() == 'en') lang="en" dir="ltr" @else lang="ar" dir="rtl" style="float:right;"  @endif>
                    <h2>@lang('message.Quick Links')</h2>
                    <ul>
                        <li><a href="{{ url('locale/en') }}">English</a></li>
                        <li><a href="{{ url('locale/ar') }}">Arabic</a></li>
                        <li>@lang('message.FAQ')</li>
                        <li>@lang('message.Contact us')</li>
                        <li>@lang('message.Terms & Conditions of Use')</li>
                    </ul>
                </div>
                <div class="col-md-2" @if(app()->getLocale() == 'en') lang="en" dir="ltr" @else lang="ar" dir="rtl" style="float:right;"  @endif>
                    <h2>@lang('message.Location')</h2>
                    <ul>
                        <li>@lang('message.USA')</li>
                        <li>@lang('message.UK')</li>
                        <li>@lang('message.INDIA')</li>
                        <li>@lang('message.SRI LANKA')</li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="copyright">
                    @lang('message.Copyright By@ - 2019 Good Car company . All Rights Reserved')
                </div>
            </div>
        </div>
    </div>
    <!--End Click Froud here-->