@extends('layouts.main')

@section('content')

<div class="my">
        <div class="inside_ban_Wrap seo">
            <div class="container">
                <div class="seo_flex">
                    <h1>Know AboutUs</h1>
                    @foreach($aboutus as $about)
                    <p class="custome_p_career">{{$about->about_content}}. </p>
                    @endforeach
                </div>
            </div>
        </div>
    </div>


    <div class="wrapper padding_bottom">
        <div class="container-fluid">
            <div class="container">
                <div class="row panel panel-default panel-body">
                    @foreach($aboutus as $about)
                    <div class="col-md-6">
                        <h1>Know AboutUs</h1>
                        
                        <p class="custome_p_career">{{$about->about_content}}. </p>
                        
                    </div>
                    <div class="col-md-6 fixed_Height">
                        <img src="{{$about->about_image_url}}" class="about_us_img">
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>


@endsection