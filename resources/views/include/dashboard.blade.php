@extends('layouts.main')

@section('content')
<style type="text/css">
        .group {
            position: relative;
            margin-top: 28px;
            width: 50%;
            float: left;
        }

        input {
            font-size: 18px;
            padding: 10px 10px 10px 5px;
            display: block;
            width: 300px;
            border: none;
            border-bottom: 1px solid #757575;
        }

        input:focus {
            outline: none;
        }

        /* LABEL ======================================= */
        label {
            color: #999;
            font-size: 14px;
            font-weight: normal;
            position: absolute;
            pointer-events: none;
            left: 5px;
            top: 18px;
            transition: 0.2s ease all;
            -moz-transition: 0.2s ease all;
            -webkit-transition: 0.2s ease all;
        }

        /* active state */
        input:focus~label,
        input:valid~label {
            top: -20px;
            font-size: 14px;
            color: #5264AE;
        }

        /* BOTTOM BARS ================================= */
        .bar {
            position: relative;
            display: block;
            width: 300px;
        }

        .bar:before,
        .bar:after {
            content: '';
            height: 2px;
            width: 0;
            bottom: 1px;
            position: absolute;
            background: #5264AE;
            transition: 0.2s ease all;
            -moz-transition: 0.2s ease all;
            -webkit-transition: 0.2s ease all;
        }

        .bar:before {
            left: 50%;
        }

        .bar:after {
            right: 50%;
        }

        /* active state */
        input:focus~.bar:before,
        input:focus~.bar:after {
            width: 50%;
        }

        /* HIGHLIGHTER ================================== */
        .highlight {
            position: absolute;
            height: 60%;
            width: 100px;
            top: 25%;
            left: 0;
            pointer-events: none;
            opacity: 0.5;
        }

        /* active state */
        input:focus~.highlight {
            -webkit-animation: inputHighlighter 0.3s ease;
            -moz-animation: inputHighlighter 0.3s ease;
            animation: inputHighlighter 0.3s ease;
        }

        /* ANIMATIONS ================ */
        @-webkit-keyframes inputHighlighter {
            from {
                background: #5264AE;
            }

            to {
                width: 0;
                background: transparent;
            }
        }

        @-moz-keyframes inputHighlighter {
            from {
                background: #5264AE;
            }

            to {
                width: 0;
                background: transparent;
            }
        }

        @keyframes inputHighlighter {
            from {
                background: #5264AE;
            }

            to {
                width: 0;
                background: transparent;
            }
        }

        @media only screen and (min-width:320px) and (max-width:480px) {
            input {
                width: 94%;
            }

            .bar {
                width: 100%;
            }
        }
    </style>
    <div class="section checkoutt">
        <div class="container">
            <div class="row">
                
           @if(session()->has('message.level'))
    <div class="alert alert-{{ session('message.level') }}"> 
    {!! session('message.content') !!}
    </div>
 @endif
        </div>
    </div>
</div>
<div class="dashboard">
        <div class="container">
            <div class="row">
                <div class="col-md-2">
                    <ul class="nav nav-pills">
                        <li><a data-toggle="pill" href="#menu4">Profile</a></li>
                        <li class="active"><a data-toggle="pill" href="#menu3">Address</a></li>
                        <li><a data-toggle="pill" href="#home">Invoice</a></li>
                        <li><a data-toggle="pill" href="#menu1">Tracking</a></li>
                        <li><a data-toggle="pill" href="#menu2">Orders</a></li>                        
                        <li><a data-toggle="pill" href="#menu5">Support</a></li>
                        <li><a href="{{route('user.logout')}}"><i class="fa fa-sign-out" aria-hidden="true"></i>Logout</a></li>
                    </ul>
                </div>
                <div class="col-md-10">
                    <div class="tab-content">
                        <div id="home" class="tab-pane fade">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="invoice_generate">
                                        <h1>INVOICE GENERATE</h1>
                                        @if(session()->has('message.level'))
                                   <div class="alert alert-{{ session('message.level') }}"> 
                                     {!! session('message.content') !!}
                                     </div>
                                   @endif
                                        <form>
                                            <ul>
                                                <li>
                                                    <span>Invoice Number</span>
                                                    <strong><input type="number" name="" placeholder="invoice number"></strong>
                                                </li>
                                                <li>
                                                    <span>Purchase Date</span>
                                                    <strong><input type="datetime-local" name="bdaytime">
                                                        <input type="submit" value="Send" style="border: #a9a9a9 solid 1px; margin-top: 15px;" required="">
                                                    </strong>
                                                </li>
                                            </ul>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="well">
                                        <div class="invoice-title">
                                            <h2>Invoice</h2>
                                            <h3 class="pull-right">Number # 12345</h3>
                                        </div>
                                        <hr>
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <address>
                                                    <strong>Order Status:</strong><br>
                                                </address>
                                            </div>
                                            <div class="col-xs-6 text-right">
                                                <address>
                                                    <button type="button" class="order_status">Order being processed</button>
                                                </address>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <address>
                                                    <strong>Billed To:</strong><br>
                                                    John Smith<br>
                                                    1234 Main<br>
                                                    Apt. 4B<br>
                                                    Springfield, ST 54321
                                                </address>
                                            </div>
                                            <div class="col-xs-6 text-right">
                                                <address>
                                                    <strong>Shipped To:</strong><br>
                                                    Jane Smith<br>
                                                    1234 Main<br>
                                                    Apt. 4B<br>
                                                    Springfield, ST 54321
                                                </address>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <address>
                                                    <strong>Shiping Tracking No.</strong><br>
                                                    65413S6578<br>
                                                </address>
                                            </div>
                                            <div class="col-xs-6 text-right">
                                                <address>
                                                    <strong>Order Date:</strong><br>
                                                    March 7, 2014<br><br>
                                                </address>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h3 class="panel-title"><strong>Order summary</strong></h3>
                                        </div>
                                        <div class="panel-body">
                                            <div class="table-responsive">
                                                <table class="table table-condensed">
                                                    <thead>
                                                        <tr>
                                                            <td><strong>Item</strong></td>
                                                            <td class="text-center"><strong>Number</strong></td>
                                                            <td class="text-center"><strong>Description</strong></td>
                                                            <td class="text-center"><strong>Price</strong></td>
                                                            <td class="text-center"><strong>Quantity</strong></td>
                                                            <td class="text-right"><strong>Totals SAR</strong></td>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <!-- foreach ($order->lineItems as $line) or some such thing here -->
                                                        <tr>
                                                            <td>Hyndai</td>
                                                            <td class="text-center">BS-200</td>
                                                            <td class="text-center">Grille Assy</td>
                                                            <td class="text-center">$10.99</td>
                                                            <td class="text-center">1</td>
                                                            <td class="text-right">$10.99 </td>
                                                        </tr>
                                                        <tr>
                                                            <td>Honda</td>
                                                            <td class="text-center">BS-400</td>
                                                            <td class="text-center">Grille Assy</td>
                                                            <td class="text-center">$20.00</td>
                                                            <td class="text-center">3</td>
                                                            <td class="text-right">$60.00 </td>
                                                        </tr>
                                                        <tr>
                                                            <td>Hyndai</td>
                                                            <td class="text-center">BS-600</td>
                                                            <td class="text-center">Grille Assy</td>
                                                            <td class="text-center">$600.00</td>
                                                            <td class="text-center">1</td>
                                                            <td class="text-right">$600.00 </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="thick-line"></td>
                                                            <td class="thick-line"></td>
                                                            <td class="thick-line"></td>
                                                            <td class="thick-line"></td>
                                                            <td class="thick-line text-center"><strong>Subtotal</strong></td>
                                                            <td class="thick-line text-right">$670.99 </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="no-line"></td>
                                                            <td class="no-line"></td>
                                                            <td class="no-line"></td>
                                                            <td class="no-line"></td>
                                                            <td class="no-line text-center"><strong>Shipping</strong></td>
                                                            <td class="no-line text-right">$15 </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="no-line"></td>
                                                            <td class="no-line"></td>
                                                            <td class="no-line"></td>
                                                            <td class="no-line"></td>
                                                            <td class="no-line text-center"><strong>Total</strong></td>
                                                            <td class="no-line text-right">$685.99 </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="icons_print">
                                    <div class="col-md-3 col-xs-3">
                                        <a href="#">
                                            <img src="images/print.png" />
                                        </a>
                                    </div>
                                    <div class="col-md-4 col-xs-4">
                                        <button type="button" class="order_status">Order Details</button>
                                    </div>
                                    <div class="col-md-5 col-xs-4">
                                        <button type="button" class="order_status">Source Invoice</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="menu1" class="tab-pane fade">
                            <div class="row shop-tracking-status">
                                <div class="col-md-12">
                                    <div class="well">
                                        <div class="form-horizontal">
                                            <div class="form-group">
                                                <label for="inputOrderTrackingID" class="col-sm-2 col-xs-4 control-label order-id" style="font-size: 2rem;
    margin-top: -14px;">Order id</label>
                                                <div class="col-sm-10 col-xs-8">
                                                    <input type="text" class="form-control" id="inputOrderTrackingID" value="" placeholder="# put your order id here">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-offset-2 col-sm-10">
                                                    <button type="button" id="shopGetOrderStatusID" class="btn btn-success order_status">Get status</button>
                                                </div>
                                            </div>
                                        </div>
                                        <h4>Your order status:</h4>
                                        <ul class="list-group">
                                            <li class="list-group-item">
                                                <span class="prefix">Date created:</span>
                                                <span class="label label-success order_status">12.12.2013</span>
                                            </li>
                                            <li class="list-group-item">
                                                <span class="prefix">Last update:</span>
                                                <span class="label label-success order_status">12.15.2013</span>
                                            </li>
                                            <li class="list-group-item">
                                                <span class="prefix">Comment:</span>
                                                <span class="label label-success order_status">customer's comment goes here</span>
                                            </li>
                                            <li class="list-group-item">You can find out latest status of your order with the following link:</li>
                                            <li class="list-group-item"><a href="//tracking/link/goes/here">//tracking/link/goes/here</a></li>
                                        </ul>
                                        <div class="order-status">
                                            <div class="order-status-timeline">
                                                <!-- class names: c0 c1 c2 c3 and c4 -->
                                                <div class="order-status-timeline-completion c3"></div>
                                            </div>
                                            <div class="image-order-status image-order-status-new active img-circle">
                                                <span class="status">Accepted</span>
                                                <div class="icon"></div>
                                            </div>
                                            <div class="image-order-status image-order-status-active active img-circle">
                                                <span class="status">In progress</span>
                                                <div class="icon"></div>
                                            </div>
                                            <div class="image-order-status image-order-status-intransit active img-circle">
                                                <span class="status">Shipped</span>
                                                <div class="icon"></div>
                                            </div>
                                            <div class="image-order-status image-order-status-delivered active img-circle">
                                                <span class="status">Delivered</span>
                                                <div class="icon"></div>
                                            </div>
                                            <div class="image-order-status image-order-status-completed active img-circle">
                                                <span class="status">Completed</span>
                                                <div class="icon"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="menu2" class="tab-pane fade">
                            <div class="">
                                <div class="table-wrapper">
                                    <div class="table-title">
                                        <div class="row">
                                            <div class="col-sm-5">
                                                <h2>User <b>Order Management</b></h2>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="product_filter">
                                            <strong>{{ count($order) }} orders</strong> placed in
                                            <span>
                                                <select>
                                                    <option>2019</option>
                                                    <option>2018</option>
                                                    <option>2017</option>
                                                </select>
                                            </span>
                                        </div>
                                    </div>
                                    @foreach($order as $orders)
                                    <div class="row">
                                        <div class="order-list">
                                            <div class="order-list-top">
                                                <div class="col-md-2">
                                                    ORDER PLACED<br />
                                                    <span>{!! date('d M, Y',strtotime($orders->created_at)) !!}</span>
                                                </div>
                                                <div class="col-md-2">
                                                    TOTAL<br />
                                                    <span> ${{$orders->product->price}}</span>
                                                </div>
                                                <div class="col-md-2">
                                                    SHIP TO<br />
                                                    <span>
                                                        <div class="dropdown">
                                                            {{Auth::user()->name}}<button class="dropbtn"> <i class="fa fa-angle-down" aria-hidden="true"></i></button>
                                                            <div class="dropdown-content">
                                                                <i class="fa fa-caret-up" aria-hidden="true"></i>
                                                                <a href="#">
                                                                    <p>{{Auth::user()->name}}</p>
                                                                    <p>@if(isset(Auth::user()->useraddress->address)){{Auth::user()->useraddress->address}}@endif</p>
                                                                    <p>Sector 2</p>
                                                                    <p>NOIDA, UTTAR PRADESH</p>
                                                                    <p>India</p>
                                                                    <p>Phone: {{Auth::user()->mobile}}</p>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </span>
                                                </div>
                                                <div class="col-md-6" style="text-align: right;">
                                                    ORDER # 408-9641700-5267522<br />
                                                    <span class="pull-right"><a href="#">Order Details Invoice</a></span>
                                                </div>
                                            </div>
                                            <div class="order-list-bottom">
                                                <div class="">
                                                    <div class="col-md-2">
                                                        <img src="images/product.png" class="avatar" alt="Avatar">
                                                    </div>
                                                    <div class="col-md-10">
                                                        <a class="a-link-normal" href="/gp/product/B07F8XW4VK/ref=ppx_yo_dt_b_asin_title_o00_s00?ie=UTF8&amp;psc=1">
                                                          <p><span>Part Name:</span>   @isset($orders->addpart->part_name){{$orders->addpart->part_name}}@endif
                                                        </a></p>
                                                        <p><span>Sold by:</span> {{$orders->vendor->company_name}}</p>
                                                        <p><span>Price:</span>${{$orders->product->price}}</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                    
                                    
                                    <!--<div class="clearfix">
                                        <div class="hint-text">Showing <b>5</b> out of <b>25</b> entries</div>
                                        <ul class="pagination">
                                            <li class="page-item disabled"><a href="#">Previous</a></li>
                                            <li class="page-item"><a href="#" class="page-link">1</a></li>
                                            <li class="page-item"><a href="#" class="page-link">2</a></li>
                                            <li class="page-item active"><a href="#" class="page-link">3</a></li>
                                            <li class="page-item"><a href="#" class="page-link">4</a></li>
                                            <li class="page-item"><a href="#" class="page-link">5</a></li>
                                            <li class="page-item"><a href="#" class="page-link">Next</a></li>
                                        </ul>
                                    </div>-->
                                </div>
                            </div>
                        </div>
                        <div id="menu3" class="tab-pane fade in active">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="well">
                                        <div class="invoice-title">
                                            <h2>Manage Addresses</h2>
                                        </div>
                                        <hr>
                                        <div class="add_new_address">
                                            <div class="add_new_address_boxes">
                                                <h1>ADD A NEW ADDRESS</h1>
                                                {{ Form::open(array('route' => 'user.address','class'=>'form-horizontal')) }}
                                                <input type="hidden" name="user_id" value="{{Auth::user()->id}}">
                                                    <div class="group">
                                                        <input type="text" required value="{{Auth::user()->name}}">
                                                        <span class="highlight"></span>
                                                        <span class="bar"></span>
                                                        <label>Name</label>
                                                    </div>

                                                    <div class="group">
                                                        <input type="text" required value="{{Auth::user()->email}}">
                                                        <span class="highlight"></span>
                                                        <span class="bar"></span>
                                                        <label>Email</label>
                                                    </div>
                                                    <div class="group">
                                                        <input type="number" required name="pin_code">
                                                        <span class="highlight"></span>
                                                        <span class="bar"></span>
                                                        <label>PinCode</label>
                                                    </div>

                                                    <div class="group">
                                                        <input type="number" required value="{{Auth::user()->mobile}}">
                                                        <span class="highlight"></span>
                                                        <span class="bar"></span>
                                                        <label>10-digit mobile number</label>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="group" style="width: 100%;">
                                                                <input type="text" name="address" required style="width: 84.3%;">
                                                                <span class="highlight"></span>
                                                                <span class="bar"></span>
                                                                <label>Address (Area and Street)</label>
                                                            </div>
                                                        </div>

                                                    </div>

                                                    <div class="group">
                                                        <input type="text" required name="locality">
                                                        <span class="highlight"></span>
                                                        <span class="bar"></span>
                                                        <label>Locality</label>
                                                    </div>
                                                    <div class="group">
                                                        <input type="text" required name="city">
                                                        <span class="highlight"></span>
                                                        <span class="bar"></span>
                                                        <label>City</label>
                                                    </div>

                                                    <div class="group">
                                                        <input type="text" required name="states">
                                                        <span class="highlight"></span>
                                                        <span class="bar"></span>
                                                        <label>State</label>
                                                    </div>
                                                    <div class="group">
                                                        <select class="form-control" name="address_location">
                                                            <option>Select</option>
                                                            <option value="0">Home</option>
                                                            <option value="1">Office</option>
                                                            <option value="2">Other</option>
                                                        </select>
                                                    </div>
                                                    <input class="form-control btn btn-primary" type="submit" name="" value="Submit">
                                                </form>
                                            </div>
                                        </div>
                                        @foreach($address as $addres)
                                        @if($addres->address_location==0)<div class="add_new_address1">
                                            <div class="add_new_address_boxes">
                                                <div class="add_new_address_aside">
                                                    <button>Home</button>
                                                    <h1>{{Auth::user()->name}}<span>{{Auth::user()->mobile}}</span></h1>
                                                    <p>{{$addres->address}}, {{$addres->states}} - {{$addres->pin_code}}</p>
                                                </div>
                                                <div class="dropdown" style="float:right;">
                                                    <button class="dropbtn"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>
                                                    <div class="dropdown-content">
                                                        <a href="#">Edit</a>
                                                        <a href="#">Delete</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @endif
                                        @if($addres->address_location==1)
                                        <div class="add_new_address1">
                                            <div class="add_new_address_boxes">
                                                <div class="add_new_address_aside">
                                                    <button>Office</button>
                                                    <h1>{{Auth::user()->name}}<span>{{Auth::user()->mobile}}</span></h1>
                                                    <p>G-65 ,sector 63, noida ,uttar pradesh, sector 63, Noida, Uttar Pradesh - 201301</p>
                                                </div>
                                                <div class="dropdown" style="float:right;">
                                                    <button class="dropbtn"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>
                                                    <div class="dropdown-content">
                                                        <a href="#">Edit</a>
                                                        <a href="#">Delete</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @endif
                                        @endforeach
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="menu4" class="tab-pane fade ">
                            <div class="row">
                                <div class="col-xs-12 col-sm-3 center">
                                    <span class="profile-picture">
                                        <img class="editable img-responsive" alt=" Avatar" id="avatar2" src="images/men.png">
                                    </span>

                                    <div class="space space-4"></div>
                                </div><!-- /.col -->

                                <div class="col-xs-12 col-sm-9">
                                    <h4 class="blue">
                                        <span class="middle">Hello , {{Auth::user()->name}}</span>
                                    </h4>
                                    <form method="post" action="{{ route('users.userUpdate') }}">
                                        {{ csrf_field() }}
                                    <div class="profile-user-info">
                                        <div class="row">
                                            <div class="personal_information">
                                                <div class="col-md-12">
                                                    Personal Information 
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="group">
                                                    <input type="text" required="" name="name" value="{{Auth::user()->name}}">
                                                    <span class="highlight"></span>
                                                    <span class="bar"></span>
                                                    <label>Full Name</label>
                                                </div>
                                            </div>
                                            
                                        </div>
                                        <div class="row">
                                            <div class="personal_information">
                                                <div class="col-md-12">
                                                    Your Gender 
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group">
                                               <div class="form-group">
                                                    <!-- <label>Gender</label> -->
                                                    <div class="col-md-2">
                                                        <input type="radio"  name="gender"  value="male" 
                                                        @if(Auth::user()->gender == 'male') checked="checked" @endif>Male
                                                        <input type="radio" @if(Auth::user()->gender == 'female') checked="checked" @endif name="gender"  value="female">Female
                                                    </div>
                                                    <!-- <div class="col-md-3"> -->
                                                    <!-- </div> -->
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="personal_information">
                                                <div class="col-md-12">
                                                    Email Address 
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="group">
                                                    <input type="text" required="" name="email" value="{{Auth::user()->email}}">
                                                    <span class="highlight"></span>
                                                    <span class="bar"></span>
                                                    <label></label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="personal_information">
                                                <div class="col-md-12">
                                                    Mobile Number 
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="group">
                                                    <input type="text" required="" name="mobile" value="{{Auth::user()->mobile}}">
                                                    <span class="highlight"></span>
                                                    <span class="bar"></span>
                                                    <label></label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <input type="submit" class="btn btn-primary" value="Update" name="">
                                </form>
                                </div><!-- /.col -->
                            </div>
                        </div>
                        <div id="menu5" class="tab-pane fade">
                            <div class="well" id="Support_m">
                                <h3>Quick Contact</h3>
                                <div class="breadcrume">
                                    <ul>
                                        <li>Help Centre</li>
                                        <li>Recent Orders</li>
                                    </ul>
                                </div>
                                <p style="padding-top: 15px;">Did you know? You can easily manage your orders in My Orders section.<a href="#menu2" data-toggle="pill">Go to My Orders</a></p>
                                <h4>Select order with the issue</h4>
                                <div class="order-list">
                                    <div class="order-list-bottom">
                                        <div class="">
                                            <div class="col-md-2">
                                                <img src="images/product.png" class="avatar" alt="Avatar">
                                            </div>
                                            <div class="col-md-10">
                                                <a class="a-link-normal" href="/gp/product/B07F8XW4VK/ref=ppx_yo_dt_b_asin_title_o00_s00?ie=UTF8&amp;psc=1" style="box-shadow: none; padding: 0;margin-left: 0;">
                                                    Boat Bassheads 235 V2 in-Ear Super Extra Bass Earphones with Mic (Ocean Blue)
                                                </a>
                                                <p><span>Sold by:</span> Appario Retail Private Ltd</p>
                                                <p><span>00.000 Sar</span></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="order-list">
                                    <div class="order-list-bottom">
                                        <div class="">
                                            <div class="col-md-2">
                                                <img src="images/product.png" class="avatar" alt="Avatar">
                                            </div>
                                            <div class="col-md-10">
                                                <a class="a-link-normal" href="/gp/product/B07F8XW4VK/ref=ppx_yo_dt_b_asin_title_o00_s00?ie=UTF8&amp;psc=1" style="box-shadow: none; padding: 0;margin-left: 0;">
                                                    Boat Bassheads 235 V2 in-Ear Super Extra Bass Earphones with Mic (Ocean Blue)
                                                </a>
                                                <p><span>Sold by:</span> Appario Retail Private Ltd</p>
                                                <p><span>00.000 Sar</span></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="order-list">
                                    <div class="order-list-bottom">
                                        <div class="">
                                            <div class="col-md-2">
                                                <img src="images/product.png" class="avatar" alt="Avatar">
                                            </div>
                                            <div class="col-md-10">
                                                <a class="a-link-normal" href="/gp/product/B07F8XW4VK/ref=ppx_yo_dt_b_asin_title_o00_s00?ie=UTF8&amp;psc=1" style="box-shadow: none; padding: 0;margin-left: 0;">
                                                    Boat Bassheads 235 V2 in-Ear Super Extra Bass Earphones with Mic (Ocean Blue)
                                                </a>
                                                <p><span>Sold by:</span> Appario Retail Private Ltd</p>
                                                <p><span>00.000 Sar</span></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="order-list">
                                    <div class="order-list-bottom">
                                        <div class="">
                                            <div class="col-md-2">
                                                <img src="images/product.png" class="avatar" alt="Avatar">
                                            </div>
                                            <div class="col-md-10">
                                                <a class="a-link-normal" href="/gp/product/B07F8XW4VK/ref=ppx_yo_dt_b_asin_title_o00_s00?ie=UTF8&amp;psc=1" style="box-shadow: none; padding: 0;margin-left: 0;">
                                                    Boat Bassheads 235 V2 in-Ear Super Extra Bass Earphones with Mic (Ocean Blue)
                                                </a>
                                                <p><span>Sold by:</span> Appario Retail Private Ltd</p>
                                                <p><span>00.000 Sar</span></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="order-list">
                                    <div class="order-list-bottom">
                                        <div class="">
                                            <div class="col-md-2">
                                                <img src="images/product.png" class="avatar" alt="Avatar">
                                            </div>
                                            <div class="col-md-10">
                                                <a class="a-link-normal" href="/gp/product/B07F8XW4VK/ref=ppx_yo_dt_b_asin_title_o00_s00?ie=UTF8&amp;psc=1" style="box-shadow: none; padding: 0;margin-left: 0;">
                                                    Boat Bassheads 235 V2 in-Ear Super Extra Bass Earphones with Mic (Ocean Blue)
                                                </a>
                                                <p><span>Sold by:</span> Appario Retail Private Ltd</p>
                                                <p><span>00.000 Sar</span></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <form>
                                    <h3>Contact us today, and get reply with in 24 hours!</h3>
                                    <div class="group">
                                        <input type="text" required="">
                                        <span class="highlight"></span>
                                        <span class="bar"></span>
                                        <label>Name</label>
                                    </div>

                                    <div class="group">
                                        <input type="text" required="">
                                        <span class="highlight"></span>
                                        <span class="bar"></span>
                                        <label>Email</label>
                                    </div>

                                    <div class="group">
                                        <input type="number" required="">
                                        <span class="highlight"></span>
                                        <span class="bar"></span>
                                        <label>10-digit mobile number</label>
                                    </div>
                                    <div class="row">
                                        <div class="group">
                                            <textarea placeholder="Type your Message Here...." tabindex="5" required=""></textarea>

                                        </div>
                                    </div>

                                    <div class="row submit_button">
                                        <div class="col-md-2 col-xs-6">
                                            <button type="button" class="btn btn-default order_status" style="width: 100%">Save</button>
                                        </div>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--End Click Froud here-->
@endsection