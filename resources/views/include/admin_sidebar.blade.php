<div id="sidebar-wrapper" data-simplebar="" data-simplebar-auto-hide="true">
			<div class="brand-logo">
				<a href="index.php">
					<img src="{{asset('images/logo-icon.png')}}" class="logo-icon" alt="logo icon">
					<h5 class="logo-text">Admin Dashboard</h5>
				</a>
			</div>
			<!-- <div class="user-details">
				<div class="media align-items-center user-pointer collapsed" data-toggle="collapse" data-target="#user-dropdown">
					<div class="avatar"><img class="mr-3 side-user-img" src="{{asset('images/avatar-13.png')}}" alt="user avatar"></div>
					<div class="media-body">
						<h6 class="side-user-name">{{Auth::user()->name}}</h6>
					</div>
				</div>
				<div id="user-dropdown" class="collapse">
					<ul class="user-setting-menu">
						<li><a href="javaScript:void();"><i class="icon-user"></i>  My Profile</a></li>
						<li><a href="{{route('user.logout')}}"><i class="icon-power"></i> Logout</a></li>
					</ul>
				</div>
			</div> -->
			<ul class="sidebar-menu do-nicescrol">
				<li class="sidebar-header">MAIN NAVIGATION</li>
				<li>
					<a href="{{url('http://exteriortravels.com/Admin_Dashboard')}}" class="waves-effect">
						<i class="zmdi zmdi-view-dashboard"></i> <span>Dashboard</span>
					</a>
				</li>
				<li>
					<a href="product.php" class="waves-effect">
						<i class="zmdi zmdi-layers"></i>
						<span>Manage category</span> <i class="fa fa-angle-left pull-right"></i>
					</a>
					<ul class="sidebar-submenu">
						<li><a href="{{route('admin.product')}}"><i class="zmdi zmdi-long-arrow-right"></i> Product Management</a></li>
						<!--   <li><a href="price.php"><i class="zmdi zmdi-long-arrow-right"></i> Price Management</a></li> -->
						<li><a href="parts_picture.php"><i class="zmdi zmdi-long-arrow-right"></i> Parts from Picture</a></li>
						<!--  <li><a href=""><i class="zmdi zmdi-long-arrow-right"></i> Category Management</a></li> -->
					</ul> 
				</li>
				<li>
					<a href="#" class="waves-effect">
						<i class="zmdi zmdi-layers"></i>
						<span>CMS Management</span> <i class="fa fa-angle-left pull-right"></i>
					</a>
					<ul class="sidebar-submenu">
						<li><a href="{{route('about.index')}}"><i class="zmdi zmdi-long-arrow-right"></i>About</a></li>
						<li><a href="{{route('contact.index')}}"><i class="zmdi zmdi-long-arrow-right"></i> Contact</a></li>
						<!--<li><a href="#"><i class="zmdi zmdi-long-arrow-right"></i>Services</a></li>
						<li><a href="#"><i class="zmdi zmdi-long-arrow-right"></i>Clients</a></li>
						<li><a href="#"><i class="zmdi zmdi-long-arrow-right"></i>FAQ</a></li>-->
					</ul> 
				</li>
				<li>
					<a href="{{route('admin.productlist')}}" class="waves-effect">
						<i class="zmdi zmdi-chart"></i> <span>Product List</span>
					</a>
				</li>
				<li>
					<a href="{{route('user.adminlist')}}" class="waves-effect">
						<i class="zmdi zmdi-chart"></i> <span>Customer List</span>
					</a>
				</li>
				<li>
					<a href="{{route('admin.order')}}" class="waves-effect">
						<i class="zmdi zmdi-chart"></i> <span>Order Details</span>
					</a>
				</li>
				<li>

               <!-- <li>
                  <a href="javaScript:void();" class="waves-effect">
                  <i class="zmdi zmdi-layers"></i>
                  <span>Manage Cateogry</span> <i class="fa fa-angle-left pull-right"></i>
                  </a>
                  <ul class="sidebar-submenu">
                     <li><a href="product.php"><i class="zmdi zmdi-long-arrow-right"></i>Vehicle Brands</a></li>
                     <li><a href="price.php"><i class="zmdi zmdi-long-arrow-right"></i> Vehicle locations</a></li>
                     <li><a href=""><i class="zmdi zmdi-long-arrow-right"></i> Category Management</a></li> -->
                     <!-- </ul> -->
                     <!-- </li> --> 
                     <li>
                     	<a href="{{route('vendor.adminlist')}}" class="waves-effect">
                     		<i class="zmdi zmdi-card-travel"></i>
                     		<span> Vendor Details</span>
                     	</a>
                     </li>
               <!--<li>
                  <a href="user.php" class="waves-effect">
                  <i class="zmdi zmdi-chart"></i> <span>User Details</span>
                  </a>
              </li>-->
              <li>
              	<a href="javascript:void(0)" class="waves-effect">
              		<i class="zmdi zmdi-invert-colors"></i> <span>Tracking</span>
              	</a>
              </li>
              <li>
              	<a href="#" class="waves-effect">
              		<i class="zmdi zmdi-layers"></i>
              		<span>Query Management</span> <i class="fa fa-angle-left pull-right"></i>
              	</a>
              	<ul class="sidebar-submenu">
              		<li><a href="#"><i class="zmdi zmdi-long-arrow-right"></i>Vendor</a></li>
              		<li><a href="#"><i class="zmdi zmdi-long-arrow-right"></i>Customers</a></li>

              	</ul> 
              </li>
          </ul>
      </div>