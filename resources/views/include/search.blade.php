<!--start banner sec here-->
    <div class="my" @if(app()->getLocale() == 'en') lang="en" dir="ltr" @else dir="rtl" lang="ar" @endif>
        <div class="inside_ban_Wrap seo">
            <div class="container">
                <div class="seo_flex">
                    <h1>{{ __('message.PARTS 2 DOOR - Discount Auto Parts') }}</h1>
                    {{ Form::open(array('route' => 'frame.productsearch','class'=>'form-horizontal','files'=>true)) }}
                        <input type="text" id="fname" name="search_type" placeholder="{{ __('message.Enter vehicle VIN/Frame or part number') }}" required="" style="width: 75%">
                        
                            <input type="submit" name="" value="{{ __('message.submit') }}" style="background-image: linear-gradient(to right, #375cff , #02a2ff);
    border-radius: 30px;
    color: #fff;
    border: none;
    height: 60px;
    width: 22%;
    font-family: 'Corbel';
    font-size: 2.4rem;
    font-weight: 500;
    text-transform: uppercase;
    float: right;">
                        
                    </form>
                    <p><span>Examples:</span> 0449436150 , 0449436150 , JTB31UJ75W3007203 , UZJ120-0046047</p>
                </div>
            </div>
        </div>
    </div>
    <!--End banner sec here-->