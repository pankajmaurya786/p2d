@extends('layouts.main')

@section('content')

<div class="my">
        <div class="inside_ban_Wrap listing">
            <div class="container">
            </div>
        </div>
    </div>
    <link href="http://demo.expertphp.in/css/jquery.ui.autocomplete.css" rel="stylesheet">
<script src="http://demo.expertphp.in/js/jquery.js"></script>
<script src="http://demo.expertphp.in/js/jquery-ui.min.js"></script>

<style>





    .dropdown .dropdown-menu {
    padding: 20px;
    top: 30px !important;
    width: 350px !important;
    left: 0px !important;
    box-shadow: 0px 5px 30px black;
}i.fa.fa-search.custom_cs {
    margin-top: 10px;
}.dropdown {
    float: right;
    padding-right:0px!important;
    position: relative;

}.bootstrap-select > .dropdown-toggle {
    position: relative;
    width: 100%;
    z-index: 1;
    text-align: right;
    white-space: nowrap;
}.bootstrap-select:not(.input-group-btn), .bootstrap-select[class*="col-"] {
    float: none;
    display: inline-block;
    margin-left:none;
}.show {
    display: block!important;
}.bootstrap-select:not(.input-group-btn) {
    width: 70%!important;
    margin: 0 auto;
}
@if(app()->getLocale() == 'ar')
    .search_btn{

    }
.dropdown.bootstrap-select {
    float: right!important;
}
.filter_div .search_btn {
    position: absolute;
    z-index: 1;
    top: 3px;
    background: transparent;
    border: none;
    padding-left: 52px;
    
}
.filter-option-inner-inner {
    color: #333;
    font-weight: 500;
    float: right;
}
/* direction move*/

@else

.filter_div .search_btn {
    position: absolute;
    right: 59px;
    z-index: 1;
    top: -6px;
    background: transparent;
    border: none;
}
@endif
</style>
    <!--End banner sec here-->
    <!--start Click Froud here-->
    <div class="section listing_details selection2 linkPage">
        <div class="container">
            <div class="section_container">
                @if(session()->has('message.level'))
    <div class="alert alert-{{ session('message.level') }}"> 
    {!! session('message.content') !!}
    </div>
 @endif
                @foreach($product->take(1) as $products)
                @if(app()->getLocale() == 'en')<p>@lang('message.Vehicle') <img src="{{$products->addpart->vehicel->product_icon_url}}" style="width: 20px;">modeld : {{$products->addpart->vehicel->name}} - {{$products->addpart->modeld->name}}</p>@else
                <p>@lang('message.Vehicle') <img src="{{$products->addpart->vehicel->product_icon_url}}" style="width: 20px;">@lang('message.model') : {{$products->addpart->vehicel->name_arabic}} - {{$products->addpart->modeld->name_arabic}}</p>@endif
                @endforeach
                <h1 style="font-size: 27px;">@lang('message.Type part name then select from dropdown menu or select by picture')</h1>
                <div class="row">
                    <div class="col-md-12">
                        <div class="mainTag">
                            <p class="tag alert alert-dismissible">
                                @foreach($product->take(1) as $products)
                                <a href="#">@if(app()->getLocale()=='en'){{$products->addpart->part_name}} @else {{$products->addpart->part_name_arabic}}  @endif</a>
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                @endforeach
                            </p>
                        </div>
                        <div class="filter_div tag_serch">
                            <form method="get" action="{{route('search.productpart2')}}">
                            {!! Form::text('part_number', null, array('placeholder' =>Lang::get("message.Example: Brake Pads..."),'id'=>'search_text','class'=>'add_part_number')) !!}
                            @foreach($product->take(1) as $products)
                            <input type="hidden" name="search_type" id="vechile_id" value="{{$products->addpart->vehicel->id}}">
                            {{ \Session::put('vechile_id',$products->addpart->vehicel->id) }}
                            @endforeach
                            <button class="search_btn" @if(app()->getLocale()=='ar') style="padding-right:307px !important; border-left:none;" @endif>
                                <i class="fa fa-search custom_cs" aria-hidden="true"></i></a>
                            </button>
                            <div id="search_suggesstions"></div>
                            <p>@lang('message.Search By: Part number / Vin Number / Frame Number')</p>
                            <p class="partsImage"> <a href="#">@lang('message.Select Parts From Picture')</a> </p>
                           </form>
                        </div>
                    </div>
                    <div class="col-md-12 productSection">

                        
                        <style type="text/css">
                            .alert-info2 {
                                   background-color: #d9edf7;
                                   border-color: #bce8f1;
                                   color: #31708f;
                                   }
                        </style>
                        @if (count($product) === 0)
                        <div class="alert alert-info2">@lang('message.Unable to find parts. Try to select the parts by their pictures or enter directly their numbers if you know them.')</div>
                        @else
                        @foreach($product as $products)
                        <!----Product List ----->
                        <div class="product" id="flip1" @if(app()->getLocale() == 'ar') dir="rtl" lang="ar" @else dir="ltr" lang="en"  @endif>
                            <div class="">
                                <div class="col-md-12">
                                    <div class="reference_no">
                                        @lang('message.Sold By') :<span> @if(app()->getLocale()=='en'){{$products->vendor->company_name}}
                                        @else {{$products->vendor->company_name}} @endif</span>
                                    </div>
                                </div>
                            </div>
                            <div class="sectionDetail col-md-4" @if(app()->getLocale() == 'ar') dir="rtl" lang="ar" style="float:right;" @else dir="ltr" lang="en"  @endif>
                                <ul>
                                    <li>@lang('message.Part Name') :
                                    @if(app()->getLocale()=='en'){{$products->addpart->part_name}} @else {{$products->addpart->part_name_arabic}}@endif</li>
                                    <li>@lang('message.Part Brand') : @if(app()->getLocale()=='en'){{$products->addpart->vehicel->name}} @else {{$products->addpart->vehicel->name_arabic}}@endif</li>
                                    <li>@lang('message.Part Number') :{{$products->addpart->part_number}}</li>
                                </ul>
                                <p id="flip" class="btn">@lang('message.View Details')</p>

                            </div>
                            <div class="sectionDetail col-md-3" @if(app()->getLocale() == 'ar') dir="rtl" lang="ar" style="float:right; @else dir="ltr" lang="en"  @endif>
                                <a href="#">
                                    <img id="myImg" src="{{$products->product_image_url}}">
                                </a>
                            </div>
                            <div class="sectionDetail col-md-2" @if(app()->getLocale() == 'ar') dir="rtl" lang="ar" style="float:right; @else dir="ltr" lang="en"  @endif>
                                <h3 class="price">{{$products->price}} $</h3>
                            </div>
                            <div class="sectionDetail col-md-3">
                                <a href="{{route('add.cart',array('id'=>$products->id))}}" class="buynow custom_margin_top">@lang('message.Add Cart')</a>
                                <style>
                                    p#flip {
                                        color: #2e67ff;
                                        font-size: 11px;
                                        cursor: pointer;
                                    }

                                    p.flip2 {
                                        color: #2e67ff;
                                        font-size: 11px;
                                        cursor: pointer;
                                    }

                                    .custom_margin_top {
                                        margin-top: 16% !important;
                                    }
                                </style>
                                <!-- <p>Dispatch in 3 - 5 Days</p> -->
                            </div>
                        </div>
                        
                        <div id="panel" style="background: #e9eaeb;">
                            <div class="row" >
                                <div class="col-md-12">

                                    <h2 @if(app()->getLocale() == 'ar') style="float:right !important;" @endif>@if(app()->getLocale() == 'en'){{$products->addpart->vehicel->name}} @else {{$products->addpart->vehicel->name_arabic}} @endif</h2>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <p><strong @if(app()->getLocale() == 'ar') style="float:right !important; padding-left:210px;" @endif>
                                        @if(isset($products->description))
                                            @if(app()->getLocale() == 'en')
                                                {{$products->description}}
                                            @else
                                                {{$products->arabic_description }}
                                            @endif

                                        @endif</strong><span>
                                            <img src="{{$products->addpart->vehicel->product_icon_url}}" />
                                        </span>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <!----Product List ----->
                      @endforeach 
                      @endif 
                       

                    </div>
                </div>
            </div>
        </div>
    </div>




<script type="text/javascript">
    $(document).ready(function(){
      
    $(document).on("keyup",'.add_part_number', function() {
            var key = $(this).val();
            var vechile_id = $('#vechile_id').val();
            // alert(vechile_id);
            $("#search_suggesstions").html("");
            // $("#success_message").html("");
            if (key != "") {
                var _token = "{{ csrf_token() }}";
                $.ajax({
                    url: "{{ route('autoCompleteSearch') }}",
                    method: "POST",
                    data: { query: key,vechile_id:vechile_id,_token: _token },
                    success: function(data) {
                        $("#search_suggesstions").fadeIn();
                        $("#search_suggesstions").html(data);
                        
                    }
                });
            }
        });

    $(document).on('click','.select_result',function(){
       var result =  $(this).text();
        $('.add_part_number').val(result);
        $("#search_suggesstions").hide();
    });
});
</script>

@endsection