@extends('layouts.main')

@section('content')

   <div class="my">
        <div class="inside_ban_Wrap listing">
            <div class="container">

            </div>
        </div>
    </div>
    <style> 
@if(app()->getLocale() == 'ar')

.dropdown.bootstrap-select {
    float: right!important;
}
.filter_div .search_btn {
    position: absolute;
    z-index: 1;
    top: 3px;
    background: transparent;
    border: none;
    padding-left: 52px;
    right: auto;
}
.filter-option-inner-inner {
    color: #333;
    font-weight: 500;
    float: right;
}
/* direction move*/

@endif
    .dropdown .dropdown-menu {
    padding: 20px;
    top: 30px !important;
    width: 350px !important;
    left: 0px !important;
    box-shadow: 0px 5px 30px black;
}i.fa.fa-search.custom_cs {
    margin-top: 10px;
}.dropdown {
    float: right;
    padding-right:0px!important;
    position: relative;

}.bootstrap-select > .dropdown-toggle {
    position: relative;
    width: 100%;
    z-index: 1;
    text-align: right;
    white-space: nowrap;
}.bootstrap-select:not(.input-group-btn), .bootstrap-select[class*="col-"] {
    float: none;
    display: inline-block;
    margin-left:none;
}.show {
    display: block!important;
}.bootstrap-select:not(.input-group-btn) {
    width: 70%!important;
    margin: 0 auto;
}


</style>
    <!--End banner sec here-->
    <!--start Click Froud here-->
    <div class="section listing_details selection2">
        <div class="container">
            <div class="section_container">
                
                 @foreach($geographical->take(1) as $geographicals)
                 @if(app()->getLocale() == 'en')<p>@lang('message.PARTS 2 DOOR') / @if(isset($geographicals->geographical->name)){{$geographicals->geographical->name}}@endif</p>@else
                 <p>@lang('message.PARTS 2 DOOR') / @if(isset($geographicals->geographical->name_arabic)){{$geographicals->geographical->name_arabic}}@endif</p>@endif
                 @if(app()->getLocale() == 'en')<h1>@lang('message.Vehicle Selection for') @if(isset($geographicals->vehicel->name)){{$geographicals->vehicel->name}}@endif</h1>@else
                 <h1>@lang('message.Vehicle Selection for') @if(isset($geographicals->vehicel->name_arabic)){{$geographicals->vehicel->name_arabic}}@endif</h1>@endif
                 @endforeach
                <p>@lang('message.Select Year')</p>
                <div class="row">
                    <div class="col-md-12">
                        <div class="filter_div">
                            <form method="get" action="{!! route('search.list') !!}">
                            <select name="search_type" class="selectpicker" data-live-search="true" required="">
                                @foreach($geographical as $geographicals)
                                <option value="{{$geographicals->id}}">{{$geographicals->year}}</option>
                                @endforeach
                            </select>

                            <button class="search_btn">
                                <i class="fa fa-search custom_cs" aria-hidden="true" ></i></a>
                            </button>
                            </form>
                        </div>
                    </div>


                </div>


            </div>
        </div>
    </div>


@endsection