<!--start Click Froud here-->
    <div class="section">
        <div class="container">
            <div class="section_container">
                <h1>{{ __('message.Vehicles Brands') }} </h1>
                <div class="flex-container">
                    @if(app()->getLocale() == 'en')
                     @foreach($vehicles as $vehicle)
                    <div>
                        <a href="{{url('location',array('id'=>$vehicle->id))}}">
                          <img src="{{$vehicle->product_icon_url}}" class="avatar" alt="Avatar">
                            <h1>{{$vehicle->name}}</h1> 
                        </a>
                    </div>
                     @endforeach
                @else:
                     @foreach($vehicles as $vehicle)
                    <div>
                        <a href="{{url('location',array('id'=>$vehicle->id))}}">
                          
                          <img src="{{$vehicle->product_icon_url}}" class="avatar" alt="Avatar">
                            <h1>{{$vehicle->name_arabic}}</h1> 
                        </a>
                    </div>
                     @endforeach
                @endif;


                    <!--<div>-->
                    <!--    <a href="listing.html">-->
                    <!--        <img src="{{asset('images/car_brand2.png')}}" />-->
                    <!--        <h1>Hyundai</h1>-->
                    <!--    </a>-->
                    <!--</div>-->
                    <!--<div>-->
                    <!--    <a href="listing.html">-->
                    <!--        <img src="{{asset('images/car_brand3.png')}}" />-->
                    <!--        <h1>Kia</h1>-->
                    <!--    </a>-->
                    <!--</div>-->
                    <!--<div>-->
                    <!--    <a href="#">-->
                    <!--        <img src="{{asset('images/car_brand4.png')}}" />-->
                    <!--        <h1>Lexus</h1>-->
                    <!--    </a>-->
                    <!--</div>-->
                    <!--<div>-->
                    <!--    <a href="listing.html">-->
                    <!--        <img src="{{asset('images/car_brand5.png')}}" />-->
                    <!--        <h1>Nissan</h1>-->
                    <!--    </a>-->
                    <!--</div>-->
                    <!--<div>-->
                    <!--    <a href="listing.html">-->
                    <!--        <img src="{{asset('images/car_brand6.png')}}" />-->
                    <!--        <h1>Infinity</h1>-->
                    <!--    </a>-->
                    <!--</div>-->
                </div>
            </div>
        </div>
    </div>
    <!--End Click Froud here-->