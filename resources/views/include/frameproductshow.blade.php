@extends('layouts.main')

@section('content')

   <div class="my">
        <div class="inside_ban_Wrap listing">
            <div class="container">
<div class="section custom_top">
                <div class="container">
                    <div class="section_container inharit_section_container">
                        <h1> @lang('message.Vin/Part/Frame Number')</h1>
                        <style type="text/css">
                            .alert-info2 {
                                   background-color: #d9edf7;
                                   border-color: #bce8f1;
                                   color: #31708f;
                                   }
                        </style>
                        @if (count($framesearch) === 0)
                        <div class="alert alert-info2">Unable to find parts. Try to select the parts by their pictures or enter directly their numbers if you know them.</div>
                        @else
                        <div class="flex-container">
                            <div class="custom_table">
                                <div class="table_bottom">
                                    <div class="table-responsive">
                                        <table border="1" class="custom_table">
                                            <tr>
                                                <th>@lang('message.Brand Name')</th>
                                                <th>@lang('message.Year')</th>
                                                <th>@lang('message.Model')</th>
                                                <th>@lang('message.Action')</th>
                                            </tr>
                                            @foreach($framesearch as $framesearchs)
                                            <tr>
                                                <td>@if(app()->getLocale() == 'en'){{$framesearchs->vehicel->name}}@else {{$framesearchs->vehicel->name_arabic  }} @endif</td>
                                                <td>{{$framesearchs->year->year}}</td>
                                                <td>@if(app()->getLocale() == 'en'){{$framesearchs->modeld->name}} @else{{$framesearchs->modeld->name_arabic}} @endif</td>
                                                <td class="text-center">
                                                    {{ link_to_route('search.productpart', 'Search', array('search_type'=>$framesearchs->id), ['class' => 'btn btn-sm btn-primary','value'=>Lang::get('message.search')]) }}
                                                </td>
                                            </tr>
                                            @endforeach
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                      @endif

                    </div>
                </div>
            </div>
            </div>
        </div>
    </div>
    <!--End banner sec here-->



    <!--start Click Froud here-->
    


@endsection