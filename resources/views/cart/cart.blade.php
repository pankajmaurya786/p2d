@extends('layouts.main')

@section('content')

<div class="my">
        <div class="inside_ban_Wrap listing">
            <div class="container">

            </div>
        </div>
    </div>
    <!--End banner sec here-->

                @if(session()->has('message.level'))
                <div class="alert alert-{{ session('message.level') }}"> 
                {!! session('message.content') !!}
                </div>
                @endif

    <!--start Click Froud here-->
    <div class="section listing_details selection2 linkPage">
        <div class="container">
            <div class="section_container checkout cart">
                <h1 style="font-size: 27px;">@lang('message.My Cart')</h1>
                <div class="row">
                    <div class="col-md-12 productSection">


                        <!----Product List ----->
                        <div class="product" id="box_1">
                            <div class="sectionDetail col-md-2">
                                <img src="images/part4.png">
                            </div>
                            <div class="sectionDetail col-md-4">
                                <ul class="float-none">
                                    <li>Jack Assy</li>
                                    <li>Brand: Toyota</li>
                                    <li>Partname :Splitter</li>
                                    <li>
                                        <select class="form-control" id="exampleFormControlSelect1">
                                            <option selected="">QTY: 1</option>
                                            <option value="">QTY: 2</option>
                                            <option value="">QTY: 3</option>
                                            <option value="">QTY: 4</option>
                                        </select>
                                    </li>
                                </ul>
                            </div>
                            <div class="sectionDetail col-md-3">
                                <h3 class="price">83.05 $</h3>
                            </div>
                            <div class="sectionDetail col-md-3">
                                <a href="#" class="buynow remove inharitance_remove_button_color " onclick="return confirm('Are you sure to delete this item?')" id="removeBox">Remove <span class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span></a>
                            </div>
                        </div>

                        <!----Product List ----->
                        <div class="product" id="box_2">
                            <div class="sectionDetail col-md-2">
                                <img src="images/part6.png">
                            </div>
                            <div class="sectionDetail col-md-4">
                                <ul class="float-none">
                                    <li>Jack Assy</li>
                                    <li>Brand: Toyota</li>
                                    <li>Partname :Splitter</li>

                                    <li>
                                        <select class="form-control" id="exampleFormControlSelect1">
                                            <option selected="">QTY: 1</option>
                                            <option value="">QTY: 2</option>
                                            <option value="">QTY: 3</option>
                                            <option value="">QTY: 4</option>
                                        </select>
                                    </li>
                                </ul>
                            </div>
                            <div class="sectionDetail col-md-3">
                                <h3 class="price">83.05 $</h3>
                            </div>
                            <div class="sectionDetail col-md-3">
                                <a href="#" class="buynow remove inharitance_remove_button_color" onclick="return confirm('Are you sure to delete this item?')" id="removeBox2">Remove <span class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span></a>
                            </div>
                        </div>

                        <!----Product List ----->
                        <div class="product" id="box_3">
                            <div class="sectionDetail col-md-2">
                                <img src="images/part10.png">
                            </div>
                            <div class="sectionDetail col-md-4">
                                <ul class="float-none">
                                    <li>Jack Assy</li>
                                    <li>Brand: Toyota</li>
                                    <li>Partname :Splitter</li>
                                    <li>
                                        <select class="form-control" id="exampleFormControlSelect1">
                                            <option selected="">QTY: 1</option>
                                            <option value="">QTY: 2</option>
                                            <option value="">QTY: 3</option>
                                            <option value="">QTY: 4</option>
                                        </select>
                                    </li>
                                </ul>
                            </div>
                            <div class="sectionDetail col-md-3">
                                <h3 class="price">83.05 $</h3>
                            </div>
                            <div class="sectionDetail col-md-3">
                                <a href="#" class="buynow remove inharitance_remove_button_color" onclick="return confirm('Are you sure to delete this item?')" id="removeBox3">Remove <span class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span></a>
                            </div>
                        </div>

                        <!----Product List ----->
                        <div class="product" id="box_4">
                            <div class="sectionDetail col-md-2">
                                <img src="images/part11.png">
                            </div>
                            <div class="sectionDetail col-md-4">
                                <ul class="float-none">
                                    <li>Jack Assy</li>
                                    <li>Brand: Toyota</li>
                                    <li>Partname :Splitter</li>
                                    <li>
                                        <select class="form-control" id="exampleFormControlSelect1">
                                            <option selected="">QTY: 1</option>
                                            <option value="">QTY: 2</option>
                                            <option value="">QTY: 3</option>
                                            <option value="">QTY: 4</option>
                                        </select>
                                    </li>
                                </ul>
                            </div>
                            <div class="sectionDetail col-md-3">
                                <h3 class="price">83.05 $</h3>
                            </div>
                            <div class="sectionDetail col-md-3">
                                <a href="#" class="buynow remove inharitance_remove_button_color" onclick="return confirm('Are you sure to delete this item?')" id="removeBox4">Remove <span class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span></a>
                            </div>
                        </div>


                    </div>
                </div>

                <div class="col-md-6">
                    <a href="selection10.html" class="conShop">Continue Shopping</a>
                </div>



                <div class="col-md-6">
                    <a href="checkout.html" class="protoChek">Proceed To Checkout</a>
                </div>


            </div>
        </div>
    </div>


@endsection