@extends('layouts.admin_dashboard')

@section('content')
<?php 
    $state = isset($year->vehicel->id) ? $year->vehicel->id : '';
    $geographical = isset($year->geographical->id) ? $year->geographical->id : '';
?>
<div class="clearfix"></div>
               <div class="content-wrapper">
                  <div class="container-fluid">
                     <!--Start Dashboard Content-->
                     <div class="row">
                       <div class="table-wrapper">
                         <div class="table-title">
                            <div class="row">
                               <div class="col-sm-5">
                                 <h2><b>Edit Year</b></h2>
                                  <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="javaScript:void();">Manage</a></li>
                                    <li class="breadcrumb-item"><a href="javaScript:void();">Edit Year</a></li>
                                 </ol>
                              </div>
                           </div>
                           <a href="{{ route('admin.product') }}">  <button type="button" class="btn" style="float:right;">Back</button></a>
                        </div>


                        <div class="row">
                        <div class="col-lg-12"><div class=" text-uppercase" style="color: black;
                           padding: 5px;
                           font-size: 22px;
                           font-weight: 600;">English <img src="{{asset('images/flags.png')}}" style="width: 25px;"></div>
                          <div class="card">
                           <div class="card-header text-uppercase" style="background: #dddddd;">Add  New Year  </div>

                           <div class="card-body">
                              {{ Form::model($year, array('route' => array('year.update', $year->id), 'method' => 'put', 'class'=>'dropzone dz-clickable','files'=>true)) }}
                                <div class="row">
                                   <div class="col-md-6">
                                      <label class="control-label " for="email">Select Brand</label>
                                      <!-- custom select -->
                                      <link href="{{ asset('assets/css/select2.min.css') }}" rel="stylesheet">
                                      <style>
                                       .select2-dropdown {top: 0px !important; left: 0px !important;}
                                    </style>
                                    
                                   {{ Form::select('vehicel_id',[''=>'Select']+App\Model\Vehicel::pluck('name','id')->toArray(), $state, array('class' => 'form-control')) }}

                                 </div>

                                 <div class="col-md-6">
                                    <label class="control-label " for="email">Select Location</label>
                                    <div class="">
                                    {{ Form::select('geographical_id',[''=>'Select']+App\Model\Geographical::pluck('name','id')->toArray(), $geographical, array('class' => 'form-control')) }}
                                    </div>
                                 </div>
                              <div class="col-md-6">
                                <label class="control-label " for="pwd">Vehicle Year Name:</label>
                               <div class="">
                                  <input type="text" name="year" class="form-control" value="{{$year->year}}">
                               </div>
                            </div>
                            <div class="col-md-6">
                               <button type="submit" class="btn btn-primary mt-2">Update</button>
                            </div>
                         </div>


                      </form>
                   </div>
                   <!-- section 2 -->
                     <div class="row mt-6">
                        <div class="col-lg-12">
                         <!-- <div class="card"> -->
                          <!-- <div class="card-header text-uppercase mt-5"> -->
                             <!-- <div class="eng" style="color: black;
                             padding: 5px;
                             font-size: 22px;
                             font-weight: 600;" >Arabic <img src="{{asset('images/AE-United-Arab.png')}}" style="width: 25px;"></div> -->
                          <!-- </div> -->
                          <!-- <div class="card-body"> -->
                             <!-- <form action="#" class="dropzone dz-clickable" id="dropzone">
                               <div class="row">
                                  <div class="col-md-6">
                                     <label class="control-label " for="email">Select Brand</label>
                                     <!-- custom select -->


                                    <!--  <select id="arcountry" class="form-control">
                                      <option>KIA</option>
                                      <option>TOYOTA</option>
                                   </select> -->

                                   <!-- custom select  ended-->
                                <!-- </div> -->

                                <!-- <div class="col-md-6"> -->
                                   <!-- <label class="control-label " for="email">Select Location</label>
                                   <div class="">
                                      <select id="location" class="form-control">
                                         <option>ASIA</option>
                                         <option>USA</option>
                                      </select>
                                   </div> -->
                                <!-- </div> -->
                                <!-- <div class="col-md-6"> -->
                                   <!-- <div class="">
                                     <label class="control-label " for="email">Vehicle Year Name:</label> -->



                                     <!-- custom select -->
                                     <!-- <link rel="stylesheet" type="text/css" href="css/select2.min.css"> -->
                                     <!-- <style>
                                      .select2-dropdown {top: 0px !important; left: 0px !important;}
                                   </style>
                                   <select id="Yearname" class="form-control">
                                      <option>1901</option>
                                      <option>1902</option>
                                      <option>1903</option>
                                      <option>1904</option>
                                      <option>1905</option>
                                      <option>1906</option>
                                      <option>1907</option>
                                      <option>1908</option>
                                      <option>1909</option>
                                      <option>1910</option>
                                      <option>1911</option>
                                      <option>1912</option>
                                      <option>1913</option>
                                      <option>1914</option>
                                      <option>1915</option>
                                      <option>1916</option>
                                      <option>1917</option>
                                      <option>1918</option>
                                      <option>1919</option>
                                      <option>1920</option>
                                      <option>1921</option>
                                      <option>1922</option>
                                      <option>1923</option>
                                      <option>1924</option>
                                   </select> -->
                                   
                                   
                                   <!-- custom select  ended-->


                                <!-- </div> -->
                             <!-- </div> -->
                             <!-- <div class="col-md-6"> -->
                               <!-- <label class="control-label " for="pwd">Url Slug.</label> -->
                               <!-- <div class="">           -->
                                 <!-- <input type="text" class="form-control" id="pwd"> -->
                              <!-- </div> -->
                           <!-- </div> -->
                           <!-- <div class="col-md-6"> -->
                              <!-- <button type="submit" class="btn btn-primary mt-2">Submit</button> -->
                           <!-- </div> -->
                        <!-- </div>  -->


                        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
                        <script src="js/select2.min.js"></script>
                        <script>
                          $("#country").select2( {
                             placeholder: "Select Vehicle",
                             allowClear: true
                          } );
                       </script>


                       <script>
                          $("#Year").select2( {
                             placeholder: "Select Vehicle",
                             allowClear: true
                          } );


                          $("#lan").select2( {
                             placeholder: "Select Vehicle",
                             allowClear: true
                          } );$("#arcountry").select2( {
                             placeholder: "Select Vehicle",
                             allowClear: true
                          } );$("#location").select2( {
                             placeholder: "Select Vehicle",
                             allowClear: true
                          } );$("#Yearname").select2( {
                             placeholder: "Select Vehicle",
                             allowClear: true
                          } );
                       </script>

                    </form>
                 </div>
              </div>
           </div>
        </div>
      <!-- section 2 ended -->
   </div>
</div>
</div>
</div>
<div class="clearfix">
  <div class="hint-text">Showing <b>5</b> out of <b>25</b> entries</div>

</div>
</div>
</div>
<!--End Row-->
<!--End Dashboard Content-->
</div>

<!--End content-wrapper-->
<!--Start Back To Top Button-->
<a href="javaScript:void();" class="back-to-top"><i class="fa fa-angle-double-up"></i> </a>


@endsection