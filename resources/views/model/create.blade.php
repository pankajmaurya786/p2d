@extends('layouts.admin_dashboard')

@section('content')
<div class="clearfix"></div>
               <div class="content-wrapper">
                  <div class="container-fluid">
                     <!--Start Dashboard Content-->
                     <div class="row">
                       <div class="table-wrapper">
                         <div class="table-title">
                            <div class="row">
                               <div class="col-sm-5">
                                  <h2><b>Add New Vehicle Model</b></h2>
                                  <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="javaScript:void();">Manage</a></li>
                                    <li class="breadcrumb-item"><a href="javaScript:void();"> Add New Vehicle Model</a></li>
                                 </ol>
                              </div>
                           </div>
                           <a href="{{ route('admin.product') }}">  <button type="button" class="btn" style="float:right;">Back</button></a>
                        </div>

                        <div class="row">
                        <div class="col-lg-12">
                           <div class="eng" style="color: black;
                        padding: 5px;
                        font-size: 22px;
                        font-weight: 600;">English <img src="{{asset('images/flags.png')}}" style="width: 25px;"></div>
                          <div class="card">
                           <div class="">

                              <div class=""><div class="card-header text-uppercase">ADD NEW YEAR</div> </div></div>

                           <div class="card-body">
                              {{ Form::open(array('route' => 'model.store','class'=>'dropzone dz-clickable','files'=>true)) }}
                                <div class="row">
                                   <div class="col-md-6">
                                      <label class="control-label " for="email">Select Brand</label>
                                      <!-- custom select -->
                                      <link rel="stylesheet" type="text/css" href="css/select2.min.css">
                                      <style>
                                       .select2-dropdown {top: 0px !important; left: 0px !important;}
                                    </style>
                                    <select id="country" name="vehicel_id" class="form-control">
                                              <option value="" selected disabled>Select</option>
                                               @foreach($vehical as $key => $country)
                                               <option value="{{$key}}"> {{$country}}</option>
                                               @endforeach
                                            </select>

                                 </div>

                                 <div class="col-md-6">
                                    <label class="control-label " for="email">Select Location</label>
                                    <div class="">
                                  
                                       <select name="geographical_id" id="state" class="form-control" >
                                       </select>

                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="">
                                      <label class="control-label " for="email">Vehicle Year Name:</label>
                                      <style>
                                       .select2-dropdown {top: 0px !important; left: 0px !important;}
                                    </style>
                                    
                                      <select name="year_id" id="city" class="form-control" >
                                      </select>


                                 </div>
                              </div>
                              <div class="col-md-6">
                                <label class="control-label " for="pwd">Enter Vehicle Model</label>
                                <div class="">          
                                  <input type="text" name="name" class="form-control" id="pwd">
                               </div>
                            </div>

                            <div class="col-md-6">
                                <label class="control-label " for="pwd">Enter Vehicle Model Arabic</label>
                                <div class="">          
                                  <input type="text" name="name_arabic" class="form-control" id="pwd">
                               </div>
                            </div>
                            <div class="col-md-6">
                               <button type="submit" class="btn btn-primary mt-2">Submit</button>
                            </div>
                         </div>


                      </form>
                   </div>
                   <!-- section 2 -->
                   


                      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
                      <script src="js/select2.min.js"></script>
                      <script>
                        $("#country").select2( {
                           placeholder: "Select Vehicle",
                           allowClear: true
                        } );
                     </script>


                     <script>
                        $("#Year").select2( {
                           placeholder: "Select Vehicle",
                           allowClear: true
                        } );


                        $("#lan").select2( {
                           placeholder: "Select Vehicle",
                           allowClear: true
                        } );$("#arcountry").select2( {
                           placeholder: "Select Vehicle",
                           allowClear: true
                        } );$("#location").select2( {
                           placeholder: "Select Vehicle",
                           allowClear: true
                        } );$("#Yearname").select2( {
                           placeholder: "Select Vehicle",
                           allowClear: true
                        } );
                     </script>

                  </form>
               </div>
            </div>
         </div>
      </div>
      <!-- section 2 ended -->
   </div>
</div>
</div>
</div>
<div class="clearfix">
  <div class="hint-text">Showing <b>5</b> out of <b>25</b> entries</div>

</div>
</div>
</div>
<!--End Row-->
<!--End Dashboard Content-->
</div>
<!-- End container-fluid-->

<!--End content-wrapper-->
<!--Start Back To Top Button-->
<a href="javaScript:void();" class="back-to-top"><i class="fa fa-angle-double-up"></i> </a>
<script type="text/javascript">
    $('#country').change(function(){
    var countryID = $(this).val();    
    if(countryID){
        $.ajax({
           type:"GET",
           url:"{{url('get-state-list')}}?vehicel_id="+countryID,
           success:function(res){               
            if(res){
                $("#state").empty();
                $("#state").append('<option>Select</option>');
                $.each(res,function(key,value){
                    $("#state").append('<option value="'+key+'">'+value+'</option>');
                });
           
            }else{
               $("#state").empty();
            }
           }
        });
    }else{
        $("#state").empty();
        $("#city").empty();
    }      
   });
    $('#state').on('change',function(){
    var stateID = $(this).val();    
    if(stateID){
        $.ajax({
           type:"GET",
           url:"{{url('get-city-list')}}?geographical_id="+stateID,
           success:function(res){               
            if(res){
                $("#city").empty();
                $.each(res,function(key,value){
                    $("#city").append('<option value="'+key+'">'+value+'</option>');
                });
           
            }else{
               $("#city").empty();
            }
           }
        });
    }else{
        $("#city").empty();
    }
        
   });
</script>
@endsection     