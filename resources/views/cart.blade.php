@extends('layouts.main')

@section('content')

<div class="my">
        <div class="inside_ban_Wrap listing">
            <div class="container">

            </div>
        </div>
    </div>
    <!--End banner sec here-->



    <!--start Click Froud here-->
    <div class="section listing_details selection2 linkPage">
        <div class="container">
            <div class="section_container checkout cart">
                <h1 style="font-size: 27px;">@lang('message.My Cart')</h1>
                <div class="row">
                    <div class="col-md-12 productSection">


                        <!----Product List ----->
                        <?php $total = 0 ?>
 
        @if(session('cart'))
            @foreach(session('cart') as $id => $details)
 
                <?php $total += $details['price'] * $details['quantity'] ?>
                        <div class="product" id="box_1">
                            <div class="sectionDetail col-md-2">
                                <img src="{{ $details['photo'] }}">
                            </div>
                            <div class="sectionDetail col-md-4">
                                <ul class="float-none">
                                    <li>@lang('message.Part Brand'):@if(app()->getLocale() == 'en'){{ $details['name'] }} @else {{ $details['name_arabic'] }} @endif </li>
                                    <li>@if(app()->getLocale() == 'en'){{ $details['part_name'] }} @else {{ $details['part_name_arabic'] }} @endif</li>
                                    <li>@lang('message.Quantity'):{{ $details['quantity'] }}</li>
                                    <input type="hidden" name="vendor_id" value="{{ $details['vendor'] }}">
                                </ul>
                            </div>
                            <div class="sectionDetail col-md-3">
                                <h3 class="price">{{ $details['price'] }} $</h3>
                            </div>
                            <div class="sectionDetail col-md-3">
                                <td class="actions" data-th="">
                        <button class="btn btn-info btn-sm update-cart" data-id="{{ $id }}"><i class="fa fa-refresh"></i></button>
                        <button class="btn btn-danger btn-sm remove-from-cart" data-id="{{ $id }}"><i class="fa fa-trash-o"></i></button>
                    </td>
                            </div>
                        </div>
            @endforeach
        @endif
                        <!----Product List ----->
                    </div>
                </div>

                <div class="col-md-6">
                    <a href="{{route('home')}}" class="conShop">@lang('message.Continue Shopping')</a>
                </div>



                <div class="col-md-6">
                    <a href="{{route('check.out')}}" class="protoChek">@lang('message.Proceed To Checkout')</a>
                </div>


            </div>
        </div>
    </div>
<script type="text/javascript">
 
        $(".update-cart").click(function (e) {
           e.preventDefault();
 
           var ele = $(this);
 
            $.ajax({
               url: '{{ url('update-cart') }}',
               method: "patch",
               data: {_token: '{{ csrf_token() }}', id: ele.attr("data-id"), quantity: ele.parents("tr").find(".quantity").val()},
               success: function (response) {
                   window.location.reload();
               }
            });
        });
 
        $(".remove-from-cart").click(function (e) {
            e.preventDefault();
 
            var ele = $(this);
 
            if(confirm("Are you sure")) {
                $.ajax({
                    url: '{{ url('remove-from-cart') }}',
                    method: "DELETE",
                    data: {_token: '{{ csrf_token() }}', id: ele.attr("data-id")},
                    success: function (response) {
                        window.location.reload();
                    }
                });
            }
        });
 
    </script>

@endsection