@extends('layouts.vendor_dashboard')

@section('content')
<div class="clearfix"></div>
        <div class="content-wrapper">
            <div class="container-fluid">
                <!--Start Dashboard Content-->
                <div class="row">
                    <div class="container-fluid">
                        <!-- Breadcrumb-->
                        <div class="row pt-2 pb-2">
                            <div class="col-sm-9">
                                <h4 class="page-title">View Products</h4>
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
                                    <li class="breadcrumb-item"><a href="javaScript:void();">View Products</a></li>
                                </ol>
                            </div>
                        </div>
                        @if(session()->has('message.level'))
                       <div class="alert alert-{{ session('message.level') }}"> 
                    {!! session('message.content') !!}
                       </div>
                    @endif
                        <!-- End Breadcrumb-->
                        <div class="row custome_box_Vp">
                            <div class="table-wrapper">
                                <div class="table-title">
                                    <div class="row">
                                        <div class="col-sm-5">
                                            <div class="text-uppercase inharit_cart">Products Listing</div>
                                        </div>
                                    </div>
                                </div>
                                
                                <table class="table table-striped table-hover table-bordered">
                                    <thead>
                                        <tr class="bg-primary">
                                            <th>Sl.No</th>
                                            <th>Part Name</th>
                                            <th>Primary Image</th>
                                            <th>Price</th>
                                            <th>No Of Quantity </th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody id="myTable">
                                      @foreach($product as $products)
                                        <tr>
                                            <td>{{$products->id}}</td>
                                            <td>{{$products->addpart->part_name}}</td>
                                            <td><a href="#"><img src="{{$products->product_image_url}}" class="avatar" alt="Avatar"> </a></td>
                                            <td>{{$products->price}}</td>
                                            <td>{{$products->quantity}}</td>
                                            <td>{{ Form::open(array('route' => array('product.destroy', $products->id), 'method' => 'delete', 'class' => 'form-inline','name'=>'delete')) }}  
                           {{ link_to_route('product.edit', 'Edit', array($products->id), ['class' => 'btn btn-sm btn-primary']) }}
                           {{Form::submit('Delete', ['class' => 'btn btn-sm btn-danger','id'=>'delete'])}}
                           {{ Form::close() }}
                         </td>   
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                
                                <div class="clearfix">
                                    <div class="hint-text">Showing <b>5</b> out of <b>25</b> entries</div>
                                    <ul class="pagination">
                                        <li class="page-item disabled"><a href="#">Previous</a></li>
                                        <li class="page-item"><a href="#" class="page-link">1</a></li>
                                        <li class="page-item"><a href="#" class="page-link">2</a></li>
                                        <li class="page-item active"><a href="#" class="page-link">3</a></li>
                                        <li class="page-item"><a href="#" class="page-link">4</a></li>
                                        <li class="page-item"><a href="#" class="page-link">5</a></li>
                                        <li class="page-item"><a href="#" class="page-link">Next</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--End Row-->
            </div>
            <!-- End container-fluid-->
        </div>
      @endsection