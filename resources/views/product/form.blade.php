                                  <div class="form-group">
                                                <label class="control-label col-sm-2" for="email">Enter Part Number:</label>
                                                <div class="col-sm-12">

                                                    <!--custom select option -->
                                                    <link rel="stylesheet" href="{{asset('assets/css/select2.min.css')}}" />
                                                    <style>
                                                        .select2-dropdown {
                                                            top: 0px !important;
                                                            left: 0px !important;
                                                        }
                                                    </style>
                                                    <select id="country" name="addpart_id" style="padding: 10px;">
                                                        <option value="" class="text-black">Select Part</option>
                                                       @foreach($addpart as $addparts)
                                                        <option value="{{$addparts->id}}" class="text-black">{{$addparts->part_name}}</option>
                                                        @endforeach
                                                    </select>
                                                    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
                                                    <script src="{{ asset('assets/js/select2.min.js') }}"></script>
                                                   
                                                    <script>
                                                        $("#country").select2({
                                                            placeholder: "Select Part",
                                                            allowClear: true
                                                        });
                                                    </script>
                                                    <!--custom select option Ended-->
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-sm-2" for="pwd">Add Images</label>
                                                <div class="col-sm-12">
                                                    <!--                                                    <label>Allow Multiple Images</label>-->
                                                    <style>
                                                        input.form-control.upload {
                                                            line-height: 1;
                                                        }
                                                    </style>
                                                    <input type="file" name="product_image" class="form-control upload" class="line_height_1">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-sm-2" for="pwd">Enter Part Price.</label>
                                                <div class="col-sm-12">
                                       {{ Form::text('price', NULL, array('class' => 'form-control')) }}
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-sm-2" for="pwd">Number Of Quantity</label>
                                                <div class="col-sm-12">
                                    {{ Form::text('quantity', NULL, array('class' => 'form-control')) }}
                                                </div>
                                            </div>