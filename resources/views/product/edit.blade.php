@extends('layouts.vendor_dashboard')

@section('content')
<div class="clearfix"></div>
        <div class="content-wrapper">
            <div class="container-fluid">
                <!--Start Dashboard Content-->
                <div class="row">
                    <div class="table-wrapper">
                        <div class="table-title">
                            <div class="row">
                                <div class="col-sm-5">
                                    <h2><b>Product Management</b></h2>
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="javaScript:void();">Manage</a></li>
                                        <li class="breadcrumb-item"><a href="javaScript:void();">Product Management</a></li>
                                    </ol>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="card">
                                    <div class="card-header text-uppercase">Edit Product </div>
                                    <div class="card-body">

           {{ Form::model($product, array('route' => array('product.update', $product->id), 'method' => 'put', 'class'=>'dropzone dz-clickable','files'=>true)) }}
                                          <input type="hidden" name="vendor_id" value="{{$vendor->id}}">
                                            @include('product.form')
                                            <div class="form-group">
                                                <div class="col-sm-offset-2 col-sm-10">
                                                    <button type="submit" class="btn btn-primary">Save</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--End Row-->
                <!--End Dashboard Content-->
            </div>
            <!-- End container-fluid-->
        </div>
        @endsection