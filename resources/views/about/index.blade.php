@extends('layouts.admin_dashboard')

@section('content')

<div class="clearfix"></div>
         <div class="content-wrapper">
            <div class="container-fluid">
               <!--Start Dashboard Content-->
               <div class="row">
       <div class="table-wrapper">
              <div class="table-title">
                 <div class="row">
                    <div class="col-sm-5">
                       <h2><b>About Us List</b></h2>
                       <ol class="breadcrumb">
                           <li class="breadcrumb-item"><a href="javaScript:void();">Manage</a></li>
                           <li class="breadcrumb-item"><a href="javaScript:void();">About Us List</a></li>
                        </ol>
                    </div>
                 </div>
              </div>
              <div class="row">
                    <div class="col-lg-12">
                      <div class="card">
                        <!-- <div class="card-header text-uppercase">Add New Product </div> -->
                        

                           <div class="row panel panel-default panel-body custom_index_box">
                            <div class="col-sm-12 mt-0">
                                <!--custom sidebar -->
                                <!--<ul class="sidebar-menu  do-nicescrol d-flex justify-content-center custom_bg">
                                    <li class="custom_flex">
                                        <a class="waves-effect mn-active ul-inner-menu" id="useTab_1">
                                            <i class="fa fa-user"></i> <span>Basic Information</span>
                                        </a>
                                    </li>
                                    <li class="custom_flex">
                                        <a class="waves-effect ul-inner-menu" id="useTab_2">
                                            <i class="fa fa-camera"></i>
                                            <span> Profile Picture </span>
                                        </a>
                                    </li>
                                    <li class="custom_flex">
                                        <a class="waves-effect ul-inner-menu" id="useTab_3">
                                            <i class="fa fa-first-order" aria-hidden="true"></i>
                                            <span> Order Details </span>
                                        </a>
                                    </li>

                                    <li class="custom_flex">
                                        <a class="waves-effect ul-inner-menu" id="useTab_4">
                                            <i class="fa fa-address-card-o" aria-hidden="true"></i>
                                            <span> Address </span>
                                        </a>
                                    </li>
                                    <li class="custom_flex">
                                        <a class="waves-effect ul-inner-menu" id="useTab_5">
                                            <i class="zmdi zmdi-car-taxi"></i>
                                            <span> Assign Driver </span>
                                        </a>
                                    </li>
                                    custom menu bar
                                </ul>-->
                                <!--custom sidebar ended here milan nayak-->
                            </div>
                            <div class="table-wrapper col-sm-12">
                                <div class="table-title">
                                    <a href="{{route('about.create')}}" class="btn btn-sm btn-default" style="background: cornflowerblue;">Aboutus Create</a>
                                    <div class="row">
                                        <div class="col-sm-6"></div>
                                        <div class="col-sm-6">
                                            <input class="form-control custom_filter_css" id="myInput" type="text" placeholder="Search here..">
                                        </div>
                                    </div>

                                </div>

                                <table class="table table-hover table-bordered" style="display: inline-table;">
                                    <thead>
                                        <tr class="bg-primary custom_height">
                                            <th>Sl.No</th>
                                            <th>About Us Content</th>
                                            <th>Action</th>
<?php
$counter = 0;
?>                                           

                                        </tr>
                                    </thead>
                                    <tbody id="myTable">
                                        @foreach($aboutus as $about)
                                        <tr>
                                          <td><?php echo ++$counter; ?></td> 
                                          <td>{{$about->about_content}}</td>
                                          <td>{{ Form::open(array('route' => array('about.destroy', $about->id), 'method' => 'delete', 'class' => 'form-inline','name'=>'delete')) }}  
                           {{ link_to_route('about.edit', 'Edit', array($about->id), ['class' => 'btn btn-sm btn-primary']) }}
                           {{Form::submit('Delete', ['class' => 'btn btn-sm btn-danger','id'=>'delete'])}}
                           {{ Form::close() }}
                         </td>                                         
                                        </tr>
                                       @endforeach    
                                    </tbody>
                                </table>



                                <table class="table table-striped table-hover" id="tabs-3" style="display: none">
                                    <thead>
                                        <tr>
                                            <th>Purchase Date</th>
                                            <th>plan types</th>
                                            <th>plan start date</th>
                                            <th>plan end date</th>
                                            <th>serial no</th>
                                            <th>order id</th>
                                        </tr>
                                    </thead>
                                    <tbody id="myTable">
                                        <tr>
                                            <td id="date">2015-03-25</td>
                                            <td>
                                                <a href="#">
                                                    1</a>
                                            </td>
                                            <td>18-August-2019</td>
                                            <td>22-August-2019</td>
                                            <td>
                                                <address><i class="custom_font_13">
                                                        4CE0460D0G</i>
                                                </address>
                                            </td>
                                            <td>233503</td>
                                        </tr>
                                        <tr>
                                            <td>2015-03-25</td>
                                            <td>
                                                <a href="#">
                                                    2</a>
                                            </td>
                                            <td>18-August-2019</td>
                                            <td>22-August-2019</td>
                                            <td>
                                                <address><i class="custom_font_13">
                                                        4CE0460D0G</i>
                                                </address>
                                            </td>
                                            <td>233503</td>
                                        </tr>
                                        <tr>
                                            <td>2015-03-25</td>
                                            <td>
                                                <a href="#">
                                                    3</a>
                                            </td>
                                            <td>18-August-2019</td>
                                            <td>22-August-2019</td>
                                            <td>
                                                <address><i class="custom_font_13">
                                                        4CE0460D0G</i>
                                                </address>
                                            </td>
                                            <td>233503</td>
                                        </tr>
                                        <tr>
                                            <td>2015-03-25</td>
                                            <td>
                                                <a href="#">
                                                    4</a>
                                            </td>
                                            <td>18-August-2019</td>
                                            <td>22-August-2019</td>
                                            <td>
                                                <address><i class="custom_font_13">
                                                        4CE0460D0G</i>
                                                </address>
                                            </td>
                                            <td>233503</td>
                                        </tr>
                                    </tbody>
                                </table>

                                <div id="tabs-4" style="display: none">
                                            <div class="form-group">
                                                <label for="fullname">Full name</label>
                                                <input type="text" class="form-control" id="fullname" aria-describedby="emailHelp" required="">
                                            </div>
                                            <div class="form-group">
                                                <label for="Street-Address">Street Address</label>
                                                <input type="text" class="form-control" id="Street-Address" placeholder="Street and number, P.O. box, c/o." required="">
                                                <div class="extra_space"></div>
                                                <input type="text" class="form-control" id="Street-Address" placeholder="Apartment, suite, unit, building, floor, etc." required="">
                                            </div>
                                            <div class="form-group">
                                                <label for="City">City</label>
                                                <input type="text" class="form-control" id="City" required="">
                                            </div>
                                            <div class="form-group">
                                                <label for="City">State</label>
                                                <input type="text" class="form-control" id="State" required="">
                                            </div>
                                            <div class="form-group">
                                                <label for="Zip-Code">Zip Code</label>
                                                <input type="text" class="form-control" id="Zip-Code" required="">
                                            </div>
                                            <div class="form-group">

                                                


                                                <script src="build/js/intlTelInput.js"></script>
                                                <script>
                                                    var input = document.querySelector("#phone");
                                                    window.intlTelInput(input, {
                                                        
                                                        utilsScript: "build/js/utils.js",
                                                    });
                                                </script>

                                                <!--<small>May be used to assist delivery</small>-->

                                                <button type="submit" class="btn btn-primary">Add address</button>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div><table class="table table-striped table-hover" style="display: none">
                                    <form></form>
                                        
                                    
                                </table>

                                <div class="card-body" style="display: none">
                                    <form action="#" class="dropzone dz-clickable" id="dropzone">
                                        <div class="form-group">
                                            <label class="control-label col-sm-2" for="pwd">Picture</label>
                                            <div class="col-sm-12">
                                                <input type="file" class="form-control line-height-19" id="choose_file" value="">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-offset-2 col-sm-10">
                                                <button type="submit" class="btn btn-default">Update</button>
                                                <span class="click_show" style="display: none">
                                                    <button type="submit" class="btn btn-primary">View</button>
                                                    <button type="submit" class="btn btn-danger" id="removePic">Remove</button>
                                                </span>
                                            </div>

                                        </div>
                                    </form>
                                </div>

                                <div class="clearfix">
                                </div>
                            </div>
                        </div>





                        </div>
                      </div>
                    </div>
                  </div>
                <div class="clearfix">
                   <div class="hint-text">Showing <b>5</b> out of <b>25</b> entries</div>
                  
                </div>
                </div>
              </div>
               <!--End Row-->
               <!--End Dashboard Content-->
            </div>
            <!-- End container-fluid-->
         </div>
         <!--End content-wrapper-->
         <!--Start Back To Top Button-->
         <a href="javaScript:void();" class="back-to-top"><i class="fa fa-angle-double-up"></i> </a>



@endsection