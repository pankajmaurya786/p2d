

<div class="clearfix"></div>
        <div class="content-wrapper">
            <div class="container-fluid">
                <!--Start Dashboard Content-->
                <div class="row">
                    <div class="table-wrapper">
                        <div class="table-title">
                            <div class="row">
                                <div class="col-sm-5">
                                    <h2><b>About us Management</b></h2>
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="javaScript:void();">Manage</a></li>
                                        <li class="breadcrumb-item"><a href="javaScript:void();">About us</a></li>
                                    </ol>
                                </div>
                            </div>1
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="card">
                                    <div class="card-header text-uppercase">About Content </div>
                                    <div class="card-body">

                                        {{ Form::open(array('route' => 'about.store','class'=>'dropzone dz-clickable','files'=>true)) }}
                                            <div class="form-group">
                                                <label class="control-label col-sm-2" for="pwd">Enter Part Price.</label>
                                                <div class="col-sm-12">
                                                    <input type="text" name="about_content" class="form-control" id="pwd">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-sm-2" for="pwd">Add Images</label>
                                                <div class="col-sm-12">
                                                   
                                                    <style>
                                                        input.form-control.upload {
                                                            line-height: 1;
                                                        }
                                                    </style>
                                                    <input type="file" name="about_image" class="form-control upload" class="line_height_1">
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <div class="col-sm-offset-2 col-sm-10">
                                                    <button type="submit" class="btn btn-primary">Save</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="clearfix">
                            <div class="hint-text">Showing <b>5</b> out of <b>25</b> entries</div>

                        </div>
                    </div>
                </div>
                <!--End Row-->
                <!--End Dashboard Content-->
            </div>
            <!-- End container-fluid-->
        </div>
        <!--End content-wrapper-->
        <!--Start Back To Top Button-->
        <a href="javaScript:void();" class="back-to-top"><i class="fa fa-angle-double-up"></i> </a>


