@extends('layouts.admin_dashboard')

@section('content')

<div class="clearfix"></div>
               <div class="content-wrapper">
                  <div class="container-fluid">
                     <!--Start Dashboard Content-->
                     <div class="row">
                       <div class="table-wrapper">
                         <div class="table-title">
                            <div class="row">
                               <div class="col-sm-5">
                                  <h2><b>VIN Number</b></h2>
                                  <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="javaScript:void();">Manage</a></li>
                                    <li class="breadcrumb-item"><a href="javaScript:void();">VIN Number</a></li>
                                 </ol>
                              </div>
                           </div>
                           <a href="product.php">  <button type="button" class="btn" style="float:right;">Back</button></a>
                        </div>                      
                        <div class="row">
                        <div class="col-lg-12">
                           <div class="eng" style="color: black;
                        padding: 5px;
                        font-size: 22px;
                        font-weight: 600;">English <img src="{{asset('images/flags.png')}}" style="width: 25px;"></div>
                          <div class="card">
                           <div class="">

                              <div class=""><div class="card-header text-uppercase">ADD VIN Number</div> </div></div>

                           <div class="card-body">
                              {{ Form::open(array('route' => 'vinnumber.store','class'=>'dropzone dz-clickable','files'=>true)) }}
                                <div class="row">
                                   <div class="col-md-6">
                                      <label class="control-label " for="email">Select Brand</label>
                                      <!-- custom select -->
                                      <link rel="stylesheet" type="text/css" href="css/select2.min.css">
                                      <style>
                                       .select2-dropdown {top: 0px !important; left: 0px !important;}
                                    </style>
                                    <select id="country" name="vehicel_id" class="form-control">
                                              <option value="" selected disabled>Select</option>
                                               @foreach($vehical as $key => $country)
                                               <option value="{{$key}}"> {{$country}}</option>
                                               @endforeach
                                            </select>

                                 </div>

                                 <div class="col-md-6">
                                    <label class="control-label " for="email">Select Location</label>
                                    <div class="">
                                  
                                       <select name="geographical_id" id="state" class="form-control" >
                                       </select>

                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="">
                                      <label class="control-label " for="email">Vehicle Year Name:</label>
                                      <style>
                                       .select2-dropdown {top: 0px !important; left: 0px !important;}
                                    </style>
                                    
                                      <select name="year_id" id="city" class="form-control" >
                                      </select>
                                      

                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                  <label class="control-label " for="pwd">Vehicle Model</label>
                                  <div class="">  

                                   <select name="modeld_id" id="model" class="form-control">
                                   </select>

                               </div>
                            </div>

                            <div class="col-md-6">
                                <label class="control-label " for="pwd">Frame Number</label>
                                <div class="">     

                                  <select name="framenumber_id" id="frame" class="form-control">
                                  </select>

                               </div>
                             </div>
                            <div class="col-md-6">
                                <label class="control-label " for="pwd">Enter Grade Number</label>
                               <div class="">

                                  <select name="gradenumber_id" id="grade" class="form-control">
                                  </select>

                               </div>
                            </div>

                            <div class="col-md-6">
                                <label class="control-label " for="pwd">Enter VIN Number</label>
                               <div class="">
                                  <input type="text" name="vin_number" class="form-control">
                               </div>
                            </div>
                            <div class="col-md-6">
                               <button type="submit" class="btn btn-primary mt-4">Submit</button>
                            </div>
                         </div>


                      </form>
                   </div>
                   <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
                      <script src="js/select2.min.js"></script>
                   <!-- section 2 -->
                  <!-- <div class="row mt-6">
                      <div class="col-lg-12">
                       <div class="card">
                        <div class="card-header text-uppercase mt-5">
                           <div class="eng" style="color: black;
                           padding: 5px;
                           font-size: 22px;
                           font-weight: 600;">Arabic <img src="{{asset('images/AE-United-Arab.png')}}" style="width: 25px;"></div>
                        </div>
                        <div class="card-body">
                           <form action="#" class="dropzone dz-clickable" id="dropzone">
                             <div class="row">
                                <div class="col-md-6">
                                   <label class="control-label " for="email">Select Brand</label>
                                   
                                   
                                   <select id="arcountry" class="form-control">
                                    <option>KIA</option>
                                    <option>TOYOTA</option>
                                 </select>

                                 
                              </div>

                              <div class="col-md-6">
                                 <label class="control-label " for="email">Select Location</label>
                                 <div class="">
                                    <select id="location" class="form-control">
                                       <option>ASIA</option>
                                       <option>USA</option>
                                    </select>
                                 </div>
                              </div>
                              <div class="col-md-6">
                                 <div class="">
                                   <label class="control-label " for="email">Vehicle Year Name:</label>



                                   <style>
                                    .select2-dropdown {top: 0px !important; left: 0px !important;}
                                 </style>
                                 <select id="Yearname" class="form-control">
                                    <option>1901</option>
                                    <option>1902</option>
                                    <option>1903</option>
                                    <option>1904</option>
                                    <option>1905</option>
                                    <option>1906</option>
                                    <option>1907</option>
                                    <option>1908</option>
                                    <option>1909</option>
                                    <option>1910</option>
                                    <option>1911</option>
                                    <option>1912</option>
                                    <option>1913</option>
                                    <option>1914</option>
                                    <option>1915</option>
                                    <option>1916</option>
                                    <option>1917</option>
                                    <option>1918</option>
                                    <option>1919</option>
                                    <option>1920</option>
                                    <option>1921</option>
                                    <option>1922</option>
                                    <option>1923</option>
                                    <option>1924</option>
                                 </select>
                                 
                                 
                                


                              </div>
                           </div>
                           <div class="col-md-6">
                             <label class="control-label " for="pwd">Enter Vehicle Model</label>
                             <div class="">          
                               <select id="EVM2" class="form-control">
                                       <option>CRUZE</option>
                                       <option>ALTIS</option>
                                    </select>
                            </div>
                         </div>
                         <div class="col-md-6">
                             <label class="control-label " for="pwd">Frame Number</label>
                             <select id="fm2" class="form-control">
                                       <option>CRUZE</option>
                                       <option>ALTIS</option>
                                    </select>
                         </div>
                         <div class="col-md-6">
                             <label class="control-label " for="pwd">Enter Grade Number</label>
                             <select id="EGN3" class="form-control">
                                       <option>CRUZE</option>
                                       <option>ALTIS</option>
                                    </select>
                         </div>
                         <div class="col-md-6">
                             <label class="control-label " for="pwd">Enter VIN Number</label>
                             <input type="text" name="" class="form-control">
                         </div>
                         <div class="col-md-6">
                            <button type="submit" class="btn btn-primary mt-4">Submit</button>
                         </div>
                      </div>


                      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
                      <script src="js/select2.min.js"></script>
                      <script>
                        $("#country").select2( {
                           placeholder: "Select Vehicle",
                           allowClear: true
                        } );
                     </script>


                     <script>
                        $("#Year").select2( {
                           placeholder: "Select Vehicle",
                           allowClear: true
                        } );


                        $("#lan").select2( {
                           placeholder: "Select Vehicle",
                           allowClear: true
                        } );$("#arcountry").select2( {
                           placeholder: "Select Vehicle",
                           allowClear: true
                        } );$("#location").select2( {
                           placeholder: "Select Vehicle",
                           allowClear: true
                        } );$("#Yearname").select2( {
                           placeholder: "Select Vehicle",
                           allowClear: true
                        } );$("#EVM").select2( {
                           placeholder: "Select Vehicle",
                           allowClear: true
                        } );$("#EVM2").select2( {
                           placeholder: "Select Vehicle",
                           allowClear: true
                        } );$("#fmn").select2( {
                           placeholder: "Select Vehicle",
                           allowClear: true
                        } );$("#fm2").select2( {
                           placeholder: "Select Vehicle",
                           allowClear: true
                        } );$("#EGN5").select2( {
                           placeholder: "Select Vehicle",
                           allowClear: true
                        } );$("#EGN3").select2( {
                           placeholder: "Select Vehicle",
                           allowClear: true
                        } );
                     </script>

                  </form>
               </div>
            </div>
         </div>
      </div>-->
      <!-- section 2 ended -->
   </div>
</div>
</div>
</div>
<div class="clearfix">
  <div class="hint-text">Showing <b>5</b> out of <b>25</b> entries</div>

</div>
</div>
</div>
<!--End Row-->
<!--End Dashboard Content-->
</div>
<!-- End container-fluid-->

<!--End content-wrapper-->
<!--Start Back To Top Button-->
<a href="javaScript:void();" class="back-to-top"><i class="fa fa-angle-double-up"></i> </a>


<script type="text/javascript">
    $('#country').change(function(){
    var countryID = $(this).val();    
    if(countryID){
        $.ajax({
           type:"GET",
           url:"{{url('get-state-list')}}?vehicel_id="+countryID,
           success:function(res){               
            if(res){
                $("#state").empty();
                $("#state").append('<option>Select</option>');
                $.each(res,function(key,value){
                    $("#state").append('<option value="'+key+'">'+value+'</option>');
                });
           
            }else{
               $("#state").empty();
            }
           }
        });
    }else{
        $("#state").empty();
        $("#city").empty();
    }      
   });
    $('#state').on('change',function(){
    var stateID = $(this).val();    
    if(stateID){
        $.ajax({
           type:"GET",
           url:"{{url('get-city-list')}}?geographical_id="+stateID,
           success:function(res){               
            if(res){
                $("#city").empty();
                $("#city").append('<option>Select</option>');
                $.each(res,function(key,value){
                    $("#city").append('<option value="'+key+'">'+value+'</option>');
                });
           
            }else{
               $("#city").empty();
            }
           }
        });
    }else{
        $("#city").empty();
    }
        
   });
    $('#city').on('change',function(){
    var stateID = $(this).val();    
    if(stateID){
        $.ajax({
           type:"GET",
           url:"{{url('get-model-list')}}?year_id="+stateID,
           success:function(res){               
            if(res){
                $("#model").empty();
                $("#model").append('<option>Select</option>');
                $.each(res,function(key,value){
                    $("#model").append('<option value="'+key+'">'+value+'</option>');
                });
           
            }else{
               $("#model").empty();
            }
           }
        });
    }else{
        $("#model").empty();
    }
        
   });
    $('#model').on('change',function(){
    var stateID = $(this).val();    
    if(stateID){
        $.ajax({
           type:"GET",
           url:"{{url('get-frame-list')}}?modeld_id="+stateID,
           success:function(res){               
            if(res){
                $("#frame").empty();
                $("#frame").append('<option>Select</option>');
                $.each(res,function(key,value){
                    $("#frame").append('<option value="'+key+'">'+value+'</option>');
                });
           
            }else{
               $("#frame").empty();
            }
           }
        });
    }else{
        $("#frame").empty();
    }
        
   });
    $('#frame').on('change',function(){
    var stateID = $(this).val();    
    if(stateID){
        $.ajax({
           type:"GET",
           url:"{{url('get-grade-list')}}?framenumber_id="+stateID,
           success:function(res){               
            if(res){
                $("#grade").empty();
                $("#grade").append('<option>Select</option>');
                $.each(res,function(key,value){
                    $("#grade").append('<option value="'+key+'">'+value+'</option>');
                });
           
            }else{
               $("#grade").empty();
            }
           }
        });
    }else{
        $("#grade").empty();
    }
        
   });
</script>

@endsection