@extends('layouts.admin_dashboard')

@section('content')
<?php 
    $vehicel = isset($frame->vehicel->id) ? $frame->vehicel->id : '';
    $geographical = isset($frame->geographical->id) ? $frame->geographical->id : '';
    $year = isset($frame->year->id) ? $frame->year->id : '';
    $model = isset($frame->modeld->id) ? $frame->modeld->id : '';
?>
<div class="clearfix"></div>
               <div class="content-wrapper">
                  <div class="container-fluid">
                     <!--Start Dashboard Content-->
                     <div class="row">
                      <div class="table-wrapper">
                       <div class="table-title">
                          <div class="row">
                             <div class="col-sm-5">
                                <h2><b>Frame number</b></h2>
                                <ol class="breadcrumb">
                                 <li class="breadcrumb-item"><a href="javaScript:void();">Manage</a></li>
                                 <li class="breadcrumb-item"><a href="javaScript:void();"> Frame number</a></li>
                              </ol>
                           </div>
                        </div>
                        <a href="{{ route('admin.product') }}">  <button type="button" class="btn" style="float:right;">Back</button></a>
                     </div>
                     <div class="row">
                     <div class="col-lg-12">
                        <div class="eng" style="color: black;
                        padding: 5px;
                        font-size: 22px;
                        font-weight: 600;">English <img src="{{asset('images/flags.png')}}" style="width: 25px;"></div>
                        <div class="card">
                           <div class="">

                              <div class=""><div class="card-header text-uppercase">ADD NEW FRAME NUMBER</div> </div></div>

                              <div class="card-body">
                                 {{ Form::model($frame, array('route' => array('framenumber.update', $frame->id), 'method' => 'put', 'class'=>'dropzone dz-clickable','files'=>true)) }}
                                  <div class="row">
                                     <div class="col-md-6">
                                        <label class="control-label " for="email">Select Brand</label>
                                        <!-- custom select -->
                                        <link rel="stylesheet" type="text/css" href="css/select2.min.css">
                                        <style>
                                          .select2-dropdown {top: 0px !important; left: 0px !important;}
                                       </style>
                                      {{ Form::select('vehicel_id',[''=>'Select']+App\Model\Vehicel::pluck('name','id')->toArray(), $vehicel, array('class' => 'form-control')) }}

                                    </div>

                                    <div class="col-md-6">
                                       <label class="control-label " for="email">Select Location</label>
                                       <div class="">

                                        {{ Form::select('geographical_id',[''=>'Select']+App\Model\Geographical::pluck('name','id')->toArray(), $geographical, array('class' => 'form-control')) }}

                                       </div>
                                    </div>
                                    <div class="col-md-6">
                                       <div class="">
                                        <label class="control-label " for="email">Vehicle Year Name:</label>
                                        <style>
                                          .select2-dropdown {top: 0px !important; left: 0px !important;}
                                       </style>

                                       {{ Form::select('year_id',[''=>'Select']+App\Model\Year::pluck('year','id')->toArray(), $year, array('class' => 'form-control')) }}


                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                  <label class="control-label " for="pwd">Vehicle Model</label>
                                  <div class=""> 

                                   {{ Form::select('modeld_id',[''=>'Select']+App\Model\Modeld::pluck('name','id')->toArray(), $model, array('class' => 'form-control')) }}

                              </div>
                           </div>
                           <div class="col-md-6">
                            <label class="control-label " for="pwd">Enter Frame Number</label>
                            <input type="text" name="frame_number" class="form-control" value="{{$frame->frame_number}}">
                         </div>
                         <div class="col-md-6">
                           <button type="submit" class="btn btn-primary mt-4">Update</button>
                       </div>
                    </div>


                 </form>
              </div>
              <!-- section 2 -->
              <!-- <div class="row mt-6">
                 <div class="col-lg-12">
                   <div class="card">
                     <div class="card-header text-uppercase mt-5">
                        <div class="eng" style="color: black;
                        padding: 5px;
                        font-size: 22px;
                        font-weight: 600;">Arabic <img src="{{asset('images/AE-United-Arab.png')}}" style="width: 25px;"></div>
                     </div>
                     <div class="card-body">
                        <form action="#" class="dropzone dz-clickable" id="dropzone">
                         <div class="row">
                            <div class="col-md-6">
                               <label class="control-label " for="email">Select Brand</label> -->
                               <!-- custom select -->


                               <!-- <select id="arcountry" class="form-control">
                                 <option>KIA</option>
                                 <option>TOYOTA</option>
                              </select> -->

                              <!-- custom select  ended-->
                           <!-- </div>

                           <div class="col-md-6">
                              <label class="control-label " for="email">Select Location</label>
                              <div class="">
                                 <select id="location" class="form-control">
                                    <option>ASIA</option>
                                    <option>USA</option>
                                 </select>
                              </div>
                           </div>
                           <div class="col-md-6">
                              <div class="">
                               <label class="control-label " for="email">Vehicle Year Name:</label> -->



                               <!-- custom select -->
                               <!-- <link rel="stylesheet" type="text/css" href="css/select2.min.css"> -->
                               <style>
                                 .select2-dropdown {top: 0px !important; left: 0px !important;}
                              </style>
                              <!-- <select id="Yearname" class="form-control">
                                 <option>1901</option>
                                 <option>1902</option>
                                 <option>1903</option>
                                 <option>1904</option>
                                 <option>1905</option>
                                 <option>1906</option>
                                 <option>1907</option>
                                 <option>1908</option>
                                 <option>1909</option>
                                 <option>1910</option>
                                 <option>1911</option>
                                 <option>1912</option>
                                 <option>1913</option>
                                 <option>1914</option>
                                 <option>1915</option>
                                 <option>1916</option>
                                 <option>1917</option>
                                 <option>1918</option>
                                 <option>1919</option>
                                 <option>1920</option>
                                 <option>1921</option>
                                 <option>1922</option>
                                 <option>1923</option>
                                 <option>1924</option>
                              </select> -->


                              <!-- custom select  ended-->


                           <!-- </div>
                        </div>
                        <div class="col-md-6">
                         <label class="control-label " for="pwd">Enter Vehicle Model</label>
                         <div class="">          
                          <select id="EVM2" class="form-control">
                           <option>CRUZE</option>
                           <option>ALTIS</option>
                        </select>
                     </div>
                  </div>
                  <div class="col-md-6">
                   <label class="control-label " for="pwd">Enter Frame Number</label>
                   <input type="text" class="form-control">
                </div>
                <div class="col-md-6">
                 <button type="submit" class="btn btn-primary mt-4">Submit</button>
              </div>
           </div>
 -->

           <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
           <script src="js/select2.min.js"></script>
           <script>
            $("#country").select2( {
               placeholder: "Select Vehicle",
               allowClear: true
            } );
         </script>


         <script>
            $("#Year").select2( {
               placeholder: "Select Vehicle",
               allowClear: true
            } );


            $("#lan").select2( {
               placeholder: "Select Vehicle",
               allowClear: true
            } );$("#arcountry").select2( {
               placeholder: "Select Vehicle",
               allowClear: true
            } );$("#location").select2( {
               placeholder: "Select Vehicle",
               allowClear: true
            } );$("#Yearname").select2( {
               placeholder: "Select Vehicle",
               allowClear: true
            } );$("#EVM").select2( {
               placeholder: "Select Vehicle",
               allowClear: true
            } );$("#EVM2").select2( {
               placeholder: "Select Vehicle",
               allowClear: true
            } );
         </script>

      </form>
   </div>
</div>
</div>
</div>
<!-- section 2 ended -->
</div>
</div>
</div>
</div>
<div class="clearfix">
 <div class="hint-text">Showing <b>5</b> out of <b>25</b> entries</div>

</div>
</div>
</div>
<!--End Row-->
<!--End Dashboard Content-->
</div>
<!-- End container-fluid-->

<!--End content-wrapper-->
<!--Start Back To Top Button-->
<a href="javaScript:void();" class="back-to-top"><i class="fa fa-angle-double-up"></i> </a>
<!--End Back To Top Button-->
@endsection     