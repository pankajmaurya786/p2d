@extends('layouts.vendor_dashboard')

@section('content')
        <div class="content-wrapper">
            <div class="container-fluid">
                <!--Start Dashboard Content-->
                <div class="row">
                    <div class="table-wrapper">
                        <div class="table-title">
                            <div class="row">
                                <div class="col-sm-5">
                                    <h2><b>My Profile</b></h2>
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="javaScript:void();">Manage</a></li>
                                        <li class="breadcrumb-item"><a href="javaScript:void();">MyProfile</a></li>
                                    </ol>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="card">
                                    <!-- custom profile -->
                                    <div class="container emp-profile">
                                        {{ Form::open(array('route' => 'profile.image','class'=>'form-horizontal','files'=>true)) }}
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="profile-img" style="height: 148px;">
                                                        <img src="{{Auth::user()->vendor->profile_icon_url}}" alt="" width="223px" height="148px" />
                                                        <div class="file btn btn-lg btn-primary">
                                                            Change Photo
                                                            <input type="file" name="profile_icon" />
                                                        </div>
                                                    </div>
                                                    <button type="submit" class="btn btn-primary" id="update_user_profile" style="margin-left: 50px;margin-top: 30px;">Save Changes</button>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="profile-head">
                                                        <h5>
                                                            {{Auth::user()->vendor->company_name}}
                                                        </h5>
                                                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                                                            <li class="nav-item text-black">
                                                                <a class="nav-link active text-black" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true" style="color:black">Basic Information</a>
                                                            </li>
                                                            <li class="nav-item text-black">
                                                                <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false" style="color:black">Address</a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                </div>
                                                
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                </div>
                                                <div class="col-md-8">
                                                    <div class="tab-content profile-tab" id="myTabContent">
                                                        <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab" style="background: white;">
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <label>Company Name</label>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <p>{{Auth::user()->vendor->company_name}}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <label>Name</label>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <p>{{Auth::user()->name}}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <label>Email</label>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <p>{{Auth::user()->email}}</p>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <label>Phone</label>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <p>{{Auth::user()->mobile}}</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        </form>
                                                        <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab" style="background: white">
                                                            <div class="row">
                                                                <div class="col-md-12">
                  {{ Form::open(array('route' => 'profile.update','class'=>'form-horizontal','files'=>true)) }}
                                                                        <div class="p-2">
                                                            <input type="text" name="name" class="form-control" placeholder="Enter Name" value="{{Auth::user()->name}}">
                                                                        </div>
                                                                        <div class="p-2">
                                                           <input type="tel" name="email" class="form-control" placeholder="Enter Email" value="{{Auth::user()->email}}">
                                                                        </div>
                                                                        <div class="p-2">
                                                           <input type="tel" name="mobile" class="form-control" placeholder="Enter 10 Digit Mobile Number" value="{{Auth::user()->mobile}}">
                                                                        </div>
                                                                        <div class="p-2">
                                                            <input type="text" name="address" class="form-control" placeholder="Enter Address" value="{{Auth::user()->vendor->address}}">
                                                                        </div>
                                                                        <div class="p-2">
                                                            <input type="text" name="flat_no" class="form-control" placeholder="Flat/House No./ Floor/ Building" value="{{Auth::user()->vendor->flat_no}}">
                                                                        </div>

                                                                        <div class="p-2">
                                                            <input type="text" name="locality" class="form-control" placeholder="Colony/Street./ Locality" value="{{Auth::user()->vendor->locality}}">
                                                                            <link href="https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel="stylesheet">
                                                                            <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
                                                                            <script src="https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
                                                                        </div>
                                                                        <script>
                                                                            $(function() {
                                                                                var availableTutorials = [
                                                                                    "Mumbai",
                                                                                    "Delhi",
                                                                                    "Bangalore",
                                                                                    "Hyderabad",
                                                                                    "Ahmedabad",
                                                                                    "Hyderabad",
                                                                                    "Chennai",
                                                                                    "Kolkata",
                                                                                    "Surat",
                                                                                    "Pune",
                                                                                    "Jaipur",
                                                                                    "Lucknow",
                                                                                    "Kanpur",
                                                                                    "Nagpur",
                                                                                    "Indore",
                                                                                    "Thane",
                                                                                    "Bhopal",
                                                                                    "Visakhapatnam",
                                                                                    "Pimpri & Chinchwad",
                                                                                    "Patna",
                                                                                    "Vadodara",
                                                                                    "Ghaziabad",
                                                                                    "Ludhiana",
                                                                                    "Agra",
                                                                                    "Nashik",
                                                                                    "Faridabad",
                                                                                    "Meerut",
                                                                                    "Rajkot",
                                                                                    "Kalyan & Dombivali",
                                                                                    "Vasai Virar",
                                                                                    "Varanasi",
                                                                                    "Srinagar",
                                                                                    "Aurangabad",
                                                                                    "Dhanbad",
                                                                                    "Amritsar",
                                                                                    "Navi Mumbai",
                                                                                    "Allahabad",
                                                                                    "Ranchi",
                                                                                    "Haora",
                                                                                    "Coimbatore",
                                                                                    "Jabalpur",
                                                                                    "Gwalior",
                                                                                    "Vijayawada",
                                                                                    "Jodhpur",
                                                                                    "Madurai",
                                                                                    "Raipur",
                                                                                    "Kota",
                                                                                    "Guwahati",
                                                                                    "Chandigarh",
                                                                                    "Solapur",
                                                                                    "Hubli and Dharwad",
                                                                                    "Bareilly",
                                                                                    "Moradabad",
                                                                                    "Mysore",
                                                                                    "Gurgaon",
                                                                                    "Aligarh",
                                                                                    "Jalandhar",
                                                                                    "Tiruchirappalli",
                                                                                    "Bhubaneswar",
                                                                                    "Salem",
                                                                                    "Mira and Bhayander",
                                                                                    "Thiruvananthapuram",
                                                                                    "Bhiwandi",
                                                                                    "Saharanpur",
                                                                                    "Gorakhpur",
                                                                                    "Guntur",
                                                                                    "Bikaner",
                                                                                    "Amravati",
                                                                                    "Noida",
                                                                                    "Jamshedpur",
                                                                                    "Bhilai Nagar",
                                                                                    "Warangal",
                                                                                    "Cuttack",
                                                                                    "Firozabad",
                                                                                    "Kochi",
                                                                                    "Bhavnagar",
                                                                                    "Dehradun",
                                                                                    "Durgapur",
                                                                                    "Asansol",
                                                                                    "Nanded Waghala",
                                                                                    "Kolapur",
                                                                                    "Ajmer",
                                                                                    "Gulbarga",
                                                                                    "Jamnagar",
                                                                                    "Ujjain",
                                                                                    "Loni",
                                                                                    "Siliguri",
                                                                                    "Jhansi",
                                                                                    "Ulhasnagar",
                                                                                    "Nellore",
                                                                                    "Jammu",
                                                                                    "Sangli Miraj Kupwad",
                                                                                    "Belgaum",
                                                                                    "Mangalore",
                                                                                    "Ambattur",
                                                                                    "Tirunelveli",
                                                                                    "Malegoan",
                                                                                    "Gaya",
                                                                                    "Jalgaon",
                                                                                    "Udaipur",
                                                                                    "Maheshtala",
                                                                                ];
                                                                                $("#tagsAF").autocomplete({
                                                                                    source: availableTutorials,
                                                                                    autoFocus: true
                                                                                });
                                                                            });
                                                                        </script>
                                                                        <div class="p-2">
                                                             <input type="text" name="landmark" class="form-control" placeholder="Landmark" value="{{Auth::user()->vendor->landmark}}">
                                                                        </div>


                                                                        <div class="p-2">
                                                                           <select id="city" class="form-control">
                                                                                <option value="">Select State</option>
                                                                                <option value="4">Delhi</option>
                                                                                <option value="248">Noida</option>
                                                                            </select>
                                                                        
                                                                        </div>

                                                                        <div class="p-2">
                                                                            <!--custom select-->
                                                                            <link rel="stylesheet" href="css/select2.min.css" />
                                                                            <style>
                                                                                .select2-dropdown {
                                                                                    top: 0px !important;
                                                                                    left: 0px !important;
                                                                                }
                                                                            </style>
                                                                            <select id="State" class="form-control">
                                                                                <option value="">Select State</option>
                                                                                <option value="4">Delhi</option>
                                                                                <option value="248">Noida</option>
                                                                            </select>
                                                                            <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
                                                                            <script src="js/select2.min.js"></script>
                                                                            <script>
                                                                                $("#State").select2({
                                                                                    placeholder: "Select State",
                                                                                    allowClear: true
                                                                                });$("#city").select2({
                                                                                    placeholder: "Select City",
                                                                                    allowClear: true
                                                                                });
                                                                            </script>
                                                                            <!--custom select Ended -->
                                                                            <button type="submit" class="btn btn-primary" id="update_user_profile" style="margin-left: 50px;margin-top: 30px;">Save Changes</button>
                                                                        </div>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        
                                    </div>
                                    <!-- custom profile ended -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix">
                        <div class="hint-text">Showing <b>5</b> out of <b>25</b> entries</div>
                    </div>
                </div>
            </div>
            <!--End Row-->

            <!--End Dashboard Content-->
        </div>
        <!-- End container-fluid-->

    </div>
        @endsection