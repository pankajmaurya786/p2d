
@extends('layouts.admin_dashboard')

@section('content')

               
               
               <div class="clearfix"></div>
               <div class="content-wrapper">
                  <div class="container-fluid">
                     <!--Start Dashboard Content-->
                     <div class="row">
                      <div class="table-wrapper">
                       <div class="table-title">
                          <div class="row">
                             <div class="col-sm-5">
                                <h2><b>Geographical list</b></h2>
                                <ol class="breadcrumb">
                                 <li class="breadcrumb-item"><a href="javaScript:void();">Manage</a></li>
                                 <li class="breadcrumb-item"><a href="javaScript:void();">Geographical list</a></li>
                              </ol>
                           </div>
                        </div>
                        <a href="{{ route('admin.product') }}">  <button type="button" class="btn" style="float:right;">Back</button></a>
                     </div>

                     <div class="row">
                       <div class="col-lg-12">
                        <div class="eng" style="color: black;
                        padding: 5px;
                        font-size: 22px;
                        font-weight: 600;">English <img src="{{asset('images/flags.png')}}" style="width: 25px;"></div>
                        <div class="card">
                           <div class="card-header text-uppercase" style="background: #dddddd;">ADD NEW LOCATION</div>




                           <div class="card-body">



                            <div class="form-group">
                              {{ Form::model($geographic, array('route' => array('geographical.update', $geographic->id), 'method' => 'put', 'class'=>'dropzone dz-clickable','files'=>true)) }}
                              <div class="row">
                                <div class="col-md-6">
                                      <label class="control-label " for="email">Select Brand</label>
                                      <!-- custom select -->
                                      <link href="{{ asset('assets/css/select2.min.css') }}" rel="stylesheet">
                                      <style>
                                       .select2-dropdown {top: 0px !important; left: 0px !important;}
                                    </style>
                                    
                                   <select id="country" name="vehicel_id" class="form-control">
                                              <option value="" selected disabled>Select</option>
                                               @foreach($vehical as $key => $country)
                                               <option value="{{$key}}" @if($geographic->vehicel_id==$key)selected @else '' @endif> {{$country}}</option>
                                               @endforeach
                                            </select>

                                 </div>
                               <div class="col-md-6">
                                 <label for="usr">Location Name:</label>
                                 <input type="text" name="name" value="{{$geographic->name}}" class="form-control" id="usr" required="">
                              </div>
                              
                              <div class="col-md-6">
                                 <label for="usr">Location Name Arabic:</label>
                                 <input type="text" name="name_arabic" value="{{$geographic->name_arabic}}" class="form-control" id="usr" required="">
                              </div>

                                                   
                              <div class="col-md-6">
                                 <div class="mt-5">        
                                      
                                       
                                       <button class="btn btn-primary" style="position: relative;   top: -15px;">Save</button>
                                    
                                  </div>
                              </div>
                           </div>
                           </form>
                           


                           
                              

                             <style>
                              label.control-label.col-sm-2.upload1 {
                                margin-left: -26px;
                                margin-top: 13px;
                             }
                             button.btn1 {
                                border: none;
                                border-radius: 5px;
                                padding: 5px;
                                width: 60px;
                                cursor: pointer;
                             }
                          </style>
                      </form>

                      <!-- <div class="row" style="margin-top: 50px;">
                       <div class="col-lg-12">
                        <div class="eng" style="color: black;
                        padding: 5px;
                        font-size: 22px;
                        font-weight: 600;">Arabic <img src="{{asset('images/AE-United-Arab.png')}}" style="width: 25px;"></div>
                        <div class="card">
                           <div class="card-header text-uppercase" style="background: #dddddd;">ADD NEW LOCATION </div>




                           <div class="card-body">



                            <div class="form-group">
                              <form>
                              <div class="row">
                               <div class="col-md-6">
                                 <label for="usr">Location Name:</label>
                                 <input type="text" class="form-control" id="usr" required="">
                              </div>

                              
                              
                              <div class="col-md-6">
                                 <div class="mt-5">        
                                      
                                       
                                       <button class="btn btn-primary" style="position: relative;   top: -15px;">Save</button>
                                    
                                  </div>
                              </div>
                           </div>
                           </form>
                           


                           
                              

                             <style>
                              label.control-label.col-sm-2.upload1 {
                                margin-left: -26px;
                                margin-top: 13px;
                             }
                             button.btn1 {
                                border: none;
                                border-radius: 5px;
                                padding: 5px;
                                width: 60px;
                                cursor: pointer;
                             }
                          </style>
                      </form> -->


                   </div>
                </div>
             </div>


          </div>
          <div class="clearfix">
             <div class="hint-text">Showing <b>5</b> out of <b>25</b> entries</div>

          </div>
       </div>
    </div>



    <!--End Row-->
    <!--End Dashboard Content-->
 </div>
 <!-- End container-fluid-->
</div>
<!--End content-wrapper-->
<!--Start Back To Top Button-->
<a href="javaScript:void();" class="back-to-top"><i class="fa fa-angle-double-up"></i> </a>
<!--End Back To Top Button-->

<!--Start footer-->
 @endsection                       



               
               
