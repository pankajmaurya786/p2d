@extends('layouts.admin_dashboard')

@section('content')

<div class="content-wrapper">
            <div class="container-fluid">
               <!--Start Dashboard Content-->
               <div class="row">
                   <div class="container-fluid">
                     <!-- Breadcrumb-->
                     <div class="row pt-2 pb-2">
                        <div class="col-sm-9">
                        <h4 class="page-title">User Details</h4>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javaScript:void();">User Details</a></li>
                            <li class="breadcrumb-item"><a href="javaScript:void();"> {{$user->name}}</a></li>
                         </ol>
                     </div>
                     </div>
                      <!-- End Breadcrumb-->
                     <div class="row">
                         <div class="col-lg-12">
                            <div class="card vendor_Details">
                               <div class="card-body">
                                 <ul class="list-unstyled">
                                   <li class="media">
                                     <div class="media-body">
                                       <h5 class="mt-0 mb-1">{{$user->name}}</h5>
                                       
                                     </div>
                                   </li>
                                 </ul>
                                 <div class="row">
                                    <div class="col-md-6">
                                       <div class="profile-user-info">
                                          <div class="profile-info-row">
                                            <div class="profile-info-name"> Name </div>
                                            <div class="profile-info-value">
                                              <span>{{$user->name}}</span>
                                            </div>
                                          </div>
                                          <div class="profile-info-row">
                                            <div class="profile-info-name"> Email </div>
                                            <div class="profile-info-value">
                                              <span>{{$user->email}}</span>
                                            </div>
                                          </div>
                                          <div class="profile-info-row">
                                            <div class="profile-info-name"> Mobile Number </div>
                                            <div class="profile-info-value">
                                              <span>{{$user->mobile}}</span>
                                            </div>
                                          </div>
                                          <div class="profile-info-row">
                                            <div class="profile-info-name"> Pin Code </div>
                                            <div class="profile-info-value">
                                              <span>{{$user->useraddress->pin_code}}</span>
                                            </div>
                                          </div>
                                          <div class="profile-info-row">
                                            <div class="profile-info-name"> Address </div>
                                            <div class="profile-info-value">
                                              <span>{{$user->useraddress->address}}</span>
                                            </div>
                                          </div>
                                          
                                          <div class="profile-info-row">
                                            <div class="profile-info-name"> State </div>
                                            <div class="profile-info-value">
                                              <span>{{$user->useraddress->states}}</span>
                                            </div>
                                          </div>
                                        </div>
                                    </div>
                                 </div>
                               </div>
                             </div>
                         </div>
                       </div>  
                        </div>
                   </div>
               <!--End Row-->
               
            </div>
            <!-- End container-fluid-->
         </div>


@endsection