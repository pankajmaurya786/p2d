@extends('layouts.admin_dashboard')

@section('content')

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<link href="assets/css/app-style.css" rel="stylesheet" />

<!--End topbar header-->
    <style>     
div#menu7 {     
    overflow-x: scroll;     
}       
</style>
<div class="clearfix"></div>
<div class="content-wrapper">
    <div class="container-fluid" style="position: relative;
    top: -75px;">
        <!--Start Dashboard Content-->
        <div class="row">
            <div class="table-wrapper">

                <div class="table-title">
                    <div class="row">
                        <div class="col-sm-5">
                            <h2><b>Manage category</b></h2>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="javaScript:void();">Vehicle</a></li>
                                <li class="breadcrumb-item"><a href="javaScript:void();">Vehicle Brands</a></li>
                            </ol>
                        </div>
                    </div>
                </div>
                     <h3> @if(session()->has('message.level'))
                      <div class="alert alert-{{ session('message.level') }}"> 
                      {!! session('message.content') !!}
                      </div>
                      @endif</h3>
                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#home">Add Vehicle</a></li>
                    <li><a data-toggle="tab" href="#menu1">Geographical list</a></li>
                    <li><a data-toggle="tab" href="#menu2">Add year</a></li>
                    <li><a data-toggle="tab" href="#menu3">Vehicle Model</a></li>
                    <li><a data-toggle="tab" href="#menu4">Frame number</a></li>
                    <li><a data-toggle="tab" href="#menu5">Grade Engine</a></li>
                    <li><a data-toggle="tab" href="#menu6">VIN Number</a></li>
                    <li><a data-toggle="tab" href="#menu7">Add Part</a></li>
                </ul>
<?php
$counter = 0;
?>
                <div class="tab-content">
                    <div id="home" class="tab-pane fade in active">
                        <br>
                        <a href="{{route('vehicel.create')}}">
                            <button type="button" class="order_status" style="float: right;"><i class="fa fa-plus-circle" aria-hidden="true"></i> Add New Brand</button>
                        </a>

                        <table class="table table-striped table-hover table-bordered" id="home">
                            <thead>
                                <tr class="bg-primary">
                                    <th>Sl.no</th>
                                    <th>Brand Name</th>
                                    <th>Arabic Brand Name</th>
                                    <th>Image</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($vehicel as $vehicels)
                                <tr>
                                    <td><?php echo ++$counter; ?></td>
                                    <td><a href="#">{{@$vehicels->name}}</a></td>
                                    <td><a href="#">{{@$vehicels->name_arabic}}</a></td>
                                    <td><a href="#"><img src="{{@$vehicels->product_icon_url}}" class="avatar" alt="Avatar"> </a></td>
                                    <td>{{ Form::open(array('route' => array('vehicel.destroy', $vehicels->id), 'method' => 'delete', 'class' => 'form-inline','name'=>'delete')) }}  
                           {{ link_to_route('vehicel.edit', 'Edit', array($vehicels->id), ['class' => 'btn btn-sm btn-primary']) }}
                           {{Form::submit('Delete', ['class' => 'btn btn-sm btn-danger','id'=>'delete'])}}
                           {{ Form::close() }}
                         </td>        
                                </tr>
                                @endforeach
                            </tbody>

                        </table>
                        {{ $vehicel->links() }}
                        <style>
                            .dropzone {
                                border: 2px dashed rgba(39, 38, 38, 0.3);
                                background: rgba(255, 255, 255, 0.2);
                                border-radius: 4px;
                                padding: 20px;
                            }
                        </style>
                    </div>




                    <div id="menu1" class="tab-pane fade">

                        <br>

                        <a href="{{route('geographical.create')}}">
                            <button type="button" class="order_status" style="float: right;"><i class="fa fa-plus-circle" aria-hidden="true"></i> Add New Area</button>
                        </a>

                        <table class="table table-striped table-hover table-bordered" id="home">
                            <thead>
                                <tr class="bg-primary">
                                    <th>Sl.no</th>
                                    <th>Brand Name</th>
                                    <!-- <th>Arabic Brand Name</th> -->
                                    <th>Geographical Area</th>
                                    <th>Arabic Geographical Area</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
<?php
$counter1 = 0;
?>
                            <tbody>
                                 @foreach($geographical as $geographicals)
                                <tr>
                                    <td><?php echo ++$counter1; ?></td>
                                    <td>{{@$geographicals->vehicel->name}}</td>
                                    <!-- <td>{{@$geographicals->vehicel->name_arabic}}</td> -->
                                    <td><a href="#">{{@$geographicals->name}}</a></td>
                                    <td><a href="#">{{@$geographicals->name_arabic}}</a></td>
                                     <td>{{ Form::open(array('route' => array('geographical.destroy', $geographicals->id), 'method' => 'delete', 'class' => 'form-inline','name'=>'delete')) }}  
                           {{ link_to_route('geographical.edit', 'Edit', array($geographicals->id), ['class' => 'btn btn-sm btn-primary']) }}
                           {{Form::submit('Delete', ['class' => 'btn btn-sm btn-danger','id'=>'delete'])}}
                           {{ Form::close() }}
                         </td>        
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                    </div>



                    <div id="menu2" class="tab-pane fade">
                        <br>
                        <a href="{{route('year.create')}}">
                            <button type="button" class="order_status" style="float: right;"><i class="fa fa-plus-circle" aria-hidden="true"></i> Add New Vehicle Year</button>
                        </a>
                        <table class="table table-striped table-hover" id="menu1">
                            <thead>
                                <tr class="bg-primary">
                                    <th>Sl.no</th>
                                    <th>Brand Name</th>
                                    <!-- <th>Arabic Brand Name</th> -->
                                    <th>Geographical Area</th>
                                    <th>Year</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
<?php
$counter2 = 0;
?>
                            <tbody>
                                @foreach($year as $years)
                                <tr>
                                    <td><?php echo ++$counter2; ?></td>
                                    <td>{{@$years->vehicel->name}}</td>
                                    <!-- <td>{{@$years->vehicel->name_arabic }}</td> -->
                                    <td>{{@$years->geographical->name}}</td>
                                    <td>{{@$years->year}}</td>
                                     <td>{{ Form::open(array('route' => array('year.destroy', $years->id), 'method' => 'delete', 'class' => 'form-inline','name'=>'delete')) }}  
                           {{ link_to_route('year.edit', 'Edit', array($years->id), ['class' => 'btn btn-sm btn-primary']) }}
                           {{Form::submit('Delete', ['class' => 'btn btn-sm btn-danger','id'=>'delete'])}}
                           {{ Form::close() }}
                         </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.</p>
                    </div>

                    <div id="menu3" class="tab-pane fade">
                        <br>
                        <a href="{{route('model.create')}}">
                            <button type="button" class="order_status" style="float: right;"><i class="fa fa-plus-circle" aria-hidden="true"></i> Add New Vehicle Model</button>
                        </a>


                        <!-- custom table -->
                        <table class="table table-striped table-hover" id="menu1">
                            <thead>
                                <tr class="bg-primary">
                                    <th>Sl.no</th>
                                    <th>Brand Name</th>
                                    <!-- <th>Arabic Brand Name</th> -->
                                    <th>Geographical Area</th>
                                    <th>Year</th>
                                    <th>Vehicle Model</th>
                                    <th>Arabic Vehicle Model</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
<?php
$counter3 = 0;
?>
                            <tbody>
                                @foreach($model as $models)
                                <tr>
                                    <td><?php echo ++$counter3; ?></td>
                                    <td>{{@$models->vehicel->name}}</td>
                                    <!-- <td>{{@$models->vehicel->name_arabic}}</td> -->
                                    <td>{{@$models->geographical->name}}</td>
                                    <td>{{@$models->year->year}}</td>
                                    <td><a href="#">{{$models->name}}</a></td>
                                    <td><a href="#">{{$models->name_arabic}}</a></td>
                                     <td>{{ Form::open(array('route' => array('model.destroy', $models->id), 'method' => 'delete', 'class' => 'form-inline','name'=>'delete')) }}  
                           {{ link_to_route('model.edit', 'Edit', array($models->id), ['class' => 'btn btn-sm btn-primary']) }}
                           {{Form::submit('Delete', ['class' => 'btn btn-sm btn-danger','id'=>'delete'])}}
                           {{ Form::close() }}
                         </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <!-- custom table Ended-->



                        <p>Eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
                    </div>



                    <div id="menu4" class="tab-pane fade">
                        <br>
                        <a href="{{route('framenumber.create')}}">
                            <button type="button" class="order_status" style="float: right;"><i class="fa fa-plus-circle" aria-hidden="true"></i> Add Frame Number</button>
                        </a>
<?php
$counter4 = 0;
?>
                        <table class="table table-striped table-hover" id="menu1">
                            <thead>
                                <tr class="bg-primary">
                                    <th>Sl.no</th>
                                    <th>Brand Name</th>
                                    <!-- <th>Arabic Brand Name</th> -->
                                    <th>Geographical Area</th>
                                    <th>Year</th>
                                    <th>Vehicle Model</th>
                                    <th>Frame Number</th>
                                    <!-- <th>Arabic Frame Number</th> -->
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($frame as $frames)
                                <tr>
                                    <td><?php echo ++$counter4; ?></td>
                                    <td>{{@$frames->vehicel->name}}</td>
                                    <!-- <td>{{@$frames->vehicel->name_arabic}}</td> -->
                                    <td>{{@$frames->geographical->name}}</td>
                                    <td>{{@$frames->year->year}}</td>
                                    <td>{{@$frames->modeld->name}}</td>
                                    <td><a href="#">{{@$frames->frame_number}}</a></td>
                                    <!-- <td><a href="#">{{@$frames->frame_number_arabic}}</a></td> -->
                                     <td>{{ Form::open(array('route' => array('framenumber.destroy', $frames->id), 'method' => 'delete', 'class' => 'form-inline','name'=>'delete')) }}  
                           {{ link_to_route('framenumber.edit', 'Edit', array($frames->id), ['class' => 'btn btn-sm btn-primary']) }}
                           {{Form::submit('Delete', ['class' => 'btn btn-sm btn-danger','id'=>'delete'])}}
                           {{ Form::close() }}
                         </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>



                        <p>Eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
                    </div>




                    <div id="menu5" class="tab-pane fade">
                        <br>
                        <a href="{{route('gradeengine.create')}}">
                            <button type="button" class="order_status" style="float: right;"><i class="fa fa-plus-circle" aria-hidden="true"></i> Add Grade Engine</button>
                        </a>

<?php
$counter5 = 0;
?>

                        <table class="table table-striped table-hover" id="menu1">
                            <thead>
                                <tr class="bg-primary">
                                    <th>Sl.no</th>
                                    <th>Brand Name</th>
                                    <!-- <th>Arabic Brand Name</th> -->
                                    <th>Geographical Area</th>
                                    <th>Year</th>
                                    <th>Vehicle Model</th>
                                    <th>Frame Number</th>
                                    <th>Grade Number</th>
                                    <!-- <th>Arabic Grade Number</th> -->
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($grade as $grades)
                                <tr>
                                    <td><?php echo ++$counter5; ?></td>
                                    <td>{{@$grades->vehicel->name}}</td>
                                    <!-- <td>{{@$grades->vehicel->name_arabic}}</td> -->
                                    <td>{{@$grades->geographical->name}}</td>
                                    <td>{{@$grades->year->year}}</td>
                                    <td>{{@$grades->modeld->name}}</td>
                                    <td>{{@$grades->framenumber->frame_number}}</td>
                                    <td><a href="#">{{$grades->grade_number}}</a></td>
                                    <!-- <td><a href="#">{{$grades->grade_number_arabic}}</a></td> -->
                                     <td>{{ Form::open(array('route' => array('gradeengine.destroy', $grades->id), 'method' => 'delete', 'class' => 'form-inline','name'=>'delete')) }}  
                           {{ link_to_route('gradeengine.edit', 'Edit', array($grades->id), ['class' => 'btn btn-sm btn-primary']) }}
                           {{Form::submit('Delete', ['class' => 'btn btn-sm btn-danger','id'=>'delete'])}}
                           {{ Form::close() }}
                         </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>



                        <p>Eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
                    </div>





                    <!-- custom vin number -->
                    <div id="menu6" class="tab-pane fade">
                        <br>
                        <a href="{{route('vinnumber.create')}}">
                            <button type="button" class="order_status" style="float: right;"><i class="fa fa-plus-circle" aria-hidden="true"></i> Add VIN Number</button>
                        </a>


<?php
$counter6 = 0;
?>
                        <table class="table table-striped table-hover" id="menu1">
                            <thead>
                                <tr class="bg-primary">
                                    <th>Sl.no</th>
                                    <th>Brand Name</th>
                                    <th>Arabic Brand Name</th>
                                    <th>Geographical Area</th>
                                    <th>Year</th>
                                    <th>Vehicle Model</th>
                                    <th>Frame Number</th>
                                    <th>Grade Number </th>
                                    <th>VIN Number </th>
                                    <!-- <th>Arabic VIN Number </th> -->
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($vinnumber as $vinnumbers)
                                <tr>
                                    <td><?php echo ++$counter6; ?></td>
                                    <td>{{@$vinnumbers->vehicel->name}}</td>
                                    <td>{{@$vinnumbers->vehicel->name_arabic}}</td>
                                    <td>{{@$vinnumbers->geographical->name}}</td>
                                    <td>{{@$vinnumbers->year->year}}</td>
                                    <td>{{@$vinnumbers->modeld->name}}</td>
                                    <td>{{@$vinnumbers->framenumber->frame_number}}</td>
                                    <td>{{@$vinnumbers->gradenumber->grade_number}}</td>
                                    <td><a href="#">{{$vinnumbers->vin_number}}</a></td>
                                    <!-- <td><a href="#">{{$vinnumbers->vin_number_arabic}}</a></td> -->
                                     <td>{{ Form::open(array('route' => array('vinnumber.destroy', $vinnumbers->id), 'method' => 'delete', 'class' => 'form-inline','name'=>'delete')) }}  
                           {{ link_to_route('vinnumber.edit', 'Edit', array($vinnumbers->id), ['class' => 'btn btn-sm btn-primary']) }}
                           {{Form::submit('Delete', ['class' => 'btn btn-sm btn-danger','id'=>'delete'])}}
                           {{ Form::close() }}
                         </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>



                        <p>Eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
                    </div>
                    <!-- custom vin number ended-->


                    <!---------ADD PARTNUMBER--->

                    <div id="menu7" class="tab-pane fade">
                        <br>
                        <a href="{{route('addpart.create')}}">
                            <button type="button" class="order_status" style="float: right;"><i class="fa fa-plus-circle" aria-hidden="true"></i> Add Part Number</button>
                        </a>


<?php
$counter7 = 0;
?>
                        <table class="table table-striped table-hover" id="menu1">
                            <thead>
                                <tr class="bg-primary">
                                    <th>Sl.no</th>
                                    <th>Brand Name</th>
                                    <th>Arabic Brand Name</th>
                                    <th>Geographical Area</th>
                                    <th>Year</th>
                                    <th>Vehicle Model</th>
                                    <th>Frame Number</th>
                                    <th>Grade Number </th>
                                    <th>VIN Number </th>
                                    <th>Part Number</th>
                                    <th>Part Name</th>
                                    <th>Arabic Part Name</th>
                                    <th>Display Part Number</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($addpart as $addparts)
                                <tr>
                                    <td><?php echo ++$counter7; ?></td>
                                    <td>{{@$addparts->vehicel->name}}</td>
                                    <td>{{@$addparts->vehicel->name_arabic}}</td>
                                    <td>{{@$addparts->geographical->name}}</td>
                                    <td>{{@$addparts->year->year}}</td>
                                    <td>{{@$addparts->modeld->name}}</td>
                                    <td>{{@$addparts->framenumber->frame_number}}</td>
                                    <td>{{@$addparts->gradenumber->grade_number}}</td>
                                    <td>{{@$addparts->vinnumber->vin_number}}</td>
                                    <td><a href="#">{{$addparts->part_number}}</a></td>
                                    <td><a href="#">{{$addparts->part_name}}</a></td>
                                    <td><a href="#">{{$addparts->part_name_arabic   }}</a></td>
                                    <td><a href="#">{{$addparts->display_number}}</a></td>
                                     <td>{{ Form::open(array('route' => array('addpart.destroy', $addparts->id), 'method' => 'delete', 'class' => 'form-inline','name'=>'delete')) }}  
                           {{ link_to_route('addpart.edit', 'Edit', array($addparts->id), ['class' => 'btn btn-sm btn-primary']) }}
                           {{Form::submit('Delete', ['class' => 'btn btn-sm btn-danger','id'=>'delete'])}}
                           {{ Form::close() }}
                         </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>



                        <p>Eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
                    </div>
                </div>


                <!--------------table2------------->
            </div>
        </div>
        <!--End Row-->
        <!--End Dashboard Content-->
    </div>
    <!-- End container-fluid-->
</div>
<!--End content-wrapper-->
<!--Start Back To Top Button-->
<a href="javaScript:void();" class="back-to-top"><i class="fa fa-angle-double-up"></i> </a>
@endsection