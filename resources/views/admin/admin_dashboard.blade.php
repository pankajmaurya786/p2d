@extends('layouts.admin_dashboard')

@section('content')
<div class="content-wrapper">
            <div class="container-fluid">
               <!--Start Dashboard Content-->
               <div class="row">
                   <div class="container-fluid">
               <!-- Breadcrumb-->
               <div class="row pt-2 pb-2">
                  <div class="col-sm-9">
                  <h4 class="page-title">Dashboard</h4>
                  <ol class="breadcrumb">
                      <li class="breadcrumb-item"><a href="javaScript:void();">Dashtreme</a></li>
                      <li class="breadcrumb-item"><a href="javaScript:void();">Dashboard</a></li>
                   </ol>
               </div>
               </div>
                    @if(session()->has('message.level'))
                       <div class="alert alert-{{ session('message.level') }}"> 
                    {!! session('message.content') !!}
                       </div>
                    @endif
                <!-- End Breadcrumb-->
                <div class="row mt-3">
       <div class="col-12 col-lg-6 col-xl-3">
         <div class="card gradient-deepblue">
           <div class="card-body">
              <h5 class="text-white mb-0">{{$totaluser}} <span class="float-right"><i class="fa fa-shopping-cart"></i></span></h5>
                <div class="progress my-3" style="height:3px;">
                   <div class="progress-bar" style="width:55%"></div>
                </div>
              <p class="mb-0 text-white small-font">Total User <span class="float-right">+4.2% <i class="zmdi zmdi-long-arrow-up"></i></span></p>
            </div>
         </div> 
       </div>
       <div class="col-12 col-lg-6 col-xl-3">
         <div class="card gradient-orange">
           <div class="card-body">
              <h5 class="text-white mb-0">{{$totalvendor}} <span class="float-right"><i class="fa fa-eye"></i></span></h5>
                <div class="progress my-3" style="height:3px;">
                   <div class="progress-bar" style="width:55%"></div>
                </div>
              <p class="mb-0 text-white small-font">Total Vendor <span class="float-right">+1.2% <i class="zmdi zmdi-long-arrow-up"></i></span></p>
            </div>
         </div>
       </div>
       <div class="col-12 col-lg-6 col-xl-3">
         <div class="card gradient-ohhappiness">
            <div class="card-body">
              <h5 class="text-white mb-0">{{ $order }} <span class="float-right"><i class="fa fa-eye"></i></span></h5>
                <div class="progress my-3" style="height:3px;">
                   <div class="progress-bar" style="width:55%"></div>
                </div>
              <p class="mb-0 text-white small-font">Orders <span class="float-right">+5.2% <i class="zmdi zmdi-long-arrow-up"></i></span></p>
            </div>
         </div>
       </div>
       <div class="col-12 col-lg-6 col-xl-3">
         <div class="card gradient-ibiza">
            <div class="card-body">
              <h5 class="text-white mb-0">5630 <span class="float-right"><i class="fa fa-shopping-cart"></i></span></h5>
                <div class="progress my-3" style="height:3px;">
                   <div class="progress-bar" style="width:55%"></div>
                </div>
              <p class="mb-0 text-white small-font">Sell <span class="float-right">+2.2% <i class="zmdi zmdi-long-arrow-up"></i></span></p>
            </div>
         </div>
       </div>
     </div>
                    <div class="row">
                      <div class="col-lg-8">
                        <div class="card">
                          <div class="card-header text-uppercase">Grow Chart</div>
                          <div class="card-body">
                              <div id="bar-chart" style="height:300px;"></div>
                          </div>
                        </div>
                      </div>
                      <div class="col-lg-4">
                        <div class="card">
                          <div class="card-header text-uppercase">User & Admin </div>
                          <div class="card-body">
                            <div id="donut-chart" style="height:300px;"></div>
                          </div>
                        </div>
                      </div>
                    </div><!--End Row-->

                </div>
                  
               </div>
               <!--End Row-->
               <div class="row">
                  <div class="col-12 col-lg-12">
                     <div class="card">
                        <div class="card-header">
                           Recent Order Tables
                           <div class="card-action">
                              <div class="dropdown">
                                 <a href="javascript:void();" class="dropdown-toggle dropdown-toggle-nocaret" data-toggle="dropdown">
                                 <i class="icon-options"></i>
                                 </a>
                                 <div class="dropdown-menu dropdown-menu-right">
                                    <a class="dropdown-item" href="javascript:void();">Action</a>
                                    <a class="dropdown-item" href="javascript:void();">Another action</a>
                                    <a class="dropdown-item" href="javascript:void();">Something else here</a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="javascript:void();">Separated link</a>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="table-responsive">
                           <table class="table align-items-center table-flush table-borderless">
                              <thead>
                                 <tr>
                                    <th>Product</th>
                                    <th>Photo</th>
                                    <th>Product ID</th>
                                    <th>Amount</th>
                                    <th>Date</th>
                                    <th>Shipping</th>
                                 </tr>
                              </thead>
                              <tbody>
                                 <tr>
                                    <td>Hyndai</td>
                                    <td><img src="assets/images/product.png" class="product-img" alt="product img"></td>
                                    <td>#9405822</td>
                                    <td>1250.00 Sar</td>
                                    <td>03 Aug 2017</td>
                                    <td>
                                       <div class="progress shadow" style="height: 3px;">
                                          <div class="progress-bar" role="progressbar" style="width: 90%"></div>
                                       </div>
                                    </td>
                                 </tr>
                                 <tr>
                                    <td>Hyndai</td>
                                    <td><img src="assets/images/product.png" class="product-img" alt="product img"></td>
                                    <td>#9405820</td>
                                    <td>1500.00 Sar</td>
                                    <td>03 Aug 2017</td>
                                    <td>
                                       <div class="progress shadow" style="height: 3px;">
                                          <div class="progress-bar" role="progressbar" style="width: 60%"></div>
                                       </div>
                                    </td>
                                 </tr>
                                 <tr>
                                    <td>Hyndai</td>
                                    <td><img src="assets/images/product.png" class="product-img" alt="product img"></td>
                                    <td>#9405830</td>
                                    <td>1400.00 Sar</td>
                                    <td>03 Aug 2017</td>
                                    <td>
                                       <div class="progress shadow" style="height: 3px;">
                                          <div class="progress-bar" role="progressbar" style="width: 70%"></div>
                                       </div>
                                    </td>
                                 </tr>
                                 <tr>
                                    <td>Hyndai</td>
                                    <td><img src="assets/images/product.png" class="product-img" alt="product img"></td>
                                    <td>#9405825</td>
                                    <td>1200.00 Sar</td>
                                    <td>03 Aug 2017</td>
                                    <td>
                                       <div class="progress shadow" style="height: 3px;">
                                          <div class="progress-bar" role="progressbar" style="width: 100%"></div>
                                       </div>
                                    </td>
                                 </tr>
                                 <tr>
                                    <td>Hyndai</td>
                                    <td><img src="assets/images/product.png" class="product-img" alt="product img"></td>
                                    <td>#9405840</td>
                                    <td>1800.00 Sar</td>
                                    <td>03 Aug 2017</td>
                                    <td>
                                       <div class="progress shadow" style="height: 3px;">
                                          <div class="progress-bar" role="progressbar" style="width: 40%"></div>
                                       </div>
                                    </td>
                                 </tr>
                                 <tr>
                                    <td>Hyndai</td>
                                    <td><img src="assets/images/product.png" class="product-img" alt="product img"></td>
                                    <td>#9405825</td>
                                    <td>1200.00 Sar</td>
                                    <td>03 Aug 2017</td>
                                    <td>
                                       <div class="progress shadow" style="height: 3px;">
                                          <div class="progress-bar" role="progressbar" style="width: 100%"></div>
                                       </div>
                                    </td>
                                 </tr>
                              </tbody>
                           </table>
                        </div>
                     </div>
                  </div>
               </div>
               <!--End Row-->
               <!--End Dashboard Content-->
            </div>
            <!-- End container-fluid-->
         </div>
         @endsection