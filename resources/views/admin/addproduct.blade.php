@extends('layouts.admin_dashboard')

@section('content')

<div class="clearfix"></div>
        <div class="content-wrapper">
            <div class="container-fluid">
                <!--Start Dashboard Content-->
                <div class="row">
                    <div class="table-wrapper">
                        <div class="table-title">
                            <div class="row">
                                <div class="col-sm-5">
                                    <h2><b>Product Management</b></h2>
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="javaScript:void();">Manage</a></li>
                                        <li class="breadcrumb-item"><a href="javaScript:void();">Product Management</a></li>
                                    </ol>
                                </div>
                            </div>1
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="card">
                                    <div class="card-header text-uppercase">Add New Product </div>
                                    <div class="card-body">

                                        {{ Form::open(array('route' => 'addpost.productadmin','class'=>'dropzone dz-clickable','files'=>true)) }}
                                        <div class="form-group">
                                                <label class="control-label col-sm-2" for="email">Select Vechiel</label>
                                                <div class="col-sm-12">
                                                    <!--                                                    <label>Allow Multiple Images</label>-->
                                                    <style>
                                                        input.form-control.upload {
                                                            line-height: 1;
                                                        }
                                                    </style>
                                                    <select id="country" name="vehicel_id" class="form-control" style="padding: 10px;">
                                                        <option value="" class="text-black">Select Vehicel</option>
                                                        @foreach($vehicel as $vehicels)
                                                        <option value="{{$vehicels->id}}" class="text-black">{{$vehicels->name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-sm-2" for="email">Enter Part Number:</label>
                                                <div class="col-sm-12">

                                                    <!--custom select option -->
                                                    <link rel="stylesheet" href="{{asset('assets/css/select2.min.css')}}" />
                                                    <style>
                                                        .select2-dropdown {
                                                            top: 0px !important;
                                                            left: 0px !important;
                                                        }
                                                    </style>
                                                    <select id="country" name="addpart_id" class="form-control" style="padding: 10px;">
                                                        <option value="" class="text-black">Select Part</option>
                                                        @foreach($part as $parts)
                                                        <option value="{{$parts->id}}" class="text-black">{{$parts->part_number}}</option>
                                                        @endforeach
                                                    </select>
                                                    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
                                                    <script src="{{asset('assets/js/select2.min.js')}}"></script>
                                                    <script>
                                                        $("#country").select2({
                                                            placeholder: "Select Part",
                                                            allowClear: true
                                                        });
                                                    </script>
                                                    <!--custom select option Ended-->
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-sm-2" for="pwd">Add Images</label>
                                                <div class="col-sm-12">
                                                    <!--                                                    <label>Allow Multiple Images</label>-->
                                                    <style>
                                                        input.form-control.upload {
                                                            line-height: 1;
                                                        }
                                                    </style>
                                                    <input type="file" name="product_image" class="form-control upload" class="line_height_1">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-sm-2" for="pwd">Enter Part Price.</label>
                                                <div class="col-sm-12">
                                                    <input type="text" name="price" class="form-control" id="pwd">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-sm-2" for="pwd">Number Of Quantity</label>
                                                <div class="col-sm-12">
                                                    <input type="text" name="quantity" class="form-control" id="pwd">
                                                </div>
                                            </div>
                                             <div class="form-group">
                                                <label class="control-label col-sm-2" for="pwd">Select Vendor </label>
                                                <div class="col-sm-12">
                                                        <select name="vendor_id" style="width: 100%;
                                                            padding: 9px;
                                                            border-radius: 4px;">
                                                            <option value="" class="text-black">Select Vendor</option>
                                                            @foreach($vendor as $vendors)
                                                          <option value="{{$vendors->id}}">{{$vendors->company_name}}</option>
                                                          @endforeach
                                                        </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-sm-2" for="pwd">Description</label>
                                                <div class="col-sm-12">
                                                    <input type="textarea" name="description" class="form-control" id="pwd">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-sm-2" for="pwd">Arabic Description</label>
                                                <div class="col-sm-12">
                                                    <input type="textarea" name="arabic_description" class="form-control" id="pwd">
                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <div class="col-sm-offset-2 col-sm-10">
                                                    <button type="submit" class="btn btn-primary">Save</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="clearfix">
                            <div class="hint-text">Showing <b>5</b> out of <b>25</b> entries</div>

                        </div>
                    </div>
                </div>
                <!--End Row-->
                <!--End Dashboard Content-->
            </div>
            <!-- End container-fluid-->
        </div>
        <!--End content-wrapper-->
        <!--Start Back To Top Button-->
        <a href="javaScript:void();" class="back-to-top"><i class="fa fa-angle-double-up"></i> </a>


@endsection