<!DOCTYPE html>
<html @if(app()->getLocale() == 'en') lang="en" dir="ltr" @else lang="ar" style="dir:rtl"  @endif>

<head  @if(app()->getLocale() == 'en') lang="en" dir="ltr" @else lang="ar" style="dir:rtl" @endif >
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE" />
    <title>Parts2 door</title>
    <!-- Bootstrap -->
    <link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet'>
    <link href='https://fonts.googleapis.com/css?family=Century Gothic' rel='stylesheet'>
    <link href='https://fonts.googleapis.com/css?family=Corbel' rel='stylesheet'>

     <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
     <link href="{{ asset('css/style.css') }}" rel="stylesheet">
     <link href="{{ asset('css/style_milan.css') }}" rel="stylesheet">
     <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    
</head>

<body>
    
    @include('include.topinfo')
    @include('include.search')
    @include('include.vehicles')
    @include('include.footer')  

    <!--end footerWrap sec here-->
    <script src="{{ asset('js/jquery.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/custom.js') }}"></script>
    <script src="{{ asset('js/custom_milan.js') }}"></script>
</body>

</html>