<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['middleware'=>'language'],function(){
		Route::get('locale/{locale}', function($locale) {
			// dd($locale);
			Session::put('locale', $locale);
			return redirect()->back();
		})->name('language');
		Route::get('/', 'ParttodoorController@home')->name('home');
		Route::get('user_login', 'ParttodoorController@login')->name('users.login');
		Route::post('user_login', 'ParttodoorController@loginpost')->name('user.login');
		Route::post('user_register', 'ParttodoorController@registerpost')->name('user.register');
		Route::get('user_register', 'ParttodoorController@register')->name('users.register');
		Route::post('user_info_update', 'ParttodoorController@userUpdate')->name('users.userUpdate');
		Route::get('vendor_login', 'ParttodoorController@vendorlogin')->name('vendor.login');
		Route::post('vendor_login', 'ParttodoorController@vendorloginpost')->name('vendor.loginpost');
		Route::get('vendor_register', 'ParttodoorController@vendorregister')->name('vendor.register');
		Route::post('vendor_register', 'ParttodoorController@vendorregisterpost')->name('vendor.registerpost');
		Route::get('admin_login', 'ParttodoorController@adminlogin')->name('admin.login');
		Route::post('admin_login', 'ParttodoorController@adminloginpost')->name('admin.loginpost');
		Route::get('admin_register', 'ParttodoorController@adminregister')->name('admin.register');
		Route::post('admin_register', 'ParttodoorController@adminregisterpost')->name('admin.registerpost');
		Route::get('about-us','ParttodoorController@aboutus')->name('about.us');
		Route::get('contact-us','ParttodoorController@contactus')->name('contact.us');
		Route::post('contact-us','ParttodoorController@contactuspost')->name('contact.uspost');
		Route::get('location/{id}/', 'ParttodoorController@getLocation');
		Route::get('geographicals', 'ParttodoorController@geographicallist')->name('listing.geographical');
		Route::get('years', 'ParttodoorController@yearsearch')->name('search.list');
		Route::get('frames', 'ParttodoorController@framesearch')->name('search.frame');
		Route::get('grade', 'ParttodoorController@gradesearch')->name('search.grade');
		Route::get('vinsearch', 'ParttodoorController@vinsearch')->name('search.vin');
		Route::get('partssearch', 'ParttodoorController@partssearch')->name('search.showpart');
		Route::get('productssearch', 'ParttodoorController@productssearch')->name('search.productpart');
		Route::get('productssearchs', 'ParttodoorController@productssearch2')->name('search.productpart2');
		Route::post('frameproductssearch', 'ParttodoorController@frameproductssearch')->name('frame.productsearch');
		// Route::get('searchajax','ParttodoorController@autoComplete')->name('searchajax');
		Route::post('searchajax/autocomplete','ParttodoorController@autoCompleteSearch')->name('autoCompleteSearch');
		Route::get('cart', 'ProductController@cart');
		Route::get('add-to-cart/{id}', 'ProductController@addToCart')->name('add.cart');
		Route::patch('update-cart', 'ProductController@updatecart');
		Route::delete('remove-from-cart', 'ProductController@remove');
		Route::get('check_out', 'ParttodoorController@checkout')->name('check.out');
		Route::post('check_out', 'ParttodoorController@orderolaced')->name('check.post');

		

		Route::group(array('middleware' => 'auth'), function ()
		  {
		Route::get('dashboard','ParttodoorController@dashboard')->name('user.dashboard');
		Route::get('logout','ParttodoorController@logout')->name('user.logout');
		Route::post('user_address','ParttodoorController@useraddress')->name('user.address');
		Route::get('Vendor_Dashboard','ParttodoorController@vendordashboard')->name('vendor.dashboard');
		Route::get('Vendor_Profile','ParttodoorController@vendorprofile')->name('vendor.profile');
		Route::get('Admin_Dashboard','ParttodoorController@admindashboard')->name('admin.dashboard');
		Route::get('Admin_Product','AdminController@product')->name('admin.product');
		Route::get('Add_Product','AdminController@addproduct')->name('add.product');
		Route::post('Add_Product','AdminController@productstore')->name('adminproduct.store');

		Route::get('Admin_Vendor','AdminController@vendorlist')->name('vendor.adminlist');
		Route::get('Admin_Users','AdminController@userlist')->name('user.adminlist');
		Route::get('Admin_Productadd','AdminController@addproductadmin')->name('add.productadmin');
		Route::post('Admin_Productadd','AdminController@addproductadminpost')->name('addpost.productadmin');
		Route::get('Admin_Productlist','AdminController@addproductlist')->name('admin.productlist');
		Route::delete('AdminProduct/{id}/destroy','AdminController@productadmindelet')->name('adminproduct.destroy');
		Route::post('profile_images','ParttodoorController@updateprofile')->name('profile.image');
		Route::post('profile_update','ParttodoorController@updateprofiles')->name('profile.update');
		Route::resource('vehicel','VehicelController');
		Route::resource('geographical','GeographicsController');
		Route::resource('year','YearController');
		Route::resource('model','ModelController');
		Route::resource('framenumber','FrameController');
		Route::resource('gradeengine','GradenumberController');
		Route::resource('vinnumber','VinnumberController');
		Route::resource('addpart','AddpartController');
		Route::resource('product','ProductController');
		Route::resource('category','CategoryController');
		Route::resource('subcategory','SubcategoryController');
		Route::resource('about','AboutController');
		Route::resource('contact','ContactusController');
		Route::get('Admin_Usershow/{id}','AdminController@adminusershow')->name('admin.usershow');
		Route::get('Admin_Vendorshow/{id}','AdminController@adminvendorshow')->name('admin.vendorshow');
		Route::get('get-state-list','YearController@getStateList');
		Route::get('get-city-list','FrameController@getcitylist');
		Route::get('get-model-list','GradenumberController@getmodellist');
		Route::get('get-frame-list','VinnumberController@getgradelist');
		Route::get('get-grade-list','VinnumberController@getframelist');
		Route::get('get-vin-list','AddpartController@getvinlist');
		Route::get('order_details','ParttodoorController@vendororder')->name('vendor.order');
		Route::get('admin_order_details','ParttodoorController@adminorder')->name('admin.order');
		  	});
});
Auth::routes();


