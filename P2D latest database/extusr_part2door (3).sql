-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 28, 2019 at 03:27 PM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `extusr_part2door`
--

-- --------------------------------------------------------

--
-- Table structure for table `addparts`
--

CREATE TABLE `addparts` (
  `id` int(11) NOT NULL,
  `vehicel_id` int(11) NOT NULL,
  `year_id` int(11) NOT NULL,
  `geographical_id` int(11) NOT NULL,
  `modeld_id` int(11) NOT NULL,
  `framenumber_id` int(11) NOT NULL,
  `gradenumber_id` int(11) NOT NULL,
  `vinnumber_id` int(11) NOT NULL,
  `part_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `display_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `part_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `part_name_arabic` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `part_number_arabic` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `diaplay_number_arabic` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `addparts`
--

INSERT INTO `addparts` (`id`, `vehicel_id`, `year_id`, `geographical_id`, `modeld_id`, `framenumber_id`, `gradenumber_id`, `vinnumber_id`, `part_name`, `display_number`, `part_number`, `part_name_arabic`, `part_number_arabic`, `diaplay_number_arabic`, `created_at`, `updated_at`) VALUES
(1, 2, 1, 1, 1, 1, 1, 1, 'Gear Box', 'BFHDH12', 'ADDF1235', 'ناقل الحركة', NULL, NULL, '2019-09-27 12:01:31', '2019-09-27 06:31:31'),
(2, 3, 2, 2, 2, 2, 2, 2, 'Head Lights', 'FRTE134', 'KFE2356', 'أضواء الرأس', NULL, NULL, '2019-09-26 12:45:20', '2019-09-16 23:49:07'),
(3, 4, 3, 3, 3, 3, 3, 3, 'Sensor, wheel speed', 'ZXCDF45', 'MNHJKL4', 'الاستشعار ، وسرعة عجلة القيادة', NULL, NULL, '2019-09-26 12:45:44', '2019-09-16 23:49:53'),
(4, 5, 4, 4, 4, 4, 4, 4, 'brake pads', '1212', '321111', NULL, NULL, NULL, '2019-09-17 02:15:21', '2019-09-17 02:15:21'),
(5, 2, 1, 1, 1, 1, 1, 1, 'glass', 'TWEA231D', 'TY123ASE', 'زجاج', NULL, NULL, '2019-09-26 13:00:58', '2019-09-23 00:23:08'),
(6, 2, 1, 1, 1, 1, 1, 1, 'belt', '122', '1234', 'ناقل الحركة', NULL, NULL, '2019-09-25 23:22:23', '2019-09-25 23:22:23'),
(7, 2, 1, 1, 1, 1, 1, 1, 'glass1', 'glass1', '1', 'زجاج', NULL, NULL, '2019-09-28 06:24:15', '2019-09-26 07:12:46');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `framenumbers`
--

CREATE TABLE `framenumbers` (
  `id` int(11) NOT NULL,
  `vehicel_id` int(11) NOT NULL,
  `year_id` int(11) NOT NULL,
  `geographical_id` int(11) NOT NULL,
  `modeld_id` int(11) NOT NULL,
  `frame_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `frame_number_arabic` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `framenumbers`
--

INSERT INTO `framenumbers` (`id`, `vehicel_id`, `year_id`, `geographical_id`, `modeld_id`, `frame_number`, `frame_number_arabic`, `created_at`, `updated_at`) VALUES
(1, 2, 1, 1, 1, 'DE546F5', NULL, '2019-09-16 23:26:02', '2019-09-16 23:26:02'),
(2, 3, 2, 2, 2, 'FAD123A', NULL, '2019-09-16 23:27:09', '2019-09-16 23:27:09'),
(3, 4, 3, 3, 3, 'BSS123S', NULL, '2019-09-16 23:27:51', '2019-09-16 23:27:51'),
(4, 5, 4, 4, 4, 'ABC123', NULL, '2019-09-17 02:07:26', '2019-09-17 02:07:26');

-- --------------------------------------------------------

--
-- Table structure for table `geographicals`
--

CREATE TABLE `geographicals` (
  `id` int(11) NOT NULL,
  `vehicel_id` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `name_arabic` varchar(255) CHARACTER SET utf8 COLLATE utf8_latvian_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `geographicals`
--

INSERT INTO `geographicals` (`id`, `vehicel_id`, `name`, `slug`, `name_arabic`, `created_at`, `updated_at`) VALUES
(1, 2, 'India', 'india', 'الهند', '2019-09-26 11:56:42', '2019-09-26 06:26:42'),
(2, 3, 'USA', 'usa', 'الولايات المتحدة الأمريكية', '2019-09-24 18:06:38', '2019-09-16 23:17:40'),
(3, 4, 'Asia', 'asia', 'آسيا', '2019-09-24 18:06:50', '2019-09-16 23:18:40'),
(4, 5, 'Middle east', 'middle-east', 'الشرق الأوسط', '2019-09-24 18:07:02', '2019-09-17 02:03:26'),
(5, 8, 'asia', 'asia', 'آسيا', '2019-09-25 18:47:47', '2019-09-25 18:47:47'),
(6, 3, 'china', 'china', 'الصين', '2019-09-26 06:14:27', '2019-09-26 06:14:27');

-- --------------------------------------------------------

--
-- Table structure for table `gradeengines`
--

CREATE TABLE `gradeengines` (
  `id` int(11) NOT NULL,
  `vehicel_id` int(11) NOT NULL,
  `year_id` int(11) NOT NULL,
  `geographical_id` int(11) NOT NULL,
  `modeld_id` int(11) NOT NULL,
  `framenumber_id` int(11) NOT NULL,
  `grade_number` varchar(255) NOT NULL,
  `grade_number_arabic` varchar(255) CHARACTER SET utf8 COLLATE utf8_latvian_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gradeengines`
--

INSERT INTO `gradeengines` (`id`, `vehicel_id`, `year_id`, `geographical_id`, `modeld_id`, `framenumber_id`, `grade_number`, `grade_number_arabic`, `created_at`, `updated_at`) VALUES
(1, 2, 1, 1, 1, 1, 'DASD12X', NULL, '2019-09-16 23:29:53', '2019-09-16 23:29:53'),
(2, 3, 2, 2, 2, 2, 'LKAD234', NULL, '2019-09-16 23:31:07', '2019-09-16 23:31:07'),
(3, 4, 3, 3, 3, 3, 'MSF1234', NULL, '2019-09-16 23:31:47', '2019-09-16 23:31:47'),
(4, 5, 4, 4, 4, 4, '12345a', NULL, '2019-09-17 02:08:18', '2019-09-17 02:08:18'),
(5, 2, 1, 1, 1, 1, 'ABC3232', NULL, '2019-09-27 06:04:47', '2019-09-27 06:04:47');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `modelds`
--

CREATE TABLE `modelds` (
  `id` int(11) NOT NULL,
  `year_id` int(11) NOT NULL,
  `geographical_id` int(11) NOT NULL,
  `vehicel_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `name_arabic` varchar(255) CHARACTER SET utf8 COLLATE utf8_latvian_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `modelds`
--

INSERT INTO `modelds` (`id`, `year_id`, `geographical_id`, `vehicel_id`, `name`, `name_arabic`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 2, 'Optima', 'أوبتيما', '2019-09-24 18:10:13', '2019-09-16 23:22:57'),
(2, 2, 2, 3, 'I20', NULL, '2019-09-16 23:23:44', '2019-09-16 23:23:44'),
(3, 3, 3, 4, 'Innova', NULL, '2019-09-16 23:25:14', '2019-09-16 23:25:14'),
(4, 4, 4, 5, 'R8', NULL, '2019-09-17 02:05:38', '2019-09-17 02:05:38');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `vendor_id` int(11) DEFAULT NULL,
  `product_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `addpart_id` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `vendor_id`, `product_id`, `user_id`, `addpart_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 10, NULL, '2019-09-20 11:58:50', '2019-09-20 11:58:50'),
(2, 2, 1, 10, 1, '2019-09-20 06:39:54', '2019-09-20 12:42:23'),
(3, 3, 4, 10, 2, '2019-09-20 13:50:33', '2019-09-20 13:50:33'),
(4, 2, 2, 10, 2, '2019-09-20 13:51:32', '2019-09-20 13:51:32'),
(5, 1, 5, 12, 4, '2019-09-20 21:46:21', '2019-09-20 21:46:21'),
(6, 4, 6, 12, 4, '2019-09-20 21:46:34', '2019-09-20 21:46:34'),
(7, 1, 1, 13, 1, '2019-09-20 21:58:10', '2019-09-20 21:58:10'),
(8, 1, 1, 12, 1, '2019-09-20 23:00:11', '2019-09-20 23:00:11'),
(9, 1, 1, 14, 1, '2019-09-21 03:26:01', '2019-09-21 03:26:01'),
(10, 1, 1, 15, 1, '2019-09-21 20:34:26', '2019-09-21 20:34:26'),
(11, 1, 1, 14, 1, '2019-09-21 21:13:25', '2019-09-21 21:13:25'),
(12, 1, 1, 13, 1, '2019-09-22 00:16:01', '2019-09-22 00:16:01'),
(13, 4, 6, 16, 4, '2019-09-24 00:38:50', '2019-09-24 00:38:50'),
(14, 1, 1, 17, 1, '2019-09-24 21:41:28', '2019-09-24 21:41:28'),
(15, 2, 7, 1, 5, '2019-09-25 19:17:18', '2019-09-25 19:17:18'),
(16, 1, 1, 21, 1, '2019-09-28 05:39:32', '2019-09-28 05:39:32');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `addpart_id` int(11) NOT NULL,
  `vendor_id` int(11) NOT NULL,
  `vehicel_id` int(11) DEFAULT NULL,
  `price` varchar(255) NOT NULL,
  `quantity` varchar(255) NOT NULL,
  `description` text,
  `arabic_description` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `product_image` varchar(255) NOT NULL,
  `product_image_url` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `addpart_id`, `vendor_id`, `vehicel_id`, `price`, `quantity`, `description`, `arabic_description`, `product_image`, `product_image_url`, `created_at`, `updated_at`) VALUES
(1, 1, 1, NULL, '700', '5', 'Accommodations', 'أماكن الإقامة', 'ProductJRTq4o_part2.jpg', 'https://exteriortravels.com/uploads/product/ProductJRTq4o_part2.jpg', '2019-09-28 09:50:30', '2019-09-16 23:51:07'),
(2, 2, 2, NULL, '500', '10', 'Accommodations', 'أماكن الإقامة', 'ProductWPg0Wa_part6.png', 'https://exteriortravels.com/uploads/product/ProductWPg0Wa_part6.png', '2019-09-28 09:50:30', '2019-09-16 23:51:52'),
(3, 3, 1, NULL, '1000', '10', 'Accommodations', 'أماكن الإقامة', 'Product3mXA93_part7.png', 'https://exteriortravels.com/uploads/product/Product3mXA93_part7.png', '2019-09-28 09:50:30', '2019-09-16 23:52:35'),
(4, 2, 3, NULL, '700', '5', 'Accommodations', 'أماكن الإقامة', 'ProductVSIF6k_part11.png', 'https://exteriortravels.com/uploads/product/ProductVSIF6k_part11.png', '2019-09-28 09:50:30', '2019-09-17 00:24:54'),
(5, 4, 1, NULL, '20000', '12', 'Accommodations', 'أماكن الإقامة', 'ProductcD2gwL_download.jpg', 'https://exteriortravels.com/uploads/product/ProductcD2gwL_download.jpg', '2019-09-28 09:50:30', '2019-09-17 02:27:39'),
(6, 4, 4, NULL, '20000', '12', 'Accommodations', 'أماكن الإقامة', 'Products5bl7I_download.jpg', 'https://exteriortravels.com/uploads/product/Products5bl7I_download.jpg', '2019-09-28 09:50:30', '2019-09-17 02:46:08'),
(7, 5, 2, 2, '700', '10', 'Accommodations', 'أماكن الإقامة', 'ProductNIdzey_part13.png', 'https://exteriortravels.com/uploads/product/ProductNIdzey_part13.png', '2019-09-28 09:43:41', '2019-09-23 00:24:54');

-- --------------------------------------------------------

--
-- Table structure for table `subcategories`
--

CREATE TABLE `subcategories` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_type` enum('Admin','Vendor','User') COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `mobile`, `password`, `user_type`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Satish singh', 'info@carriersjobs.com', '09453350911', '$2y$10$ytVYIxSMVNNWHtH3EZ.PauZqWvLBISFRvtMC.6j5OmY.Cg9DB046G', 'Admin', '5Y1VfWUWe14CPsUzMaNyxr9SxH2mzAVIVAArUk4fxKyA37uoEPF5GAlbd7mV', '2019-08-30 06:40:18', '2019-08-30 06:40:18'),
(2, 'TheProductMART', 'satish@theproductmart.com', '08527794108', '$2y$10$ytVYIxSMVNNWHtH3EZ.PauZqWvLBISFRvtMC.6j5OmY.Cg9DB046G', 'Admin', NULL, '2019-08-30 06:07:49', '2019-08-30 06:07:49'),
(3, 'Satish singh', 'info12@carriersjobs.com', '9453350911', '$2y$10$Ihz3/piiFUghlreCWgQiVetI86S1sA6lGRQf29HnsE/xEOny618qS', 'Admin', NULL, '2019-08-31 03:03:40', '2019-08-31 03:03:40'),
(4, 'Satish singh', 'info13@carriersjobs.com', '9453350911', '$2y$10$Ihz3/piiFUghlreCWgQiVetI86S1sA6lGRQf29HnsE/xEOny618qS', 'Vendor', '2l85HLBP7f78mvl1qLF38BAJNPuUmMsR2oFN1ooB7GeFonATm2BQVasUo2al', '2019-08-31 03:53:49', '2019-09-10 06:55:34'),
(5, 'Satish singh', 'infotest@carriersjobs.com', '09453350911', '$2y$10$ytVYIxSMVNNWHtH3EZ.PauZqWvLBISFRvtMC.6j5OmY.Cg9DB046G', 'User', '$2y$10$ytVYIxSMVNNWHtH3EZ.PauZqWvLBISFRvtMC.6j5OmY.Cg9DB046G', '2019-08-31 06:02:24', '2019-08-31 06:02:24'),
(7, 'shri', 'ersrastigpt@gmail.com', '8802328599', '$2y$10$DMs9OGZUdKZQkJQSk5H0HOAumgoH6BRnDnpFWZ44c12RLe14/.nxS', 'User', 'GWjVXj00i5vBdKB3cShM3HQxe6Fk8CWhn5NsVmy59DOzyDBBlHzN9yNyGNf8', '2019-09-17 00:16:29', '2019-09-17 00:16:29'),
(9, 'vendortest1', 'vendortest2@gmail.com', '9453350911', '$2y$10$.Xs.2dQXsM4mFU90iLyF3eyGyBxSTMCsqdjH1s6g5HNZ6i3IeGhJO', 'Vendor', 'DSxIW3pFh5qJeeUvREwwd4bHRT0gHQSob1b76yJAgSbyWq7ppMObvjve4q6e', '2019-09-17 00:21:01', '2019-09-17 00:21:01'),
(10, 'Srasti Gupta', 'sri.gpt58@gmail.com', '8010308655', '$2y$10$jeCQgaFfA0du9qlG46zxY.B15ELldanJEO71mAKk4gz1dVXN/bujW', 'User', 'hWJpIYd10VBpVu5LhPwC3xyhMIbLbKW4iUhvH9d8eoaACqVm0qGuzEy4znTd', '2019-09-17 01:53:31', '2019-09-17 01:53:31'),
(11, 'srasti test', 'ersrastigpt1@gmail.com', '8010308655', '$2y$10$bdnL46K8CJytk.yJMqmcG.zM7dlB5yXNsuX3bangGW2nH6Q8RXxuK', 'Vendor', NULL, '2019-09-17 02:39:52', '2019-09-17 02:39:52'),
(13, 'sarla singh', 'sarla12@gmail.com', '8527794108', '$2y$10$w7y1O6BPK8mNAzRiRa/SAuRwBCF9i3VPLoNBjWPArqU5iwW7P0Bxy', 'User', 'cGtgQuqPbau6rSiLvbPH7TYhVc9YdCHPmJHK9rzRB8OcWPA6WAoQqa3V0EZN', '2019-09-20 21:55:39', '2019-09-20 21:55:39'),
(14, 'ankit', 'erankit.ee@gmail.com', '8802328599', '$2y$10$nFZUAJBjonJ6uHIBuDrtUOlXQQwDeE.UM3XfupfvIMRAyedvWcrIe', 'User', NULL, '2019-09-21 03:24:47', '2019-09-21 03:24:47'),
(15, 'ankit', 'hemant.singh1990@gmail.com', '1234567890', '$2y$10$DA47eKWEVsVqnUSyftbv/eDPoF9FNxlM4RbsrVmcMGJNlVmUcy1ne', 'User', NULL, '2019-09-21 20:33:21', '2019-09-21 20:33:21'),
(17, 'sumit', 'sumitashwanii@gmail.com', '9654101333', '$2y$10$hop6TJTspDU4DX3ilok2huUydp.T9FwcDBt.LmQpKJWwW9Dj0yiwe', 'User', '5khMnkbx2z63efE5jFjzhLRdxzfXJC1bkVlbd9YIfGzNF3DC1hVTWmm4YB9d', '2019-09-24 21:27:07', '2019-09-24 21:27:07'),
(18, 'Ali Zaik', 'ali.zaik45@gmail.com', '4573675436', '$2y$10$nY.2X9qhOYnEnz4S3nZ/uOIbOucWjkvdwYL1UzPxV7e9mKRd7z1/e', 'User', NULL, '2019-09-24 22:10:17', '2019-09-24 22:10:17'),
(19, 'Ahmed Qalil', 'ah.mist@gmail.com', '5004556734', '$2y$10$UtNAKPpypFx8MTiPOJsPV.fdZht/F5lZ2tHaaWEWe8Cs36wsqSNTi', 'User', NULL, '2019-09-24 22:13:29', '2019-09-24 22:13:29'),
(20, 'pankaj', 'pankaj@gmail.com', '09453350911', '$2y$10$ytVYIxSMVNNWHtH3EZ.PauZqWvLBISFRvtMC.6j5OmY.Cg9DB046G', 'Admin', '', '2019-08-30 06:40:18', '2019-08-30 06:40:18'),
(21, 'TEST', 'user@gmail.com', '1234567892', '$2y$10$ATtfll8Ovof5Aaw.n/oGXOolKDF3TivwFiJxWPWu4cUbUk2RnC6KW', 'User', 'gv5sYL0DmJ7Uj0arE0oSHZZ4mHL2U4tYpthlAuSTI4fnGpjldXeLJx16gllu', '2019-09-28 05:38:20', '2019-09-28 05:38:20');

-- --------------------------------------------------------

--
-- Table structure for table `user_addres`
--

CREATE TABLE `user_addres` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `address` varchar(255) NOT NULL,
  `pin_code` varchar(255) NOT NULL,
  `locality` varchar(255) NOT NULL,
  `states` varchar(255) NOT NULL,
  `address_location` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_addres`
--

INSERT INTO `user_addres` (`id`, `user_id`, `address`, `pin_code`, `locality`, `states`, `address_location`, `created_at`, `updated_at`) VALUES
(1, 1, 'b-60 noida', '201301', 'Firozabad', 'Uttar Pradesh', 0, '2019-08-31 00:21:50', '2019-08-31 00:21:50'),
(2, 1, 'sadar bazar bus stop colonel, gonda', '271502', 'Colonelganj', 'Uttar Pradesh', 0, '2019-08-31 07:24:16', '2019-08-31 00:25:55'),
(3, 6, 'A98, second floor', '201301', 'Sector 15,noida', 'Uttar Pradesh', 0, '2019-09-02 13:41:49', '2019-09-02 13:41:49'),
(4, 5, 'B-832 GALI NO-1 FIRST FLOOR NEW ASHOK NAGAR DELHI', '201301', 'sbi', 'Uttar Pradesh', 0, '2019-09-07 11:02:27', '2019-09-07 05:49:44'),
(5, 10, 'Noida, Near Nirula', '201301', 'Noida', 'India (+91)', 0, '2019-09-17 01:54:40', '2019-09-17 01:54:40'),
(6, 12, 'b1', '110097', 'delhi', 'delhi', 0, '2019-09-20 21:45:57', '2019-09-20 21:45:57'),
(8, 14, 'test', '2301301', 'test', 'up', 0, '2019-09-21 03:25:41', '2019-09-21 03:25:41'),
(9, 13, 'B-832 Ashok Nagar Delhi', '201301', 'Metro Station', 'Delhi', 0, '2019-09-22 00:07:27', '2019-09-22 00:07:27'),
(10, 16, 'jedaah', '552', '552', 'sf', 0, '2019-09-24 00:38:32', '2019-09-24 00:38:32'),
(11, 17, 'Delhi', '110017', 'Delhi', 'Delhi', 0, '2019-09-24 21:41:08', '2019-09-24 21:41:08'),
(12, 21, 'abc', '1111111', 'bd', 'hgfh', 0, '2019-09-28 05:39:06', '2019-09-28 05:39:06'),
(13, 21, 'abc', '1111111', 'bd', 'hgfh', 0, '2019-09-28 05:39:06', '2019-09-28 05:39:06');

-- --------------------------------------------------------

--
-- Table structure for table `vehicels`
--

CREATE TABLE `vehicels` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `product_icon` varchar(255) NOT NULL,
  `product_icon_url` varchar(255) NOT NULL,
  `name_arabic` varchar(255) CHARACTER SET utf8 COLLATE utf8_latvian_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vehicels`
--

INSERT INTO `vehicels` (`id`, `name`, `slug`, `product_icon`, `product_icon_url`, `name_arabic`, `created_at`, `updated_at`) VALUES
(2, 'KIA', 'kia', '/tmp/phpRbY8XH', 'http://localhost/P2D/public/uploads/vehicel/VehicelDyanfY_kia.png', 'المحركات كيا', '2019-09-26 10:15:42', '2019-09-18 15:36:13'),
(3, 'Hundai', 'hundai', 'Vehicel40psC8_car_brand2.png', 'http://localhost/P2D/uploads/vehicel/Vehicel40psC8_car_brand2.png', 'هيونداي', '2019-09-27 09:59:51', '2019-09-18 15:49:46'),
(4, 'Toyota', 'toyota', 'VehicelI630DI_car_brand.png', 'http://localhost/P2D/uploads/vehicel/VehicelI630DI_car_brand.png', 'تويوتا', '2019-09-27 09:59:51', '2019-09-18 15:55:14'),
(5, 'Audi', 'audi', 'VehicelJfzwXb_Audi-Logo.png', 'http://localhost/P2D/uploads/vehicel/VehicelJfzwXb_Audi-Logo.png', 'أودي', '2019-09-27 09:59:51', '2019-09-18 15:56:16'),
(6, 'Honda', 'honda', 'VehicelH7TfPC_photo-1523676060187-f55189a71f5e.jfif', 'http://localhost/P2D/uploads/vehicel/VehicelH7TfPC_photo-1523676060187-f55189a71f5e.jfif', 'هوندا', '2019-09-27 09:59:51', '2019-09-21 03:19:49'),
(7, 'BMW', 'bmw', 'VehiceljviyTP_BMW-logo.png', 'http://localhost/P2D/uploads/vehicel/VehiceljviyTP_BMW-logo.png', 'بي إم دبليو', '2019-09-27 09:59:51', '2019-09-22 00:19:23'),
(8, 'ferrari', 'ferrari', 'VehicelOofcYE_de195c90ec36f167726f53afd684d275.jpg', 'http://localhost/P2D/uploads/vehicel/VehicelOofcYE_de195c90ec36f167726f53afd684d275.jpg', 'فيراري', '2019-09-27 09:59:51', '2019-09-25 18:44:30'),
(9, 'Lembo', 'lembo', 'VehicelphagBx_de195c90ec36f167726f53afd684d275.jpg', 'http://localhost/P2D/uploads/vehicel/VehicelphagBx_de195c90ec36f167726f53afd684d275.jpg', 'لامبورغيني', '2019-09-27 09:59:51', '2019-09-25 18:46:35');

-- --------------------------------------------------------

--
-- Table structure for table `vendors`
--

CREATE TABLE `vendors` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `company_name` varchar(255) NOT NULL,
  `address` text,
  `flat_no` varchar(255) DEFAULT NULL,
  `locality` varchar(255) DEFAULT NULL,
  `landmark` varchar(255) DEFAULT NULL,
  `profile_icon` varchar(255) DEFAULT NULL,
  `profile_icon_url` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vendors`
--

INSERT INTO `vendors` (`id`, `user_id`, `company_name`, `address`, `flat_no`, `locality`, `landmark`, `profile_icon`, `profile_icon_url`, `created_at`, `updated_at`) VALUES
(1, 3, 'TheProductMART', '', NULL, NULL, NULL, '', '', '2019-08-31 03:03:40', '2019-08-31 03:03:40'),
(2, 4, 'AmitAuto PART', 'B-60 Sector-2 Noida', 'b-60', 'Sbi Main Branch', 'metro setation 15', 'ProductjkYROJ_car_brand6.png', 'http://localhost/part2door/public/uploads/profile/ProductjkYROJ_car_brand6.png', '2019-09-10 12:25:34', '2019-09-10 06:55:34'),
(3, 9, 'vendortest2', NULL, NULL, NULL, NULL, 'Product4XORhr_flags.png', 'https://exteriortravels.com/uploads/profile/Product4XORhr_flags.png', '2019-09-16 17:26:50', '2019-09-17 00:26:50'),
(4, 11, 'test', NULL, NULL, NULL, NULL, NULL, NULL, '2019-09-17 02:39:52', '2019-09-17 02:39:52');

-- --------------------------------------------------------

--
-- Table structure for table `vinnumbers`
--

CREATE TABLE `vinnumbers` (
  `id` int(11) NOT NULL,
  `vehicel_id` int(11) NOT NULL,
  `year_id` int(11) NOT NULL,
  `geographical_id` int(11) NOT NULL,
  `modeld_id` int(11) NOT NULL,
  `framenumber_id` int(11) NOT NULL,
  `gradenumber_id` int(11) NOT NULL,
  `vin_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `vin_number_arabic` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `vinnumbers`
--

INSERT INTO `vinnumbers` (`id`, `vehicel_id`, `year_id`, `geographical_id`, `modeld_id`, `framenumber_id`, `gradenumber_id`, `vin_number`, `vin_number_arabic`, `created_at`, `updated_at`) VALUES
(1, 2, 1, 1, 1, 1, 1, 'FGTR123', NULL, '2019-09-16 23:32:41', '2019-09-16 23:32:41'),
(2, 3, 2, 2, 2, 2, 2, 'VDEE345', NULL, '2019-09-16 23:36:20', '2019-09-16 23:36:20'),
(3, 4, 3, 3, 3, 3, 3, 'LORTY345', NULL, '2019-09-16 23:36:59', '2019-09-16 23:36:59'),
(4, 5, 4, 4, 4, 4, 4, '123445', NULL, '2019-09-17 02:12:56', '2019-09-17 02:12:56');

-- --------------------------------------------------------

--
-- Table structure for table `years`
--

CREATE TABLE `years` (
  `id` int(11) NOT NULL,
  `vehicel_id` int(11) NOT NULL,
  `geographical_id` int(11) NOT NULL,
  `year` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `years`
--

INSERT INTO `years` (`id`, `vehicel_id`, `geographical_id`, `year`, `slug`, `created_at`, `updated_at`) VALUES
(1, 2, 1, '1991', '1991', '2019-09-16 23:19:53', '2019-09-16 23:19:53'),
(2, 3, 2, '2001', '2001', '2019-09-16 23:20:40', '2019-09-16 23:20:40'),
(3, 4, 3, '2011', '2011', '2019-09-16 23:21:42', '2019-09-16 23:21:42'),
(4, 5, 4, '2019', '2019', '2019-09-17 02:04:27', '2019-09-17 02:04:27'),
(5, 2, 1, '2015', '2015', '2019-09-26 06:29:25', '2019-09-26 06:29:25');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `addparts`
--
ALTER TABLE `addparts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `framenumbers`
--
ALTER TABLE `framenumbers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `geographicals`
--
ALTER TABLE `geographicals`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gradeengines`
--
ALTER TABLE `gradeengines`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `modelds`
--
ALTER TABLE `modelds`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subcategories`
--
ALTER TABLE `subcategories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_addres`
--
ALTER TABLE `user_addres`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vehicels`
--
ALTER TABLE `vehicels`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vendors`
--
ALTER TABLE `vendors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vinnumbers`
--
ALTER TABLE `vinnumbers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `years`
--
ALTER TABLE `years`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `addparts`
--
ALTER TABLE `addparts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `framenumbers`
--
ALTER TABLE `framenumbers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `geographicals`
--
ALTER TABLE `geographicals`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `gradeengines`
--
ALTER TABLE `gradeengines`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `modelds`
--
ALTER TABLE `modelds`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `subcategories`
--
ALTER TABLE `subcategories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `user_addres`
--
ALTER TABLE `user_addres`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `vehicels`
--
ALTER TABLE `vehicels`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `vendors`
--
ALTER TABLE `vendors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `vinnumbers`
--
ALTER TABLE `vinnumbers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `years`
--
ALTER TABLE `years`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
